msgid ""
msgstr ""
"Project-Id-Version: portfolio\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-16 13:33+0300\n"
"PO-Revision-Date: 2015-04-16 13:33+0300\n"
"Last-Translator: bestwebsoft.com <plugins@bestwebsoft.com>\n"
"Language-Team: Grupo Gomariz, S.L. http://www.grupogomariz.com "
"<grupogomariz@gmail.com>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: .\n"

#: portfolio.php:41 portfolio.php:481 portfolio.php:482
msgid "Portfolio"
msgstr "Portfolio"

#: portfolio.php:62
msgid "Short description"
msgstr "Descripción breve"

#: portfolio.php:62
msgid ""
"A short description which you'd like to be displayed on your portfolio page"
msgstr ""
"Una breve descripción que le gustaría que se mostrara en su página de "
"portfolio"

#: portfolio.php:63
msgid "Date of completion"
msgstr "Fecha de finalización"

#: portfolio.php:63
msgid "The date when the task was completed"
msgstr "La fecha en la que se completó la tarea"

#: portfolio.php:64 portfolio.php:395
msgid "Link"
msgstr "Enlace"

#: portfolio.php:64
msgid "A link to the site"
msgstr "Enlace al sitio web"

#: portfolio.php:65 portfolio.php:398
msgid "SVN"
msgstr "SVN"

#: portfolio.php:65
#, fuzzy
msgid "SVN URL"
msgstr "SVN"

#: portfolio.php:106 portfolio.php:412
msgid "Date of completion:"
msgstr "Fecha de finalización:"

#: portfolio.php:107 portfolio.php:413
msgid "Link:"
msgstr "Enlace:"

#: portfolio.php:108 portfolio.php:414
msgid "Short description:"
msgstr "Descripción breve:"

#: portfolio.php:109 portfolio.php:415
msgid "Description:"
msgstr "Descripción:"

#: portfolio.php:110 portfolio.php:416
msgid "SVN:"
msgstr "SVN:"

#: portfolio.php:111 portfolio.php:417
#, fuzzy
msgid "Executor Profile:"
msgstr "Perfil del creador:"

#: portfolio.php:112 portfolio.php:418
msgid "More screenshots:"
msgstr "Más capturas de pantalla:"

#: portfolio.php:113 portfolio.php:419
msgid "Technologies:"
msgstr "Tecnologías:"

#: portfolio.php:217
msgid ""
"The files \"portfolio.php\" and \"portfolio-post.php\" are not found in your "
"theme directory. Please copy them from the directory `wp-content/plugins/"
"portfolio/template/` to your theme directory for correct work of the "
"Portfolio plugin"
msgstr ""

#: portfolio.php:311
#, fuzzy
msgid "Settings saved."
msgstr "Opciones guardadas."

#: portfolio.php:316
#, fuzzy
msgid "Portfolio Settings"
msgstr "Opciones del Portfolio"

#: portfolio.php:318 portfolio.php:834 portfolio.php:850
msgid "Settings"
msgstr "Configuración"

#: portfolio.php:319 portfolio.php:835
msgid "FAQ"
msgstr "FAQ"

#: portfolio.php:323
msgid "Notice:"
msgstr ""

#: portfolio.php:323
msgid ""
"The plugin's settings have been changed. In order to save them please don't "
"forget to click the 'Save Changes' button."
msgstr ""

#: portfolio.php:324
msgid ""
"If you would like to add the Latest Portfolio Items to your page or post, "
"just copy and paste this shortcode into your post or page:"
msgstr ""

#: portfolio.php:324
msgid "where count=3 is a number of posts to show up in the portfolio."
msgstr ""

#: portfolio.php:329
#, fuzzy
msgid "Change the way to store your post_meta information for portfolio"
msgstr ""
"Actualiza la forma de almacenar su post_meta información para el portfolio"

#: portfolio.php:331
msgid "Update All Info"
msgstr "Update alle info"

#: portfolio.php:338
msgid "Update images for portfolio"
msgstr "Actualizar imágenes para el portfolio"

#: portfolio.php:340
msgid "Update images"
msgstr "Actualizar imágenes"

#: portfolio.php:350
msgid "The album cover size"
msgstr ""

#: portfolio.php:352 portfolio.php:360
msgid "Image size name"
msgstr "Nombre de la dimensión"

#: portfolio.php:353 portfolio.php:361
msgid "Width (in px)"
msgstr "Anchura (en px)"

#: portfolio.php:354 portfolio.php:362
msgid "Height (in px)"
msgstr "Altura (en px)"

#: portfolio.php:358
#, fuzzy
msgid "Size of portfolio images"
msgstr "Tamaño de la imagen del portfolio"

#: portfolio.php:366
#, fuzzy
msgid ""
"WordPress will copy thumbnails with the specified dimensions when you upload "
"a new image. It is necessary to click the Update images button at the bottom "
"of this page in order to generate new images and set new dimensions"
msgstr ""
"Wordpress creará una copia del la miniatura con las dimensiones "
"especificadas cuando usted suba una nueva imagen. Es necesario hacer click "
"en el botón Actualizar Imágenes en la parte de abajo de esta página para "
"generar las nuevas imáganes de acuerdo a las nuevas dimensiones."

#: portfolio.php:369
#, fuzzy
msgid "Sort portfolio by"
msgstr "Portfolio"

#: portfolio.php:371
#, fuzzy
msgid "portfolio id"
msgstr "Portfolio"

#: portfolio.php:372
#, fuzzy
msgid "portfolio title"
msgstr "Opciones del Portfolio"

#: portfolio.php:373
msgid "date"
msgstr ""

#: portfolio.php:374
msgid "menu order"
msgstr ""

#: portfolio.php:375
msgid "random"
msgstr ""

#: portfolio.php:379
#, fuzzy
msgid "Portfolio sorting"
msgstr "Opciones del Portfolio"

#: portfolio.php:381
msgid "ASC (ascending order from lowest to highest values - 1, 2, 3; a, b, c)"
msgstr ""

#: portfolio.php:382
msgid ""
"DESC (descending order from highest to lowest values - 3, 2, 1; c, b, a)"
msgstr ""

#: portfolio.php:386
#, fuzzy
msgid "Number of images in the row"
msgstr "Número de imágenes por fila"

#: portfolio.php:392
msgid "Display additional fields"
msgstr "Mostrar campos adicionales"

#: portfolio.php:394
msgid "Date"
msgstr "Fecha"

#: portfolio.php:396
msgid "Short Description"
msgstr "Descripción breve:"

#: portfolio.php:397
msgid "Description"
msgstr "Descripción"

#: portfolio.php:399
msgid "Executor"
msgstr "Creador"

#: portfolio.php:400 portfolio.php:558 portfolio.php:572 portfolio.php:613
#: portfolio.php:1004
msgid "Technologies"
msgstr "Tecnologías"

#: portfolio.php:404
msgid "Display the link field as a text for non-registered users"
msgstr ""

#: portfolio.php:410
msgid "Text for additional fields"
msgstr "Texto para los campos adicionales"

#: portfolio.php:423
#, fuzzy
msgid "Slug for portfolio item"
msgstr "Portfolio"

#: portfolio.php:425
msgid "for any structure of permalinks except the default structure"
msgstr ""

#: portfolio.php:429
msgid "Rewrite templates after update"
msgstr ""

#: portfolio.php:431
msgid ""
"Turn off the checkbox, if You edited the file 'portfolio.php' or 'portfolio-"
"post.php' file in your theme folder and You don't want to rewrite them"
msgstr ""

#: portfolio.php:435
msgid "Rename uploaded images"
msgstr ""

#: portfolio.php:437
msgid ""
"To avoid conflicts, all the symbols will be excluded, except numbers, the "
"Roman letters,  _ and - symbols."
msgstr ""

#: portfolio.php:441
#, fuzzy
msgid "Add portfolio to the search"
msgstr "Enlace al sitio web"

#: portfolio.php:446 portfolio.php:449 portfolio.php:453
msgid "Using Custom Search powered by"
msgstr ""

#: portfolio.php:449
msgid "Activate Custom Search"
msgstr ""

#: portfolio.php:453
msgid "Download Custom Search"
msgstr ""

#: portfolio.php:460
msgid "Save Changes"
msgstr "Guardar cambios"

#: portfolio.php:483
msgid "Add New"
msgstr "Añadir nuevo"

#: portfolio.php:484
msgid "Add New Portfolio"
msgstr "Añadir nuevo Portfolio"

#: portfolio.php:485
msgid "Edit"
msgstr "Editar"

#: portfolio.php:486
msgid "Edit Portfolio"
msgstr "Editar Portfolio"

#: portfolio.php:487
msgid "New Portfolio"
msgstr "Nuevo Portfolio"

#: portfolio.php:488 portfolio.php:489
msgid "View Portfolio"
msgstr "Ver Portfolio"

#: portfolio.php:490
msgid "Search Portfolio"
msgstr "Buscar Portfolio"

#: portfolio.php:491
msgid "No portfolio found"
msgstr "No se ha encontrado ningún portfolio"

#: portfolio.php:492
msgid "No portfolio found in Trash"
msgstr "No se ha encontrado ningún portfolio en la Papelera"

#: portfolio.php:493
msgid "Parent Portfolio"
msgstr "Portfolio padre"

#: portfolio.php:495
#, fuzzy
msgid "Create a portfolio item"
msgstr "Crear un Portfolio"

#: portfolio.php:528
msgid "Executor Profiles"
msgstr "Profielen van de makers"

#: portfolio.php:529 template/portfolio.php:19 template/portfolio.php:20
msgid "Executor Profile"
msgstr "Perfil del creador del proyecto"

#: portfolio.php:530
msgid "Search Executor Profiles"
msgstr "Buscar perfiles de creadores de proyectos"

#: portfolio.php:531
msgid "Popular Executor Profiles"
msgstr "Perfiles de creadores de proyectos populares"

#: portfolio.php:532
msgid "All Executor Profiles"
msgstr "Todos los perfiles de creadores de proyectos"

#: portfolio.php:533
msgid "Parent Executor Profile"
msgstr "Perfil de creador del proyecto padre"

#: portfolio.php:534
msgid "Parent Executor Profile:"
msgstr "Perfil de creador del proyecto padre:"

#: portfolio.php:535
msgid "Edit Executor Profile"
msgstr "Editar Perfil de creador del proyecto"

#: portfolio.php:536
msgid "Update Executor Profile"
msgstr "Actualizar perfil de creador del proyecto"

#: portfolio.php:537
msgid "Add New Executor Profile"
msgstr "Añadir nuevo perfil de creador del proyecto "

#: portfolio.php:538
#, fuzzy
msgid "New Executor Name"
msgstr "Profiel naam van de maker"

#: portfolio.php:539
msgid "Separate Executor Profiles with commas"
msgstr "Scheid de maker profielen met een komma"

#: portfolio.php:540
msgid "Add or remove Executor Profile"
msgstr "Beheer maker profielen"

#: portfolio.php:541
#, fuzzy
msgid "Choose from the most used Executor Profiles"
msgstr "Kies uit de meest gebruike profielen"

#: portfolio.php:542
#, fuzzy
msgid "Executors"
msgstr "Creador"

#: portfolio.php:559
msgid "Technology"
msgstr "Tecnología"

#: portfolio.php:560
msgid "Search Technologies"
msgstr "Buscar Tecnologías"

#: portfolio.php:561
msgid "Popular Technologies"
msgstr "Tecnologías populares"

#: portfolio.php:562
msgid "All Technologies"
msgstr "Todas las Tecnologías"

#: portfolio.php:563
msgid "Parent Technology"
msgstr "Tecnología padre"

#: portfolio.php:564
msgid "Parent Technology:"
msgstr "Tecnología padre:"

#: portfolio.php:565
msgid "Edit Technology"
msgstr "Editar Tecnología"

#: portfolio.php:566
msgid "Update Technology"
msgstr "Actualizar Tecnología"

#: portfolio.php:567
msgid "Add New Technology"
msgstr "Voeg Tecnología toe"

#: portfolio.php:568
msgid "New Technology Name"
msgstr "Nuevo nombre de Tecnología"

#: portfolio.php:569
msgid "Separate Technologies with commas"
msgstr "Separar Tecnologías con comas"

#: portfolio.php:570
msgid "Add or remove Technology"
msgstr "Agregar o quitar Tecnología"

#: portfolio.php:571
#, fuzzy
msgid "Choose from the most used technologies"
msgstr "Elegir la tecnología más usada"

#: portfolio.php:614
#, fuzzy
msgid "Your most used portfolio technologies as a tag cloud"
msgstr "Las tecnologías de portfolio más usadas en formato nube"

#: portfolio.php:639
msgid "Title"
msgstr "Título"

#: portfolio.php:677
msgid "Portfolio Info"
msgstr "Información del Portfolio"

#: portfolio.php:679
msgid "Already attached"
msgstr ""

#: portfolio.php:706
#, fuzzy
msgid "Activate"
msgstr "Plugins activados"

#: portfolio.php:713
#, fuzzy
msgid "Install now"
msgstr "Instalar %s"

#: portfolio.php:716
msgid ""
"If you'd like to attach the files, which are already uploaded, please use Re-"
"attacher plugin."
msgstr ""

#: portfolio.php:719
#, fuzzy
msgid "Learn more"
msgstr "Leer más"

#: portfolio.php:836
msgid "Support"
msgstr "Soporte"

#: portfolio.php:999 template/portfolio.php:163
msgid "Read more"
msgstr "Leer más"

#: portfolio.php:1009 template/portfolio.php:173
#: template/portfolio-post.php:159
#, php-format
msgid "View all posts in %s"
msgstr "Ver todas las entradas en %s"

#: portfolio.php:1038
#, fuzzy
msgid "Updating post_meta information..."
msgstr "Actualize post_meta información ..."

#: portfolio.php:1039
#, fuzzy
msgid "No portfolio item found"
msgstr "No se ha encontrado ningún portfolio"

#: portfolio.php:1040
#, fuzzy
msgid "All info is updated"
msgstr "Toda la información ha sido actualizada."

#: portfolio.php:1041 portfolio.php:1045
msgid "Error."
msgstr "Error."

#: portfolio.php:1042
#, fuzzy
msgid "Updating images..."
msgstr "Actualizar imágenes ..."

#: portfolio.php:1043
#, fuzzy
msgid "No image found"
msgstr "No se han encontrado imágenes."

#: portfolio.php:1044
#, fuzzy
msgid "All images are updated"
msgstr "Todas las imágenes han sido actualizadas."

#: portfolio.php:1213
#, fuzzy
msgid "Image size not defined"
msgstr "Nombre de la dimensión"

#: portfolio.php:1227
msgid ""
"We can update only PNG, JPEG, GIF, WPMP or XBM filetype. For other, please, "
"manually reload image."
msgstr ""

#: portfolio.php:1237
msgid "Image size changes not defined"
msgstr ""

#: portfolio.php:1262 portfolio.php:1265 portfolio.php:1270
msgid "Invalid path"
msgstr ""

#: portfolio.php:1390
msgid "ATTENTION!"
msgstr ""

#: portfolio.php:1391
msgid ""
"In the current version of Portfolio plugin we updated the Technologies "
"widget. If it was added to the sidebar, it will disappear and you will have "
"to add it again."
msgstr ""

#: portfolio.php:1395
msgid "Read and Understood"
msgstr ""

#, fuzzy
#~ msgid "Plugins page"
#~ msgstr "Plugins recomendados"

#, fuzzy
#~ msgid "Rate the plugin"
#~ msgstr "Plugins recomendados"

#, fuzzy
#~ msgid "Site URL"
#~ msgstr "SVN"

#~ msgid "Installed plugins"
#~ msgstr "Plugins instalados"

#~ msgid "Recommended plugins"
#~ msgstr "Plugins recomendados"

#~ msgid "Download"
#~ msgstr "Descargar"

#~ msgid "Install now from wordpress.org"
#~ msgstr "Instalar nuevo desde wordpress.org"

#, fuzzy
#~ msgid "Active Plugins"
#~ msgstr "Plugins activados"

#, fuzzy
#~ msgid "Inactive Plugins"
#~ msgstr "Plugins activados"

#, fuzzy
#~ msgid "Send to support"
#~ msgstr "Soporte"

#~ msgid "Could not read image size"
#~ msgstr "No puede obtenerse la dimensión de la imágen "

#~ msgid "Could not calculate resized image dimensions"
#~ msgstr "La nueva dimensión de las imágenes no puede calcularse"

#~ msgid "Resize path invalid"
#~ msgstr "Ruta de archivo no válida"

#, fuzzy
#~ msgid ""
#~ "If you have any questions, please contact us via plugin@bestwebsoft.com "
#~ "or fill out the contact form on our website"
#~ msgstr ""
#~ "Si tiene alguna pregunta, por favor pongase en contacto con nosotros "
#~ "mediante plugin@bestwebsoft.com of rellene nuestro formulario de contacto "
#~ "en nuestra web"

#~ msgid "URL of the SVN"
#~ msgstr "URL del SVN"

#~ msgid "Executor Profiles "
#~ msgstr "Profielen van makers"

#~ msgid "The size of the cover album for portfolio"
#~ msgstr "El tamaño de la cubierta del album del portfolio"

#, fuzzy
#~ msgid "Portfolio order by"
#~ msgstr "Información del Portfolio"

#, fuzzy
#~ msgid "Portfolio order"
#~ msgstr "Información del Portfolio"

#~ msgid "No informations found."
#~ msgstr "No se encuentra ninguna información."

#~ msgid "Style for lightbox"
#~ msgstr "Style voor de lightbox"

#~ msgid "Display sidebar"
#~ msgstr "Geeft sidebar weer"

#~ msgid "On frontend portfolio page"
#~ msgstr "Op de portfolio beginpagina"

#~ msgid "On single portfolio page"
#~ msgstr "Op de individuele portfolio pagina"
