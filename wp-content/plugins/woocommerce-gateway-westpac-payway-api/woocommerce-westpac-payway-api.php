<?php
/*
Plugin Name: WooCommerce Westpac PayWay API Gateway
Plugin URI: http://woothemes.com/woocommerce
Description: Use Westpac PayWay API (Westpac Australia) as a credit card processor for WooCommerce.
Version: 1.3.3
Author: Tyson Armstrong
Author URI: http://tysonarmstrong.com/

Copyright: © 2012 Tyson Armstrong

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '6cf86aef9b610239ed70ecd9a2ab069a', '185012' );

add_action('plugins_loaded', 'woocommerce_westpac_payway_init', 0);

function woocommerce_westpac_payway_init() {

	if (!class_exists('WC_Payment_Gateway'))  return;

	class WC_Gateway_Westpac_Payway extends WC_Payment_Gateway_CC {

		private static $log;

		public function __construct() {
			global $woocommerce;

		    $this->id 					= 'westpac_payway_api';
		    $this->method_title 		= __('Westpac PayWay API', 'woo_westpac_api');
			$this->method_description 	= __('Use Westpac PayWay API as a credit card processor for WooCommerce.', 'woo_westpac_api');
			$this->icon 				= WP_PLUGIN_URL . "/" . plugin_basename( dirname(__FILE__)) . '/images/westpac_small.png';

			$this->supports 			= array( 'default_credit_card_form', 'products');
			//$this->supports 			= array( 'subscriptions', 'default_credit_card_form', 'products', 'subscription_cancellation', 'subscription_reactivation', 'subscription_suspension', 'subscription_date_changes','subscription_amount_changes', 'subscription_payment_method_change' );

		    // Load the form fields.
		    $this->init_form_fields();

		    // Load the settings.
		    $this->init_settings();

		    // Define user set variables
		    $this->title = $this->settings['title'];
		    $this->description = __('Credit cards accepted: Visa, Mastercard, Bankcard','woo_westpac_api');
		    if ($this->settings['accept_amex'] == 'yes') $this->description .= ', '.__('American Express','woo_westpac_api');
		    if ($this->settings['accept_diners'] == 'yes') $this->description .= ', '.__('Diners Club','woo_westpac_api');

		    add_action('admin_notices', array(&$this, 'ssl_check'));
		    add_action('admin_notices', array(&$this, 'certificate_date_check'));
			/* 1.6.6 */
			add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
			/* 2.0.0 */
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			if ($this->enabled == 'yes') {
				add_action( 'wp_enqueue_scripts', array(&$this,'load_checkout_script'));

				// Add Card Name field to credit card form
				add_filter('woocommerce_credit_card_form_fields', array($this,'add_card_name_field'));
				add_action( 'wp_enqueue_scripts', array($this,'add_card_name_field_styles'));
			}


			
		}


		/**
		 * Load checkout script to determine which credit card type is being entered
		 */
		function load_checkout_script() {
			if (is_checkout()) {
				wp_enqueue_script('woo_westpac_script',WP_PLUGIN_URL . "/" . plugin_basename( dirname(__FILE__)) . '/woocommerce-westpac-payway-api.js',array('jquery','woocommerce','wc-checkout'),false,true);
				wp_localize_script('woo_westpac_script', 'WooWestpac', array( 'plugin_url' => WP_PLUGIN_URL . "/" . plugin_basename( dirname(__FILE__))));
			}
		}


		/**
         * Display an error if the certificate is out of date or expiring in the next month
         */
		function certificate_date_check() {
			$date = $this->settings['customer-certificate-expiry'];
			if ($date && strtotime($date) < strtotime('+ 1 month')) {
				$daystogo = round((strtotime($date) - time())/86400);
				if ($daystogo > 0) echo '<div class="error"><p>' . sprintf(__('Your Westpac merchant certificate is going to expire in %s days. Download a new certificate file from <a href="https://www.payway.com.au">PayWay</a> and apply it under the Westpac gateway settings.', 'woo_westpac_api'), $daystogo) . '</p></div>';
				if ($daystogo < 1) echo '<div class="error"><p>' . sprintf(__('Your Westpac merchant certificate has expired. Download a new certificate file from <a href="https://payway.com.au">PayWay</a> and apply it under the Westpac gateway settings.', 'woo_westpac_api')) . '</p></div>';
			}
		}

        /**
         * Check if SSL is enabled and notify the user
         * */
        function ssl_check() {
            if (get_option('woocommerce_force_ssl_checkout') == 'no' && $this->enabled == 'yes') :
                echo '<div class="error"><p>' . sprintf(__('Westpac PayWay is enabled, but the <a href="%s">force SSL option</a> is disabled; your checkout is not secure! Please enable SSL and ensure your server has a valid SSL certificate.', 'woo_westpac_api'), admin_url('admin.php?page=woocommerce')) . '</p></div>';
            endif;
        }


		/**
	     * Initialise Gateway Settings Form Fields
		 *
		 * @since 1.0.0
	     */
		function init_form_fields() {
			$this->form_fields = array(
			    'enabled' => array(
			        'title' => __( 'Enable/Disable', 'woo_westpac_api' ),
			        'type' => 'checkbox',
			        'label' => __( 'Enable this payment method', 'woo_westpac_api' ),
			        'default' => 'yes'
			    ),
			    'title' => array(
			        'title' => __( 'Title', 'woo_westpac_api' ),
			        'type' => 'text',
			        'description' => __( 'This controls the title which the user sees during checkout.', 'woo_westpac_api' ),
			        'default' => __( 'Westpac PayWay API', 'woo_westpac_api' )
			    ),
				'testmode' => array(
					'title' => __( 'Test mode', 'woo_westpac_api' ),
					'label' => __( 'Enable Test mode', 'woo_westpac_api' ),
					'type' => 'checkbox',
					'description' => __( 'Process transactions in Test mode. No transactions will actually take place.', 'woo_westpac_api' ),
					'default' => 'yes'
				),
				'customer-username' => array(
					'title' => __( 'Customer username', 'woo_westpac_api' ),
					'type' => 'text',
					'description' => __( 'This can be obtained at <a href="https://www.payway.com.au">PayWay</a> under Setup API > Security.', 'woo_westpac_api' ),
					'default' => ''
				),
				'customer-password' => array(
					'title' => __( 'Customer password', 'woo_westpac_api' ),
					'type' => 'password',
					'description' => __( 'This can be obtained at <a href="https://www.payway.com.au">PayWay</a> under Setup API > Security.', 'woo_westpac_api' ),
					'default' => ''
				),
				'customer-merchant' => array(
					'title' => __( 'Customer merchant', 'woo_westpac_api' ),
					'type' => 'text',
					'description' => __( 'Either use TEST (for test transactions) or your 8-digit live Westpac merchant ID, provided when you go live.', 'woo_westpac_api' ),
					'default' => 'TEST'
				),
				'customer-certificate' => array(
					'title' => __( 'Certificate file', 'woo_westpac_api' ),
					'type' => 'file',
					'description' => __( 'This can be downloaded from <a href="https://www.payway.com.au">PayWay</a> under Setup API > Certificate (select PHP as your API Technology).', 'woo_westpac_api' )
				),
				'customer-certificate-expiry' => array(
					'title' => __( 'Certificate expiry', 'woo_westpac_api' ),
					'type' => 'text',
					'class' => 'date-picker-field datepicker',
					'description' => __( 'This is displayed when you download your certificate. It is optional, however a warning will be displayed a month before your expiry date to help prevent disruptions.', 'woo_westpac_api' )
				),
				'accept_amex' => array(
					'title' => __( 'Accept American Express', 'woo_westpac_api' ),
					'label' => __( 'Accept American Express cards', 'woo_westpac_api' ),
					'type' => 'checkbox',
					'description' => __( 'Contact Westpac PayWay to enable American Express on your account.', 'woo_westpac_api' ),
					'default' => 'no'
				),
				'accept_diners' => array(
					'title' => __( 'Accept Diners Club', 'woo_westpac_api' ),
					'label' => __( 'Accept Diners Club cards', 'woo_westpac_api' ),
					'type' => 'checkbox',
					'description' => __( 'Call Westpac PayWay to activate Diners Club on your account.', 'woo_westpac_api' ),
					'default' => 'no'
				)/*
				'secret_api_key' => array(
					'title' => __( 'Secret API Key', 'woo_westpac_api' ),
					'type' => 'password',
					'description' => __( 'Required if you want to use Subscriptions', 'woo_westpac_api' )
				),
				'publishable_api_key' => array(
					'title' => __( 'Publishable API Key', 'woo_westpac_api' ),
					'type' => 'text',
					'description' => __( 'Required if you want to use Subscriptions', 'woo_westpac_api' )
				) */
			);
		} // End init_form_fields()


		/**
		 * Admin Panel Options
		 *
		 * @since 1.0.0
		 */
		public function admin_options() {
	    	wp_enqueue_script('jquery-ui-datepicker');
	    	$external_ip = $this->get_external_ip();
	    	?>
	    	<h3><?php _e('Westpac PayWay Credit Card Payment', 'woo_westpac_api'); ?></h3>
	    	<p><?php _e('Using the Westpac PayWay payment gateway.', 'woo_westpac_api'); ?></p>
	    	<p><?php _e('Before using, you must add your web server IP address to the <a href="https://www.payway.com.au">PayWay</a> configuration under Setup API > Security. Your server\'s IP address is:', 'woo_westpac_api'); echo ' <strong>'.$external_ip.'</strong>';?></p>
	    	<table class="form-table">
	    	<?php
	    		// Generate the HTML For the settings form.
	    		$this->generate_settings_html();
	    	?>
			</table><!--/.form-table-->
			<script type="text/javascript">
				jQuery(function(){
					jQuery('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
				});
			</script>
	    	<?php
	    } // End admin_options()


		/**
         * Generate file upload input field
         */
	    function generate_file_html ( $key, $data ) {
	    	$html = '';

	    	if ( isset( $data['title'] ) && $data['title'] != '' ) $title = $data['title']; else $title = '';
	    	$data['class'] = (isset( $data['class'] )) ? $data['class'] : '';
	    	$data['css'] = (isset( $data['css'] )) ? $data['css'] : '';

			$html .= '<tr valign="top">' . "\n";
				$html .= '<th scope="row" class="titledesc">';
				$html .= '<label for="' . $this->plugin_id . $this->id . '_' . $key . '">' . $title . '</label>';
				$html .= '</th>' . "\n";
				$html .= '<td class="forminp">' . "\n";
					$html .= '<fieldset><legend class="screen-reader-text"><span>' . $title . '</span></legend>' . "\n";
	                $value = ( isset( $this->settings[ $key ] ) ) ? esc_attr( $this->settings[ $key ] ) : '';
	                if ($value) {
	                	$html .= 'Currently: <strong>'.$value.'</strong><br /> ';
	                }
					$html .= '<input class="input-text wide-input '.$data['class'].'" type="file" name="' . $this->plugin_id . $this->id . '_' . $key . '" id="' . $this->plugin_id . $this->id . '_' . $key . '" style="'.$data['css'].'" value="' . $value . '" />';
					if ( isset( $data['description'] ) && $data['description'] != '' ) { $html .= '<span class="description">' . $data['description'] . '</span>' . "\n"; }
				$html .= '</fieldset>';
				$html .= '</td>' . "\n";
			$html .= '</tr>' . "\n";

	    	return $html;
	    }



		/**
	     * Validate File Field.
	     *
	     * @access public
	     * @param mixed $key
	     * @since 1.0.0
	     * @return string
	     */
	    function validate_file_field ( $key ) {
			if (empty($_FILES[$this->plugin_id . $this->id . '_' . $key]['tmp_name'])) {
				return $this->settings[$key];
			}

			add_filter('upload_mimes', array(&$this,'allow_certificate_upload'));
	    	$upload = wp_upload_bits($_FILES[$this->plugin_id . $this->id . '_' . $key]['name'],null,file_get_contents($_FILES[$this->plugin_id . $this->id . '_' . $key]['tmp_name']));
	    	remove_filter('upload_mimes',array(&$this,'allow_certificate_upload'));

	    	$text = (isset($this->settings[$key])) ? $this->settings[$key] : '';

	    	if ($upload['file']) {
	    		if (chmod($upload['file'],0600)) {
	    			$text = $upload['file'];
	    		} else {
	    			unlink($upload['file']);
	    		}
	    	}

	    	return $text;
	    }


		/**
         * Allow certificate file to be uploaded
         */

		function allow_certificate_upload ( $existing_mimes=array() ) {
			$existing_mimes['pem'] = 'application/octet-stream';
			return $existing_mimes;
		}

		/**
         * Payment Form
         */
        function payment_fields() {
			global $woocommerce;


			// Payment form
			if ($this->settings['testmode']=='yes') : ?><p><?php _e('TEST MODE ENABLED', 'woo_westpac_api'); ?></p><?php endif;
			if ($this->description) { echo '<p>'.$this->description.'</p>'; }

			$timestamp = gmdate('YmdHis'); 
			
			$this->form();

			$this->initialise_api(); /* ?>

			<script type="text/javascript" src="https://api.payway.com.au/rest/v1/creditCardForm.js?apiKey=<?php echo $this->settings['publishable_api_key']; ?>&redirectUrl={redirectUrl}"></script>

		<?php */
        }

		/**
         * Initialise the PayWayAPI
         */
        function initialise_api() {
        	require_once('api/Qvalent_PayWayAPI.php');
        	$uploaddir = wp_upload_dir();
        	$initParams =
		        "certificateFile=".$this->settings['customer-certificate'] . "&" .
		        "caFile=" . dirname(realpath(__FILE__)) ."/api/cacerts.crt" . "&" .
		        "logDirectory=".$uploaddir['basedir'];
		    $this->paywayAPI = new Qvalent_PayWayAPI();
		    $this->paywayAPI->initialise( $initParams );
        }



		/**
		 * Process the payment and return the result
		 * - redirects the customer to the pay page
		 *
		 * @since 1.0.0
		 */
		function process_payment( $order_id ) {
			global $woocommerce;
			$order = new WC_Order( $order_id );

			// New Subscriptions based
			/* if (class_exists('WC_Subscriptions_Order') && WC_Subscriptions_Order::order_contains_subscription($order)) {
				$amount = WC_Subscriptions_Order::get_total_initial_payment( $order );
			} */

        	$this->initialise_api();

		    //----------------------------------------------------------------------------
		    // Process credit card request
		    //----------------------------------------------------------------------------

		    $requestParameters = array();
		    $requestParameters[ "order.type" ] = "capture";
		    $requestParameters[ "customer.username" ] = $this->settings['customer-username'];
		    $requestParameters[ "customer.password" ] = $this->settings['customer-password'];
		    $requestParameters[ "customer.merchant" ] = ($this->settings['testmode'] == 'yes') ? 'TEST' : $this->settings['customer-merchant'];
		    $requestParameters[ "customer.orderNumber" ] = substr('O'.$order->get_order_number().'_'.substr(time(),-5),0,20);
		    $requestParameters[ "card.PAN" ] = isset($_POST['westpac_payway_api-card-number']) ? woocommerce_clean($_POST['westpac_payway_api-card-number']) : '';
		    $requestParameters[ "card.CVN" ] = isset($_POST['westpac_payway_api-card-cvc']) ? woocommerce_clean($_POST['westpac_payway_api-card-cvc']) : '';

		    // Split month and year
		    $month = '';
		    $year = '';
		    if (isset($_POST['westpac_payway_api-card-expiry'])) {
		    	$dateparts = explode(' / ', $_POST['westpac_payway_api-card-expiry']);
		    	if (count($dateparts) == 2) {
		    		list($month, $year) = $dateparts;
		    		if (strlen($year) == 4) {
		    			$year = substr($year,2,2);
		    		}
		    	}
		    }
		    $requestParameters[ "card.expiryYear" ] = woocommerce_clean($year);
		    $requestParameters[ "card.expiryMonth" ] = woocommerce_clean($month);
		    $requestParameters[ "card.currency" ] = get_woocommerce_currency();
		    $requestParameters[ "card.cardHolderName" ] = isset($_POST['westpac_payway_api-card-name']) ? str_replace(array('+','&','%'),'',substr(woocommerce_clean($_POST['westpac_payway_api-card-name']),0,60)) : '';
		    $requestParameters[ "order.amount" ] = number_format( (float)$order->order_total * 100, 0, '.', '' );
		    $requestParameters[ "order.ipAddress" ] = $this->get_user_ip();
		    $requestParameters[ "order.ECI" ] = "SSL";

		    $requestText = $this->paywayAPI->formatRequestParameters( $requestParameters );

		    $responseText = $this->paywayAPI->processCreditCard( $requestText );


			// Parse the response string into an array
		    $responseParameters = $this->paywayAPI->parseResponseParameters( $responseText );


		    switch ($responseParameters['response.summaryCode']) {
		    	case '0' :  // Success!

		    		$order->add_order_note(sprintf(__('Westpac PayWay payment on %s approved at %s. Reference ID: %s','woo_westpac_api'),$responseParameters[ "response.cardSchemeName" ],$responseParameters[ "response.transactionDate" ],$responseParameters[ "response.receiptNo" ]));
		    		$order->payment_complete();
		    		$woocommerce->cart->empty_cart();

		    		if (is_woocommerce_pre_2_1()) {
						$url = add_query_arg('key', $order->order_key, add_query_arg('order', $order_id, get_permalink(get_option('woocommerce_thanks_page_id'))));
					} else {
						$url = $order->get_checkout_order_received_url();
					}

					//$this->log(sprintf(__('Westpac PayWay payment on %s approved for order %s at %s. Reference ID: %s', 'woo_westpac_api'),$responseParameters[ "response.cardSchemeName" ],$order->get_order_number(),$responseParameters[ "response.transactionDate" ],$responseParameters[ "response.receiptNo" ]));

	                return array(
	                    'result' => 'success',
	                    'redirect' => $url
	                );
	                break;
	            case '2' : // Errored - need to ensure the payment is voided
	            	$status = 'failed';
	            	$order->add_order_note(sprintf(__('Westpac Payway payment failed due to a network problem. (ref ID: %s). "%s"', 'woo_westpac_api'), $responseParameters[ "response.receiptNo" ], $responseParameters[ "response.text" ] . ' (' . $responseParameters[ "response.responseCode" ] . ')'));
                    wc_add_notice(__('Payment error: ', 'woo_westpac_api') . $responseParameters[ "response.text" ],'error');
                    $this->log(sprintf(__('Westpac PayWay API payment error for order %s. Response from bank: %s', 'woo_westpac_api'),$order->get_order_number(),$responseParameters[ "response.text" ]));

                    // Also need to reverse payment, just in case.
                    $requestParameters[ "order.type" ] = "reversal";
                    $requestParameters[ "customer.originalOrderNumber" ] = substr('ORDER'.$order->get_order_number().'_'.time(),0,20);
                    $requestText = $this->paywayAPI->formatRequestParameters( $requestParameters );
                    $responseText = $this->paywayAPI->processCreditCard( $requestText );
                    $responseParameters = $this->paywayAPI->parseResponseParameters( $responseText );
                    if ($responseParameters['response.responseCode'] == '00' || $responseParameters['response.responseCode'] == '21') {
                    	$order->add_order_note(sprintf(__('Confirmed that the transaction has not been applied (the customer has not been charged).', 'woo_westpac_api') ));
                    	wc_add_notice(__('You have not been charged for this transaction.', 'woo_westpac_api'),'error');
                    	$this->log(sprintf(__('Confirmed that the transaction has not been applied (the customer has not been charged) for order %s', 'woo_westpac_api'),$order->get_order_number()));
                    } else {
                    	$order->add_order_note(sprintf(__('Unable to confirm that the transaction was not completed. The customer may have been charged. Please check transaction ID: ', 'woo_westpac_api').$responseParameters["response.receiptNo"] ));
                    	$this->log(sprintf(__('Unable to confirm that the transaction was not completed for order %s. The customer may have been charged. Please check transaction ID: %s', 'woo_westpac_api'),$order->get_order_number(),$responseParameters["response.receiptNo"]));
                    }

	            default: // failed (could be 1: declined or 3: rejected)
	            	$order->add_order_note(sprintf(__('Westpac Payway payment failed. Payment was rejected due to an error: %s', 'woo_westpac_api'), $responseParameters[ "response.text" ] . ' (' . $responseParameters[ "response.responseCode" ] . ')'));
                    wc_add_notice(__('Payment error: ', 'woo_westpac_api') . $responseParameters[ "response.text" ],'error');
                    $this->log(sprintf(__('Westpac Payway payment failed for order %s. Payment was rejected due to an error: %s (%s)', 'woo_westpac_api'),$order->get_order_number(),$responseParameters["response.text"], $responseParameters["response.responseCode"]));
                    return;
                    break;
		    }
		}

		/**
         * Add a Card Name field to the default WooCommerce checkout form (and make it first)
         */
        function add_card_name_field($default_fields) {
        	$fields = array_merge(array('card-name-field' => '<p class="form-row form-row-wide">
				<label for="' . esc_attr( $this->id ) . '-card-name">' . __( 'Name on card', 'woo_westpac_api' ) . ' <span class="required">*</span></label>
				<input id="' . esc_attr( $this->id ) . '-card-name" class="input-text wc-credit-card-form-card-name" type="text" autocomplete="off" placeholder="" name="' . $this->id . '-card-name' . '" />
			</p>'),$default_fields);
        	return $fields;
        }

        function add_card_name_field_styles() {
        	if (is_checkout()) {
				wp_register_style( 'woocommerce-westpac-payway-api', plugin_dir_url(__FILE__) . 'woocommerce-westpac-payway-api.css' );
				wp_enqueue_style( 'woocommerce-westpac-payway-api' );
			}
        }


		/**
         * Get user's IP address
         */
        function get_user_ip() {
            $ip = (isset($_SERVER['HTTP_X_FORWARD_FOR']) && !empty($_SERVER['HTTP_X_FORWARD_FOR'])) ? $_SERVER['HTTP_X_FORWARD_FOR'] : $_SERVER['REMOTE_ADDR'];
            if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            	$ip = '';
            }
            return $ip;
        }

		/**
         * Get server's IP address to assist with the whitelist process
         */
		function get_external_ip() {
			$external_ip = get_option('_external_ip');
			$external_ip_last_updated = get_option('_external_ip_last_updated');
			if ($external_ip == false || $external_ip_last_updated < strtotime('- 15 minutes')) {
				// Check IP
				$ch = curl_init( 'http://ifconfig.me/ip' );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
				curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
				$responseText = curl_exec($ch);
				curl_close( $ch );
				if ($responseText) update_option('_external_ip',trim($responseText));
				update_option('_external_ip_last_updated',time());
				return trim($responseText);
			} else {
				// Here's the stored IP
				return $external_ip;
			}
			return false;
		}

		public static function log( $message ) {
			if ( empty( self::$log ) ) {
				self::$log = new WC_Logger();
			}

			self::$log->add( 'woo_westpac_api', $message );

		}

	}

	if (!function_exists('is_woocommerce_pre_2_1')) {
		function is_woocommerce_pre_2_1() {
	        if ( ! defined( 'WC_VERSION' ) ) {
	            $woocommerce_is_pre_2_1 = true;
	        } else {
	            $woocommerce_is_pre_2_1 = false;
	        }
	        return $woocommerce_is_pre_2_1;
	    }
	}

	/**
	 * Add the Westpac PayWay DP gateway to WooCommerce
	 *
	 * @since 1.0.0
	 **/
	function add_westpac_payway_gateway( $methods ) {
		$methods[] = 'WC_Gateway_Westpac_Payway';
		return $methods;
	}
	add_filter('woocommerce_payment_gateways', 'add_westpac_payway_gateway' );
}