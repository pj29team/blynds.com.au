jQuery(document).ready(function() {
	jQuery('input#place_order').attr('disabled', 'disabled');
	jQuery('body.woocommerce-checkout').on('keyup','input[id^=westpac_payway_api-card]',function() {
            setButtonStatus();
	});
      jQuery('body.woocommerce-checkout').on('updated_checkout',function() {
            setButtonStatus();
      });
      jQuery('body.woocommerce-checkout').on('change','input[name=payment_method]',function() {
            setButtonStatus();      
      });

      function setButtonStatus() {
            if (areFieldsValid() || !jQuery('#payment_method_westpac_payway_api').is(':checked')) {
                  jQuery('input#place_order').removeAttr('disabled');
            } else {
                  jQuery('input#place_order').attr('disabled', 'disabled');
            }
      }

      function areFieldsValid() {
            var isValid = true;

            var number = jQuery('input#westpac_payway_api-card-number').val();
            number = number.replace(/[^0-9]/g, '');

            var validCardType = false;

            var re = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
            if (number.match(re) != null) {
                  validCardType = true;
            }
            re = new RegExp("^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$");
            if (number.match(re) != null) {
                  validCardType = true;
            }
            re = new RegExp("^3[47][0-9]{13}$");
            if (number.match(re) != null) {
                  validCardType = true;
            }
            re = new RegExp("^3(?:0[0-5]|[68][0-9])[0-9]{11}$");
            if (number.match(re) != null) {
                  validCardType = true;
            }

            // Check the expiry
            var validExpiry = false;
            if (jQuery('input#westpac_payway_api-card-expiry').val().length == 7 || jQuery('input#westpac_payway_api-card-expiry').val().length == 9) {
                  validExpiry = true;
            }

            // Check the CCV
            var validSecurityCode = false;
            if (jQuery('input#westpac_payway_api-card-cvc').val().length == 3 || jQuery('input#westpac_payway_api-card-cvc').val().length == 4) {
                  validSecurityCode = true;
            }

            // Check the card name
            var validCardName = false;
            if (jQuery('input#westpac_payway_api-card-name').val().length > 2) {
                  validCardName = true;
            }

            if (validCardType && validExpiry && validSecurityCode && validCardName) {
                  return true;
            }
            return false;
      }
});