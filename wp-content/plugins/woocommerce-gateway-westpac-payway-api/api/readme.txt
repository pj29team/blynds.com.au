Copyright (c) 2005 Qvalent Pty Ltd.

Qvalent PayWay Cards API for PHP
================================

1. Requirements

PHP 4 or higher is required.  You must install the libcurl package for PHP.
See http://www.php.net/manual/en/ref.curl.php


2. Using the Example Code

* Copy the "Qvalent_PayWayAPI.php" file and the contents of the 
"examples" directory to your web directory.

* Download your certificate and save it on your server.  Record this location 
for use in the initialisation of the Qvalent_PayWayAPI object.  Update this
value in the processCard.php file.

* Copy the cacerts.crt Certificate Authority file to a secure location on your 
server.  Record this location for use in the initialisation of the 
Qvalent_PayWayAPI object.  Update this value in the processCard.php file.

* Choose a log directory and ensure that your web application can write to that
directory on the server.  Use this location in the initialisation of the 
Qvalent_PayWayAPI object.  Update this value in the processCard.php file.

* Enter your username and password in the processCard.php file.

* Using your web browser, browse to index.htm, then press the Process Capture
button.  You should receive a successful response from the Qvalent payment server.
