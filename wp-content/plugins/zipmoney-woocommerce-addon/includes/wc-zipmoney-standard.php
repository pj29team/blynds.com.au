<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Class for handling standard checkout flow.
 *
 * @class    WC_ZipMoney_Standard
 * @version  1.1.0
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */

class WC_ZipMoney_Standard extends WC_ZipMoney
{

  /**
   * Handles standard checkout. Makes a request to /checkout endpoint. 
   * Returns redirect_url when successful.
   *
   * @access public
   * @return mixed boolean | string $redirect_url
   */
	public function handleCheckout( $order )
	{
		global $woocommerce;
		
		$requestArray = $this->_prepareRequestForCheckout( $order );
			
		$this->log( "Request:- " . json_encode($requestArray ));		

		try{

			$response  = $this->_api->checkout($requestArray);

			$this->log( "Response:- " . json_encode($response->toArray() ));
			
			if($response->isSuccess())		
		    	return  $response->getRedirectUrl();
		    else
		    	return false;

		} catch (Exception $e){

			throw new ZipMoney_Exception($e->getMessage());

		} 

	}
  
  /**
   * Makes a request to /checkout endpoint through the handleCheckout() method. 
   * Prints json response.
   *
   * @access public
   */
  public  function getredirecturl()
  {
    $redirect_url = $this->handleCheckout();
    $json = array('redirect_url' => $redirect_url);    
    header("Content-type:application/json");
    die(json_encode($json)); 
  }
 

  /**
   * Handles order refunds. Makes a request to /refund endpoint. 
   * Returns redirect_url when successful.
   *
   * @access public   
   * @param  WC_Order $order, float $amount, string $reason (optional)
   * @return mixed boolean | ZipMoney_Response object 
   */
	public function handleRefund( $order ,$amount, $reason = null)
	{	
		global $woocommerce;
		
		$requestArray = $this->_prepareRequestForRefund( $order ,$amount, $reason);

		$this->log( "Request:- ". json_encode($requestArray ));		

		try{

			$response  = $this->_api->refund($requestArray);

			$this->log( "Response:- ".json_encode( $response->toArray() ));
			
			if($response->isSuccess())		
		    return $response->toObject();
		  else
		    return false;

		} catch (Exception $e){

			throw new ZipMoney_Exception($e->getMessage());
		} 
	}

 /**
   * Prepares $requestArray array for refund
   *
   * @access public   
   * @param  WC_Order $order, float $amount, string $reason (optional)
   * @return array requestArray 
   */
	protected function _prepareRequestForRefund( $order , $refund_amount, $refund_reason = null)
	{
		global $woocommerce;

		$requestArray = array();
		
		// Set Basic required info
		$requestArray['reason'] 	   = $refund_reason;
		$requestArray['refund_amount'] = $refund_amount;

		$requestArray['order_id']	   = (string)$order->id;
		$requestArray['txn_id']	 	   = (string)$order->get_transaction_id(); 

		$reference 					   = md5(rand(0, 9999999));
		$requestArray['reference']	   = $reference;

		update_post_meta($order->id,'_zipmoney_refund_reference',$reference);
		
		// Set Callback Urls
		$requestArray = $this->_setCallbackUrls($requestArray,$order);

		// Set the order info
		$requestArray['order']    = $this->_setOrder($order);
 		// Set Version Info
		$requestArray['version']  = array( 'client' => self::CLIENT, "platform" => self::PLATFORM );
		// Set Metadata
		$requestArray['metadata'] = array( 'order_reference' => $reference);

	 return $requestArray;
	}

 /**
   * Prepares $requestArray array for checkout
   *
   * @access public   
   * @param  WC_Order $order
   * @return array $requestArray 
   */
	protected function _prepareRequestForCheckout( $order )
	{
		global $woocommerce;

		$requestArray = array();
		
		// Returns the checkout object
		$checkout     = $woocommerce->checkout();

		// Set Basic required info
		$requestArray['charge'] 	   = $this->is_capture()?"true":"false";
		$requestArray['currency_code'] = get_woocommerce_currency();
		$requestArray['order_id']	   = (string)$order->id;
		$requestArray['txn_id']	 	   = 0; // Not required for checkout
		$requestArray['in_store']	   = false;
		// Set Callback Urls
		$requestArray = $this->_setCallbackUrls($requestArray,$order);
		
		if($consumer = $this->_setCustomerInfo($order)){
			$requestArray['consumer']  = $consumer;
		}

		if($billing_info = $this->_setBillingInfo($order)){
			// Set the billing info
			$requestArray['billing_address']  = $this->_setBillingInfo($order);		
			// Set the shipping info
			$requestArray['shipping_address'] = $this->_setBillingInfo($order);
		}	

		// If different shipping address is provided
		if($checkout->posted['ship_to_different_address'] && $shipping = $this->_setShippingInfo($order) )  $requestArray['shipping_address'] = $shipping;

		// Set the order info
		$requestArray['order']    = $this->_setOrder( $order );
 		// Set Version Info
		$requestArray['version']  = array( 'client' => self::CLIENT, "platform" => self::PLATFORM );
		// Set Metadata
		$requestArray['metadata'] = array( 'order_reference' => null);

	 return $requestArray;
	}

} 