<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Comments
 *
 * Base Class for Standard and Express Checkout classes
 *
 * @class    WC_ZipMoney
 * @version  1.1.0
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */
class WC_ZipMoney
{

    protected $_api = null;

    protected $_gateway = null;

    const    VERSION = '1.0.0';

    const    PLATFORM = 'Woocommerce';

    const    CLIENT = 'WooCommerce ZipMoney Payment API';

    /**
     *
     * @param  WC_ZipMoney_Payment $gateway
     */
    public function __construct($gateway)
    {
        $this->_api = new ZipMoney_Api($gateway->merchant_id, $gateway->merchant_key, $gateway->environment);

        $this->_gateway = $gateway;

    }

    /**
     * Sets callback urls to the request array
     *
     * @access protected
     * @param  array $requestArray , WC_Order $order_quote
     * @return array $requestArray
     */
    protected function _setCallbackUrls($requestArray, $order_quote)
    {
        global $woocommerce;

        // Set callback Urls
        $requestArray['cancel_url'] = $order_quote->get_cancel_order_url();
        $requestArray['error_url'] = $this->_gateway->get_error_url($order_quote);
        $requestArray['decline_url'] = $woocommerce->cart->get_cart_url();
        $requestArray['cart_url'] = $woocommerce->cart->get_cart_url();
        $requestArray['success_url'] = $this->_gateway->get_return_url($order_quote);
        $requestArray['refer_url'] = $this->_gateway->get_return_url($order_quote);

        return $requestArray;
    }

    /**
     * Sets callback urls to the request array
     *
     * @access protected
     * @param array $requestArray , WC_Order $order_quote
     * @return array $requestArray
     */
    protected function _setOrder($order_quote)
    {
        $order = array();

        $order['id'] = $order_quote->id;
        $order['tax'] = $order_quote->get_total_tax();
        $order['shipping_value'] = $order_quote->get_total_shipping();
        $order['total'] = (float)$order_quote->get_total();

        if ($items = $order_quote->get_items()) {

            foreach ($items as $item_id => $item) {
                $product = new WC_Product($item['product_id']);

                $order['detail'][] = array(
                    'quantity' => $item['qty'],
                    'name' => $item['name'],
                    'price' => get_post_meta($item['product_id'], '_price', true),
                    'description' => $product->post->post_excerpt,
                    'sku' => $product->get_sku(),
                    'id' => $item_id,
                    'category' => strip_tags($product->get_categories()),
                    'image_url' => wp_get_attachment_url($product->get_image_id())
                );
            }
        }

        return $order;
    }


    /**
     * Sets billing info to the request array
     *
     * @access protected
     * @param  WC_Order $order_quote
     * @return array $billing_address
     */
    protected function _setBillingInfo($order_quote)
    {
        $billing_address = $order_quote->get_address("billing");

        if (empty($billing_address['address_1']))
            return false;

        $billing_address = array(
            'first_name' => $billing_address['first_name'],
            'last_name' => $billing_address['last_name'],
            'line1' => $billing_address['address_1'],
            'line2' => $billing_address['address_2'],
            'country' => $billing_address['country'],
            'zip' => $billing_address['postcode'],
            'city' => $billing_address['city'],
            'state' => $billing_address['state']
        );


        return $billing_address;
    }

    /**
     * Sets shipping info to the request array
     *
     * @access protected
     * @param  WC_Order $order_quote
     * @return array $shipping_address
     */
    protected function _setShippingInfo($order_quote)
    {
        $shipping_address = $order_quote->get_address("shipping");

        if (empty($shipping_address['address_1']))
            return false;

        $shipping_address = array(
            'first_name' => $shipping_address['first_name'],
            'last_name' => $shipping_address['last_name'],
            'line1' => $shipping_address['address_1'],
            'line2' => $shipping_address['address_2'],
            'country' => $shipping_address['country'],
            'zip' => $shipping_address['postcode'],
            'city' => $shipping_address['city'],
            'state' => $shipping_address['state']
        );

        return $shipping_address;
    }

    /**
     * Sets customer info to the request array
     *
     * @access protected
     * @param  WC_Order $order_quote
     * @return array $consumer
     */
    protected function _setCustomerInfo($order_quote)
    {
        $lifetime_sales_refunded_amount = 0;
        $lifetime_sales_amount = 0;
        $average_sale_value = 0;
        $maximum_sale_value = 0;
        $i = 0;

        $consumer['gender'] = "0";

        if ($order_quote) {
            $consumer['email'] = $order_quote->billing_email;
            $consumer['first_name'] = $order_quote->billing_first_name;
            $consumer['last_name'] = $order_quote->billing_last_name;
            $consumer['phone'] = $order_quote->billing_phone;
        }

        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            $created_at = $current_user->data->user_registered;

            $customer_orders = get_posts(array(
                'numberposts' => -1,
                'meta_key' => '_customer_user',
                'meta_value' => get_current_user_id(),
                'post_type' => wc_get_order_types(),
                'post_status' => array('wc-completed', 'wc-refunded'),
            ));

            $lifetime_sales_amount = 0;
            if (is_array($customer_orders)) {
                foreach ($customer_orders as $post) {
                    $order = new WC_Order($post->ID);
                    $order_total = $order->get_total();

                    if ($order->get_status() == 'completed') {
                        $i++;
                        $lifetime_sales_amount += $order_total;

                        if ($order_total > $maximum_sale_value)
                            $maximum_sale_value = $order_total;

                    } else if ($order->get_status() == 'refunded') {
                        $lifetime_sales_refunded_amount += $order_total;
                    }
                }
            }

            if ($i > 0)
                $average_sale_value = (float)round($lifetime_sales_amount / $i, 2);

            if ($current_user->user_firstname)
                $consumer['first_name'] = $current_user->user_firstname;

            if ($current_user->user_lastname)
                $consumer['last_name'] = $current_user->user_lastname;

            if ($current_user->user_email)
                $consumer['email'] = $current_user->user_email;

            $consumer['email'] = $current_user->data->user_email;
            $consumer['first_name'] = get_user_meta($current_user->ID, 'first_name', true);
            $consumer['last_name'] = get_user_meta($current_user->ID, 'last_name', true);
        }

        if (empty($consumer['email']) || !isset($consumer['email']))
            return false;

        $consumer['lifetime_sales_amount'] = (float)$lifetime_sales_amount;
        $consumer['lifetime_sales_units'] = (int)$i;
        $consumer['average_sale_value'] = (float)$average_sale_value;
        $consumer['maximum_sale_value'] = (float)$maximum_sale_value;
        $consumer['lifetime_sales_refunded_amount'] = (float)$lifetime_sales_refunded_amount;

        if (is_checkout())
            $consumer['phone'] = $order_quote->billing_phone;

        return $consumer;
    }

    /**
     * Renders error page
     *
     * @access public
     */
    public function error()
    {
        get_header();
        ?>
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <section class="error-404 not-found">
                    <header class="page-header">
                        <h1 class="page-title"><?php _e('An error occurred'); ?></h1>
                    </header>

                    <div class="page-content">
                        <h2><?php _e('This is somewhat embarrassing, isn&rsquo;t it?'); ?></h2>
                        <p><?php _e('It looks like an error occurred while processing your transaction. Please contact zipMoney for futher details.'); ?></p>
                    </div><!-- .page-content -->
                </section><!-- .error-404 -->

            </main><!-- .site-main -->
        </div><!-- .content-area -->
        <?php get_footer();
    }

    /**
     * Logging method
     *
     * @access public
     * @param  string $message , boolean $override_log_enabled (optional)
     */
    public function log($message, $override_log_enabled = false)
    {
        if (is_array($message) || is_object($message))
            $message = print_r($message, true);

        if (isset($this)) {
            if ((isset($this->_gateway->debug) && $this->_gateway->debug) || $override_log_enabled) {
                if (empty($this->log))
                    $this->log = new WC_Logger();

                $this->log->add('zipmoney', $message);
            }

        } else {

            if ($override_log_enabled) {
                $log = new WC_Logger();

                $log->add('zipmoney', $message);
            }
        }
    }

    /**
     * This function is a static function to write the log if necessary.
     *
     * There are lot of places in this plugin to call WC_ZipMoney::staticLog(). But this method is not static. As a result,
     * some compiler will throw exception as it calls a non static function
     *
     * @param $message
     * @param bool $override_log_enabled
     */
    public static function staticLog($message, $override_log_enabled = false)
    {
        if($override_log_enabled == false){
            return;
        }

        if (is_array($message) || is_object($message)){
            $message = print_r($message, true);
        }

        $log = new WC_Logger();
        $log->add('zipmoney', $message);
    }

    /**
     * Sets customer info to the request array
     *
     * @access public
     * @return boolean
     */
    public function handleConfigUpdate()
    {
        global $woocommerce;

        $requestArray['version'] = array('client' => self::CLIENT, "platform" => self::PLATFORM);

        self::log("Request:-\n");
        self::log($requestArray);

        try {

            $response = $this->_api->settings($requestArray);
            $settings = $response->toArray();

            self::log("Response:-\n");
            self::log($settings);

            if ($response->isSuccess()) {
                $options = array();

                $options['product'] = $settings['Settings']['product'];
                $options['capture_method'] = $settings['Settings']['capture_method'];
                $options['log_enabled'] = $settings['Settings']['log_enabled'];
                $options['log_level'] = $settings['Settings']['log_level'];
                $options['timeout'] = $settings['Settings']['timeout'];
                $options['title'] = $settings['Settings']['title'];
                $options['checkout_title'] = $settings['Settings']['checkout_title'];
                $options['checkout_description'] = $settings['Settings']['checkout_description'];
                $options['merchant_public_key'] = $settings['Settings']['merchant_public_key'];

                $options['assets'] = $settings['assets'];
                $options['asset_values'] = $settings['asset_values'];

                update_option($this->_gateway->plugin_id . $this->_gateway->id . '_api_settings', $options);

                $this->_handleBaseUrlUpdate();

                return true;
            } else
                return false;

        } catch (Exception $e) {
            //Logging
            self::log($e->getMessage());

            return false;
        }
    }

    /**
     * Sets customer info to the request array
     *
     * @access private
     * @return boolean
     */
    private function _handleBaseUrlUpdate()
    {

        global $woocommerce;

        $requestArray = array('base_url' => get_site_url() . "/");

        $requestArray['version'] = array('client' => self::CLIENT, "platform" => self::PLATFORM);

        self::log("\Request:-\n");
        self::log($requestArray);

        try {

            $response = $this->_api->configure($requestArray);
            $settings = $response->toArray();

            self::log("\Response:-\n");
            self::log($settings);

            if ($response->isSuccess())
                return true;
            else
                return false;

        } catch (Exception $e) {
            //Logging
            self::log($e->getMessage());

            return false;
        }

    }

    /**
     * Checks if the express flow is enabled
     *
     * @access public
     * @return boolean
     */
    public function is_express()
    {
        return (boolean)$this->_gateway->is_express;
    }

    /**
     * Checks if the capture is set
     *
     * @access public
     * @return boolean
     */
    public function is_capture()
    {
        return isset($this->_gateway->api_settings['capture_method']) &&
        ($this->_gateway->api_settings['capture_method'] == 0 || $this->_gateway->api_settings['capture_method'] == '0') ? true : false;
    }
} 
