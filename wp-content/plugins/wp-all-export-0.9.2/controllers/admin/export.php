<?php 
/**
 * Import configuration wizard
 * 
 * @author Pavel Kulbakin <p.kulbakin@gmail.com>
 */

class PMXE_Admin_Export extends PMXE_Controller_Admin {
	
	protected $isWizard = true; // indicates whether controller is in wizard mode (otherwize it called to be deligated an edit action)	

	protected function init() {		

		parent::init();							
		
		if ('PMXE_Admin_Manage' == PMXE_Plugin::getInstance()->getAdminCurrentScreen()->base) { // prereqisites are not checked when flow control is deligated
			$id = $this->input->get('id');
			$this->data['export'] = $export = new PMXE_Export_Record();			
			if ( ! $id or $export->getById($id)->isEmpty()) { // specified import is not found
				wp_redirect(add_query_arg('page', 'pmxe-admin-manage', admin_url('admin.php'))); die();
			}
			$this->isWizard = false;		
		
		} else {						
			$action = PMXE_Plugin::getInstance()->getAdminCurrentScreen()->action; 
			$this->_step_ready($action);			
		}

		// preserve id parameter as part of baseUrl
		$id = $this->input->get('id') and $this->baseUrl = add_query_arg('id', $id, $this->baseUrl);					

	}

	public function set($var, $val)
	{
		$this->{$var} = $val;
	}
	public function get($var)
	{
		return $this->{$var};
	} 

	/**
	 * Checks whether corresponding step of wizard is complete
	 * @param string $action
	 */
	protected function _step_ready($action) {		

		// step #1: xml selction - has no prerequisites
		if ('index' == $action) return true;
				
		if ('element' == $action) return true;

		$this->data['update_previous'] = $update_previous = new PMXE_Export_Record();

		$update_previous->getById(PMXE_Plugin::$session->update_previous);

		if ('options' == $action) return true;

		if ( ! PMXE_Plugin::$session->has_session()){
			wp_redirect_or_javascript($this->baseUrl); die();
		}

		if ('process' == $action) return true;
		
	}
	
	/**
	 * Step #1: Choose CPT
	 */
	public function index() {	

		PMXE_Plugin::$session->clean_session();	

		$wp_uploads = wp_upload_dir();		
				
		$this->data['post'] = $post = $this->input->post(array(
			'cpt' => array(),		
			'export_to' => 'xml',
			'export_type' => 'specific',
			'wp_query' => ''
		));		

		// Delete history
		$history_files = PMXE_Helper::safe_glob(PMXE_ROOT_DIR . '/history/*', PMXE_Helper::GLOB_RECURSE | PMXE_Helper::GLOB_PATH);
		if ( ! empty($history_files) ){ 
			foreach ($history_files as $filePath) {
				@file_exists($filePath) and @unlink($filePath);		
			}
		}

		if ($this->input->post('is_submitted')){  																											

			PMXE_Plugin::$session->set('export_type', $post['export_type']);			

			if ('advanced' == $post['export_type']) { 

				if( "" == $post['wp_query'] ){
					$this->errors->add('form-validation', __('WP Query field is required', 'pmxe_plugin'));
				}
				else {

					$exportQuery = eval('return new WP_Query(array(' . $post['wp_query'] . ', \'offset\' => 0, \'posts_per_page\' => 1));');
					
					if ( empty($exportQuery) ) {
						$this->errors->add('form-validation', __('Invalid query', 'pmxe_plugin'));		
					}
					elseif ( empty($exportQuery->found_posts) ) {
						$this->errors->add('form-validation', __('No matching posts found for WP_Query expression specified', 'pmxe_plugin'));		
					}
					else {												
						PMXE_Plugin::$session->set('wp_query', $post['wp_query']);
						PMXE_Plugin::$session->set('found_posts', $exportQuery->found_posts);										
					}

				}
			}
			else {

				$post_types = ( ! is_array($post['cpt']) ) ? array($post['cpt']) : $post['cpt'];								

				if (in_array('product', $post_types)) $post_types[] = 'product_variation';				

				$exportQuery = new WP_Query( array( 'post_type' => $post_types, 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => 1 ));		

				if (empty($exportQuery->found_posts)){
					$this->errors->add('form-validation', __('No matching posts found for selected post types', 'pmxe_plugin'));	
				}
				else{
					PMXE_Plugin::$session->set('cpt', $post_types);	
					PMXE_Plugin::$session->set('found_posts', $exportQuery->found_posts);										
				}
			}			

		} 	
		
		if ($this->input->post('is_submitted') and ! $this->errors->get_error_codes()) {			
				
			check_admin_referer('choose-cpt', '_wpnonce_choose-cpt');					 																		

			PMXE_Plugin::$session->save_data(); 				
								
			wp_redirect(add_query_arg('action', 'template', $this->baseUrl)); die();												
			
		}
		
		$this->render();
	}		

	/**
	 * Step #2: Template
	 */ 
	public function template(){

		$default = PMXE_Plugin::get_default_import_options();

		if ($this->isWizard) {	
			$DefaultOptions = (PMXE_Plugin::$session->has_session() ? PMXE_Plugin::$session->get_clear_session_data() : array()) + $default;
			$post = $this->input->post($DefaultOptions);					
		}
		else{
			$DefaultOptions = $this->data['export']->options + $default;
			$post = $this->input->post($DefaultOptions);			
			$post['scheduled'] = $this->data['export']->scheduled;

			foreach ($post as $key => $value) {
				PMXE_Plugin::$session->set($key, $value);
			}
			
		}

		if ('advanced' == $post['export_type']) { 

			if( "" == $post['wp_query'] ){
				 $this->errors->add('form-validation', __('WP Query field is required', 'pmxe_plugin'));
			}
			else {

				$exportQuery = eval('return new WP_Query(array(' . $post['wp_query'] . ', \'orderby\' => \'ID\', \'order\' => \'ASC\', \'offset\' => 0, \'posts_per_page\' => 10));');
				
				if ( empty($exportQuery) ) {
					$this->errors->add('form-validation', __('Invalid query', 'pmxe_plugin'));		
				}
				elseif ( empty($exportQuery->found_posts) ) {
					$this->errors->add('form-validation', __('No matching posts found for WP_Query expression specified', 'pmxe_plugin'));		
				}
				else {						
					PMXE_Plugin::$session->set('found_posts', $exportQuery->found_posts);										
				}

			}
		}
		else {

			$post_types = ( ! is_array($post['cpt']) ) ? array($post['cpt']) : $post['cpt'];								

			if (in_array('product', $post_types)) $post_types[] = 'product_variation';				

			$exportQuery = new WP_Query( array( 'post_type' => $post_types, 'orderby' => 'ID', 'order' => 'ASC', 'posts_per_page' => 10 ));		

			if (empty($exportQuery->found_posts)){
				$this->errors->add('form-validation', __('No matching posts found for selected post types', 'pmxe_plugin'));	
			}
			else{					
				PMXE_Plugin::$session->set('found_posts', $exportQuery->found_posts);										
			}
		}			

		PMXE_Plugin::$session->save_data(); 

		$this->data['post'] =& $post;								
		
		if ($this->input->post('is_submitted')) {

			check_admin_referer('template', '_wpnonce_template');

			if ( empty($post['cc_type'][0]) ){
				$this->errors->add('form-validation', __('You haven\'t selected any columns for export.', 'pmxe_plugin'));
			}	

			if ( 'csv' == $post['export_to'] and '' == $post['delimiter'] ){
				$this->errors->add('form-validation', __('CSV delimiter must be specified', 'pmxe_plugin'));
			}

			if ( ! $this->errors->get_error_codes()) {										

				if ($this->isWizard) {					
					foreach ($this->data['post'] as $key => $value) {
						PMXE_Plugin::$session->set($key, $value);	
					}
					PMXE_Plugin::$session->save_data(); 				
					wp_redirect(add_query_arg('action', 'options', $this->baseUrl)); die();	
				}							
				else {
					$this->data['export']->set(array( 'options' => $post, 'settings_update_on' => date('Y-m-d H:i:s')))->save();
					if ( ! empty($post['friendly_name']) ) {
						$this->data['export']->set( array( 'friendly_name' => $post['friendly_name'], 'scheduled' => (($post['is_scheduled']) ? $post['scheduled_period'] : '') ) )->save();	
					}
					wp_redirect(add_query_arg(array('page' => 'pmxe-admin-manage', 'pmxe_nt' => urlencode(__('Options updated', 'pmxi_plugin'))) + array_intersect_key($_GET, array_flip($this->baseUrlParamNames)), admin_url('admin.php'))); die();
				}
									
			}
			
		}

		$this->data['acf_groups'] = array();	

		$this->data['existing_acf_meta_keys'] = array();	

		if ( class_exists( 'acf' ) ) {
		
			global $acf;			

			if ($acf and version_compare($acf->settings['version'], '5.0.0') >= 0){

				$saved_acfs = get_posts(array('posts_per_page' => -1, 'post_type' => 'acf-field-group'));																

				//$this->data['acf_groups'] = $acfs = acf_local()->groups;

			}
			else{

				$this->data['acf_groups'] = apply_filters('acf/get_field_groups', array());	

			}			

			if ( ! empty($saved_acfs)){
				foreach ($saved_acfs as $key => $obj) {
					$this->data['acf_groups'][] = array(
						'ID' => $obj->ID,
						'title' => $obj->post_title
					);
				}
			}								

			if ( ! empty($this->data['acf_groups']) ){

				// get all ACF fields
				if ($acf->settings['version'] and version_compare($acf->settings['version'], '5.0.0') >= 0){		

					foreach ($this->data['acf_groups'] as $key => $acf_obj) {

						if ( is_numeric($acf_obj['ID'])){

							$acf_fields = get_posts(array('posts_per_page' => -1, 'post_type' => 'acf-field', 'post_parent' => $acf_obj['ID'], 'post_status' => 'publish', 'orderby' => 'menu_order', 'order' => 'ASC'));				

							if ( ! empty($acf_fields) ){

								foreach ($acf_fields as $field) {				

									$fieldData = (!empty($field->post_content)) ? unserialize($field->post_content) : array();			
									
									$fieldData['ID']    = $field->ID;
									$fieldData['id']    = $field->ID;
									$fieldData['label'] = $field->post_title;
									$fieldData['key']   = $field->post_name;					

									if (empty($fieldData['name'])) $fieldData['name'] = $field->post_excerpt;

									if ( ! empty($fieldData['name'])) $this->data['existing_acf_meta_keys'][] = $fieldData['name'];
									
									$this->data['acf_groups'][$key]['fields'][] = $fieldData;
									
								}
							}
						}
					}
				}
				else{

					foreach ($this->data['acf_groups'] as $key => $acf_obj) {

						if (is_numeric($acf_obj['ID'])){

							$fields = array();

							foreach (get_post_meta($acf_obj['ID'], '') as $cur_meta_key => $cur_meta_val)
							{	
								if (strpos($cur_meta_key, 'field_') !== 0) continue;

								$fields[] = (!empty($cur_meta_val[0])) ? unserialize($cur_meta_val[0]) : array();			
														
							}

							if (count($fields)){

								$sortArray = array();

								foreach($fields as $field){
								    foreach($field as $key=>$value){
								        if(!isset($sortArray[$key])){
								            $sortArray[$key] = array();
								        }
								        $sortArray[$key][] = $value;
								    }
								}

								$orderby = "order_no"; 

								array_multisort($sortArray[$orderby],SORT_ASC,$fields); 

								foreach ($fields as $field){ 
									$this->data['acf_groups'][$key]['fields'][] = $field;									
									if ( ! empty($field['name'])) $this->data['existing_acf_meta_keys'][] = $field['name'];
								}
								
							}
						}
					}
				}
			}			
		}

		$this->data['woo_data'] = class_exists('WooCommerce') ? array(
			'_visibility', '_stock_status', '_downloadable', '_virtual', '_regular_price', '_sale_price', '_purchase_note', '_featured', '_weight', '_length',
			'_width', '_height', '_sku', '_sale_price_dates_from', '_sale_price_dates_to', '_price', '_sold_individually', '_manage_stock', '_stock', '_upsell_ids', '_crosssell_ids',
			'_downloadable_files', '_download_limit', '_download_expiry', '_download_type', '_product_url', '_button_text', '_backorders', '_tax_status', '_tax_class', '_product_image_gallery', '_default_attributes',
			'total_sales', '_product_attributes'
		) : array();

		$this->data['existing_meta_keys'] = array();
		$hide_fields = array('_edit_lock', '_edit_last');

		if ( $exportQuery->post_count )
		{
			while ( $exportQuery->have_posts() ) :				

				$exportQuery->the_post(); $record = get_post( get_the_ID() );

				$record_meta = get_post_meta($record->ID, '');
				if ( ! empty($record_meta)){
					foreach ($record_meta as $record_meta_key => $record_meta_value) {
						if ( ! in_array($record_meta_key, $this->data['existing_meta_keys']) and ! in_array($record_meta_key, $hide_fields) and !empty($record_meta_key)){
							if ( ! empty($this->data['acf_groups']) ){
								$is_acf_field = false;
								foreach ($this->data['acf_groups'] as $key => $group) {
									if (!empty($group['fields'])){
										foreach ($group['fields'] as $field) {
											if ( in_array($record_meta_key, array($field['name'], "_" . $field['name'])) or !empty($field['name']) and strpos($record_meta_key, $field['name']) === 0 or !empty($field['name']) and strpos($record_meta_key, "_" . $field['name']) === 0){
												$is_acf_field = true;
												break(2);
											}
										}
									}						
								}
								if ($is_acf_field) continue;																	
							}
							
							if (class_exists('WooCommerce')){										

								if ( strpos($record_meta_key, 'attribute_pa_') === 0 || strpos($record_meta_key, '_min_') === 0 || strpos($record_meta_key, '_max_') === 0){
									if ( ! in_array($record_meta_key, $this->data['woo_data']))
										$this->data['woo_data'][] = $record_meta_key;
									
									continue;
								}

							}

							if ( ! in_array($record_meta_key, $this->data['woo_data'])) {										
								$this->data['existing_meta_keys'][] = $record_meta_key;
							}
							
						}
					}
				}

			endwhile;
		}		

		global $wp_taxonomies;		
		$this->data['existing_taxonomies'] = array();
		$this->data['existing_attributes'] = array();
		
		foreach ($wp_taxonomies as $key => $obj) {	if (in_array($obj->name, array('nav_menu'))) continue;

			if (strpos($obj->name, "pa_") === 0){
				$this->data['existing_attributes'][] = $obj->name;									
			}
			else{
				$this->data['existing_taxonomies'][] = $obj->name;									
			}
		}		
				
		$this->render();		
	}

	public function options()
	{

		$default = PMXE_Plugin::get_default_import_options();

		if ($this->isWizard) {	
			$DefaultOptions = (PMXE_Plugin::$session->has_session() ? PMXE_Plugin::$session->get_clear_session_data() : array()) + $default;
			$post = $this->input->post($DefaultOptions);
		}
		else{
			$DefaultOptions = $this->data['export']->options + $default;
			$post = $this->input->post($DefaultOptions);			
			$post['scheduled'] = $this->data['export']->scheduled;
			foreach ($post as $key => $value) {
				PMXE_Plugin::$session->set($key, $value);
			}						
			PMXE_Plugin::$session->save_data(); 
		}

		$this->data['post'] =& $post;								
		
		if ($this->input->post('is_submitted')) {

			check_admin_referer('options', '_wpnonce_options');
			
			if ($post['is_generate_templates'] and '' == $post['template_name']){	
				$friendly_name = '';
				$post_types = PMXE_Plugin::$session->get('cpt');
				if ( ! empty($post_types) )
				{					
					$post_type_details = get_post_type_object( array_shift($post_types) );					
					$friendly_name = $post_type_details->labels->name . ' Export - ' . date("Y F d H:i");
				}
				else
				{
					$friendly_name = 'WP_Query Export - ' . date("Y F d H:i");
				}			
				$post['template_name'] = $friendly_name;
			}			

			if ( ! $this->errors->get_error_codes()) {
				
				if ($this->isWizard) {					
					foreach ($this->data['post'] as $key => $value) {
						PMXE_Plugin::$session->set($key, $value);	
					}
					PMXE_Plugin::$session->save_data(); 				
					wp_redirect(add_query_arg('action', 'process', $this->baseUrl)); die();	
				}							
				else {
					$this->data['export']->set(array( 'options' => $post, 'settings_update_on' => date('Y-m-d H:i:s')))->save();
					if ( ! empty($post['friendly_name']) ) {
						$this->data['export']->set( array( 'friendly_name' => $post['friendly_name'], 'scheduled' => (($post['is_scheduled']) ? $post['scheduled_period'] : '') ) )->save();	
					}
					wp_redirect(add_query_arg(array('page' => 'pmxe-admin-manage', 'pmxe_nt' => urlencode(__('Options updated', 'pmxi_plugin'))) + array_intersect_key($_GET, array_flip($this->baseUrlParamNames)), admin_url('admin.php'))); die();
				}
									
			}
			
		}

		$this->render();
	}

	/**
	 * Step #3: Export
	 */ 
	public function process()
	{										

		@set_time_limit(0);		

		$export = $this->data['update_previous'];		

		if ( ! PMXE_Plugin::is_ajax() ) {

			if ("" == PMXE_Plugin::$session->friendly_name){
				$friendly_name = '';
				$post_types  = PMXE_Plugin::$session->get('cpt');
				if ( ! empty($post_types) )
				{					
					$post_type_details = get_post_type_object( array_shift($post_types) );					
					$friendly_name = $post_type_details->labels->name . ' Export - ' . date("Y F d H:i");
				}
				else
				{
					$friendly_name = 'WP_Query Export - ' . date("Y F d H:i");
				}

				PMXE_Plugin::$session->set('friendly_name', $friendly_name);
			} 

			PMXE_Plugin::$session->set('file', '');
			PMXE_Plugin::$session->save_data(); 		

			$export->set(
				array(
					'triggered' => 0,		
					'processing' => 0,
					'exported'  => 0,
					'executing' => 1,
					'canceled' => 0,
					'options'   => PMXE_Plugin::$session->get_clear_session_data(),
					'friendly_name' => PMXE_Plugin::$session->friendly_name,
					'scheduled' => (PMXE_Plugin::$session->is_scheduled) ? PMXE_Plugin::$session->scheduled_period : '',
					'registered_on' => date('Y-m-d H:i:s'),
					'last_activity' => date('Y-m-d H:i:s')
				)
			)->save();						

			$options = $export->options;

			if ( $options['is_generate_import'] and wp_all_export_is_compatible() ){				
				
				$import = new PMXI_Import_Record();

				if ( ! empty($options['import_id']) ) $import->getById($options['import_id']);

				if ($import->isEmpty()){

					$import->set(array(		
						'parent_import_id' => 99999,
						'xpath' => '/',			
						'type' => 'upload',																
						'options' => array('empty'),
						'root_element' => 'root',
						'path' => 'path',
						//'name' => '',
						'imported' => 0,
						'created' => 0,
						'updated' => 0,
						'skipped' => 0,
						'deleted' => 0,
						'iteration' => 1					
					))->save();					

					PMXE_Plugin::$session->set('import_id', $import->id);

					$options['import_id'] = $import->id;

					$export->set(array(
						'options' => $options
					))->save();
				}
				else{

					if ( $import->parent_import_id != 99999 ){

						$newImport = new PMXI_Import_Record();

						$newImport->set(array(		
							'parent_import_id' => 99999,
							'xpath' => '/',			
							'type' => 'upload',																
							'options' => array('empty'),
							'root_element' => 'root',
							'path' => 'path',
							//'name' => '',
							'imported' => 0,
							'created' => 0,
							'updated' => 0,
							'skipped' => 0,
							'deleted' => 0,
							'iteration' => 1					
						))->save();					

						PMXE_Plugin::$session->set('import_id', $newImport->id);

						$options['import_id'] = $newImport->id;

						$export->set(array(
							'options' => $options
						))->save();

					}

				}

			}

			PMXE_Plugin::$session->set('update_previous', $export->id);

			PMXE_Plugin::$session->save_data();

		}		

		$this->render();

	}	
	
}