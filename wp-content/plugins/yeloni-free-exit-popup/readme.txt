=== Yeloni Exit Popup ===
Contributors: jayasrinagrale, kranthitech
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RRK6C4QZSLBCG
Tags: mailchimp popup, mailchimp subscription form, mailchimp subscription popup, exit popups, popup plugin, popup before user leaves, exit overlay, exit overlay popup, list building, popup, popup with image, subscription form, mailchimp, newsletter, signup form, popup editor, popup on back button, popup on click, popup on link click, popup on scroll, popup on page load, popup on delay, aweber subscription popup, aweber exit popup, mailchimp exit popup
Requires at least: 3.0.1
Tested up to: 4.4
Stable tag: 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Exit Popups are the best way to engage visitors leaving your website. Show offers, social buttons, email signup forms or customize it as you like.




== Description ==

>Note from the Plugin Developer: After speaking to a lot of customers, we have revamped the plugin released a major which addresses new requirements/features. If you want to continue using the older version, please click the link below. It would really help us if you can send an email to admin@yeloni.com and tell us what are the features you are missing / your feedback. [Click Here for older version of the plugin](http://yeloni.com/free-plugin-download/yeloni-exit-popup.zip)

**What the plugin does:**

* Show a high converting popup on your website based on your settings - either when a user is leaving your website, after a specified time duration, after the visitor scrolls, or when the visitor reaches the bottom of the page.


**Free Version Includes:**

**1. Design Your Own Popup**

* Start from multiple themes for Email Subscription, Social Sharing, Offer/Landing Page Redirection, Chat Activation

* Customize the theme and messaging according to your requirements - Text, Images, Colors, Fonts etc

**2. Configure Behavior of the Popup**

* Triggers - Exit Intent, Page Load, Time Delay, Scrolling a certain height of the Page

* Three types of Exit-Intent - Based on mouse movement, Clicking Browser Back button, Clicking Internal or External Link

* Popups with multiple themes for Email Subscription, Social Sharing, Offer/Landing Page Redirection, Chat Activation

* Ability to create multiple Popups

**3. Integrations with Email Marketing Services**

* Mailchimp 
* Aweber
* Other services coming soon

* For chat activation


**Paid Version Includes: [with a 5 day free trial]**

* Responsive Popups on Mobile & Tablet Screens

* Showing Popup on specific Pages instead of all Pages



>Exit Popups are the best way to engage visitors leaving your website. Easily add various types of exit popups with social buttons, offer images, yes no question popups, email subscription forms, mailchimp, aweber & infusionsoft integrations and much more.





**Why you're going to love Yeloni Exit Popups**

* Yeloni is a Lightweight plugin that will not slow down or clog up your website. 

* Yeloni popup only shows when the user is about to leave your website. It will not show when the user is on the scroll bar, or navigating within your website.

* Awesome support - here help you be better at what you do.

* Multiple well tested & high converting themes.

* Popup Imagery & HTML Design - Need a custom image made for your popup? or a custom HTML form designed? We would love to assist you with that.




If you have suggestions for a new add-on, feel free to email us at admin@yeloni.com- Thanks! To know more about Yeloni: [Click Here](http://www.yeloni.com)

Cross Browser Testing on Yeloni is supported by [BrowserStack](http://www.browserstack.com)

== Installation ==

1. Click on "Plugins" -> "Add new".

2. Type Yeloni Exit Popup in the search bar.

3. Click on "Install now". 

4. Click on "Activate Plugin".



You can also install it using the downloaded zip file like so:

1. Unzip the 'Yeloni Exit Popup.zip' to a folder. 

2. Upload `Yeloni Exit Popup` folder to the `/wp-content/plugins/` directory

2a. If you are installing directly from the 'Plugins -> Add New' menu from your wordpress dashboard, just click 'Install' and continue with the steps below.

3. Activate the plugin through the 'Plugins' menu in WordPress

4. Next, go to Settings -> Yeloni Exit Pop and change the default settings to match your own. Save the changes.

5. The exit popup must now be active on your website.



== Frequently Asked Questions ==

= 1. Is this a free exit popup? =

Yes, this popup comes with both free and paid version. In the free version, you can add social share buttons and offer images in the exit popup. For a small fee you can activate the 'custom HTML exit popup', using which you can add your own HTML/CSS/Scripts to the exit popup.


= 2. What are the types of exit popups that I can have on my website? =

* Social Share - Adds a social share buttons such as Facebook Like, Facebook Share, Tweet & Google Plus to the exit popup on your website

* Linked Image - Take your visitors to an offer/discount page or to your latest post. This is a popup with a linked image.

* Email Subscription / Custom HTML Popup - For a small fee you can activate the 'custom HTML exit popup', using which you can add your own HTML/CSS/Scripts to the exit popup. 

You can also integrate your existing email subscrption tools such as Mailchimp, Aweber, Infusionsoft using this feature. All you have to do is just add the code provided by these platforms into the popup and you are set to go!

* Yes No Popup [ Question Popup ] - Ask your visitors a question and engage them or even better - inspire them to sign up for your email list.

* Survey Popup

Coming soon :)

* Age Verification Popup 

Coming soon :)




= 3. Can I suggest new features? =

By all means, please do. We would like to know how you are using this plugin & any features that you think will make life easy for you. Send me a quick mail on admin@yeloni.com



= 4. Does this popup show when a user has clicked on another blog post? =

You have full control over the popup settings. You can fire the popup when a visitor is leaving the page, on page load, when a visitor scrolls, when a visitor reaches the bottom of the page, show the popup only on specific posts/pages, show the popup when a visitor clicks on an external link - you name it, the setting is there.



= 5. Can I use Mailchimp with this popup? =

Yes, you can integrate **Campaign Monitor, Mad Mimi, MailChimp, Send in Blue, Stream Send, Sendy** or any other tools that you use to store and manage your email lists. Just choose your provider add the code that these platforms give you to the popup and you are all set to go.



= Any other question you'd like to ask? =

Please do contact us [here](http://www.yeloni.com) or email us on admin@yeloni.com



== Screenshots ==

1. Few of the high converting themes you can use to create custom popups on your website. 


2. Popup Themes are divided into purposes such as social sharing, yes no question popups, offer popups, email subscription popups and custom popups where you can add your own HTML/CSS code.


3. Some more themes!


4. After you select a theme, you can customize every component to match your requirements and see the changes upated in real time on the right side.


5. Each component comes with various settings that give you full control of how the popup should look.


6. After design, you can control when the popup shows up, how, to whom and add other settings in the 'configure' section.


7. One of the many popups in action!



== Changelog ==
= 3.8.7 =
Direct Integration with Mailchimp
Direct Integration with Aweber

= 3.2.6 =
"Show Popup Once per visitor" setting is now available in the lite version
In the Configuration section of the popup building screen, you should be seeing three options
1. Show Popup once per visitor
2. Show Popup once per session

= 3.2.1 =
Now it works fine with SSL websites

= 3.0 =
Oh boy! We have revamped the whole plugin. 
The plugin now comes with 14 new themes, full control over all popup components & ability to edit them; custom settings and full control over when the popup is shown, on which pages, how it is seen.

= 1.8 =
New feature to add custom HTML/CSS/Scripts to the popup

= 1.7 =
New paid feature - ability to show popup only on some selected pages/posts

= 1.5 =
URL tracking, fixed minor bugs, easier payment options.

= 1.3 =
Fixed minor issues

= 1.0 =
Added three types of exit popups - social share, linked images & email subscription.

= 0.5 = 
This version also has the ability to activate the premium  version and use Exit popups with email subscription.

= 0.1 =
Launched Yeloni Exit Popup Plugin with Facebook Like & Tweet features.
