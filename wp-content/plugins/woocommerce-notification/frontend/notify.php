<?php

/**
 * Class VI_WNOTIFICATION_Frontend_Notify
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class VI_WNOTIFICATION_Frontend_Notify {
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'init_scripts' ) );

		add_action( 'init', array( $this, 'init' ) );
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );

		add_action( 'wp_ajax_nopriv_woonotification_get_product', array( $this, 'product_html' ) );
		add_action( 'wp_ajax_woonotification_get_product', array( $this, 'product_html' ) );

		add_action( 'woocommerce_order_status_completed', array( $this, 'woocommerce_order_status_completed' ) );
		add_action( 'woocommerce_order_status_pending', array( $this, 'woocommerce_order_status_completed' ) );

	}

	public function woocommerce_order_status_completed( $order_id ) {
		$params       = new VI_WNOTIFICATION_Admin_Settings();
		$archive_page = $params->get_field( 'archive_page' );
		if ( ! $archive_page ) {
			update_option( '_woocommerce_notification_prefix', substr( md5( date( "YmdHis" ) ), 0, 10 ) );
		}
	}


	/**
	 * Show HTML on front end
	 */
	public function product_html() {
		$params = new VI_WNOTIFICATION_Admin_Settings();
		$enable = $params->get_field( 'enable' );
		if ( $enable ) {
			echo $this->show_product( true );
		}

		die;
	}

	/**
	 * Detect IP
	 */
	public function init() {
		$params         = new VI_WNOTIFICATION_Admin_Settings();
		$detect_country = $params->get_field( 'country' );
		$detect         = isset( $_COOKIE['ip'] ) ? 1 : 0;
		if ( ! $detect_country && ! $detect ) {
			$data            = $this->geoCheckIP( $this->getIP() );
			$data['country'] = isset( $data['country'] ) ? $data['country'] : 'United States';
			$data['city']    = isset( $data['city'] ) ? $data['city'] : 'New York City';

			setcookie( 'ip', 1, time() + 7 * 60 * 60 * 24, '/' );
			setcookie( 'country', $data['country'], time() + 7 * 60 * 60 * 24, '/' );
			setcookie( 'city', $data['city'], time() + 7 * 60 * 60 * 24, '/' );
		}
		/*Make cache folder*/
		if ( ! is_dir( VI_WNOTIFICATION_CACHE ) ) {
			mkdir( VI_WNOTIFICATION_CACHE, '0755', true );
			chmod( VI_WNOTIFICATION_CACHE, 0755 );
			file_put_contents( VI_WNOTIFICATION_CACHE . '.htaccess', '<IfModule !mod_authz_core.c>
Order deny,allow
Deny from all
</IfModule>
<IfModule mod_authz_core.c>
  <RequireAll>
    Require all denied
  </RequireAll>
</IfModule>
' );
		}

	}


	/**
	 * Show HTML code
	 */
	public function wp_footer() {
		$params        = new VI_WNOTIFICATION_Admin_Settings();
		$enable        = $params->get_field( 'enable' );
		$logic_value   = $params->get_field( 'conditional_tags' );
		$sound_enable  = $params->get_field( 'sound_enable' );
		$sound         = $params->get_field( 'sound' );
		$enable_mobile = $params->get_field( 'enable_mobile' );
		$is_home       = $params->get_field( 'is_home' );
		$is_checkout   = $params->get_field( 'is_checkout' );
		$is_cart       = $params->get_field( 'is_cart' );
		// Include and instantiate the class.
		$detect = new VillaTheme_Mobile_Detect;

		// Any mobile device (phones or tablets).
		if ( $detect->isMobile() ) {
			if ( ! $enable_mobile ) {
				return false;
			}
		}
		/*Assign page*/
		if ( $is_home && is_home() ) {
			return;
		}
		if ( $is_checkout && is_checkout() ) {
			return;
		}
		if ( $is_cart && is_cart() ) {
			return;
		}
		if ( $logic_value ) {
			if ( stristr( $logic_value, "return" ) === false ) {
				$logic_value = "return (" . $logic_value . ");";
			}
			if ( ! eval( $logic_value ) ) {
				return;
			}
		}
		if ( $enable ) {
			echo $this->show_product();
		}

		if ( $sound_enable ) { ?>
			<audio id="woocommerce-notification-audio">
				<source src="<?php echo esc_url( VI_WNOTIFICATION_SOUNDS_URL . $sound ) ?>">
			</audio>
		<?php }
	}

	/**
	 * Add Script and Style
	 */
	function init_scripts() {
		$params      = new VI_WNOTIFICATION_Admin_Settings();
		$is_home     = $params->get_field( 'is_home' );
		$is_checkout = $params->get_field( 'is_checkout' );
		$is_cart     = $params->get_field( 'is_cart' );
		/*Conditional tags*/
		$logic_value = $params->get_field( 'conditional_tags' );
		/*Assign page*/
		if ( $is_home && is_home() ) {
			return;
		}
		if ( $is_checkout && is_checkout() ) {
			return;
		}
		if ( $is_cart && is_cart() ) {
			return;
		}
		if ( $logic_value ) {
			if ( stristr( $logic_value, "return" ) === false ) {
				$logic_value = "return (" . $logic_value . ");";
			}
			if ( ! eval( $logic_value ) ) {
				return;
			}
		}

		wp_enqueue_style( 'woocommerce-notification', VI_WNOTIFICATION_CSS . 'woocommerce-notification.css', array(), VI_WNOTIFICATION_VERSION );

		wp_enqueue_script( 'woocommerce-notification', VI_WNOTIFICATION_JS . 'woocommerce-notification.js', array( 'jquery' ), VI_WNOTIFICATION_VERSION );

		/*Custom*/

		$highlight_color    = $params->get_field( 'highlight_color' );
		$text_color         = $params->get_field( 'text_color' );
		$background_color   = $params->get_field( 'background_color' );
		$custom_css_setting = $params->get_field( 'custom_css' );
		$background_image   = $params->get_field( 'background_image' );

		$border_radius = $params->get_field( 'border_radius' ) . 'px';
		$custom_css    = "
                #message-purchased{
                        background-color: {$background_color};                       
                        color:{$text_color} !important;
                        border-radius:{$border_radius} ;
                }
                #message-purchased img{
                       border-radius:{$border_radius} 0 0 {$border_radius};
                }
                 #message-purchased a{
                        color:{$highlight_color} !important;
                }" . $custom_css_setting;
		if ( $background_image ) {
			$background_image = woocommerce_notification_background_images( $background_image );
			$custom_css       .= "#message-purchased.extended::before{
				background-image: url('{$background_image}');  
				 border-radius:{$border_radius};
			}";
		}
		wp_add_inline_style( 'woocommerce-notification', $custom_css );


	}

	/**
	 * Show product
	 *
	 * @param $product_id Product ID
	 *
	 */
	protected function show_product( $fisrt = false ) {
		$params                 = new VI_WNOTIFICATION_Admin_Settings();
		$image_position         = $params->get_field( 'image_position' );
		$position               = $params->get_field( 'position' );
		$loop                   = $params->get_field( 'loop' );
		$initial_delay          = $params->get_field( 'initial_delay' );
		$message_display_effect = $params->get_field( 'message_display_effect' );
		$message_hidden_effect  = $params->get_field( 'message_hidden_effect' );
		$initial_delay_random   = $params->get_field( 'initial_delay_random' );
		$non_ajax               = $params->get_field( 'non_ajax' );
		if ( $initial_delay_random ) {
			$initial_delay_min = $params->get_field( 'initial_delay_min' );
			$initial_delay     = rand( $initial_delay_min, $initial_delay );
		}
		$notification_per_page = $params->get_field( 'notification_per_page' );
		$display_time          = $params->get_field( 'display_time' );
		$next_time             = $params->get_field( 'next_time' );
		$class                 = array();
		$class[]               = $image_position ? 'img-right' : '';
		$enable_single_product = $params->get_field( 'enable_single_product' );
		$product_id            = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT );
		$background_image      = $params->get_field( 'background_image' );
		if ( $enable_single_product && is_product() ) {
			global $product;
			$product_id_str = 'data-product="' . esc_attr( $product->get_id() ) . '">';
		} elseif ( $product_id ) {
			$product_id_str = 'data-product="' . esc_attr( $product_id ) . '">';
		} else {
			$product_id_str = '';
		}

		switch ( $position ) {
			case  1:
				$class[] = 'bottom_right';
				break;
			case  2:
				$class[] = 'top_left';
				break;
			case  3:
				$class[] = 'top_right';
				break;
		}
		if ( $background_image ) {
			$class[] = 'extended';
			$class[] = 'wn-' . $background_image;
		}
		ob_start();
		if ( $non_ajax ) {
			$message_purchased = $params->get_field( 'message_purchased' );
			$image_redirect    = $params->get_field( 'image_redirect' );

			if ( ! is_array( $message_purchased ) ) {
				$message_purchased = array( $message_purchased );
			}
			$products = $this->get_data_args();
			if ( count( $products ) ) {
				?>
				<div id="message-purchased" class="customized <?php echo implode( ' ', $class ) ?>" style="display: none;"
					 data-loop="<?php echo esc_attr( $loop ) ?>"
					 data-initial_delay="<?php echo esc_attr( $initial_delay ) ?>"
					 data-notification_per_page="<?php echo esc_attr( $notification_per_page ) ?>"
					 data-display_time="<?php echo esc_attr( $display_time ) ?>"
					 data-next_time="<?php echo esc_attr( $next_time ) ?>"
					 data-display_effect="<?php echo empty( $message_display_effect ) ? esc_attr( 'fade-in' ) : esc_attr( $message_display_effect ); ?>"
					 data-hidden_effect="<?php echo empty( $message_hidden_effect ) ? esc_attr( 'fade-out' ) : esc_attr( $message_hidden_effect ); ?>"
					 data-products="<?php echo base64_encode( json_encode( $this->get_data_args() ) ) ?>"
					 data-messages="<?php echo base64_encode( json_encode( $message_purchased ) ) ?>"
					 data-image="<?php echo esc_attr( $image_redirect ) ?>">
					<div class="wn-nonajax"></div>
					<?php if ( $params->get_field( 'show_close_icon' ) ) {
						?>
						<span id="notify-close"></span>
						<?php
					} ?>
				</div>
			<?php }
		} else {
			?>
			<div id="message-purchased" class="customized <?php echo implode( ' ', $class ) ?>" style="display: none;"
				 data-url="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ) ?>"
				 data-loop="<?php echo esc_attr( $loop ) ?>"
				 data-initial_delay="<?php echo esc_attr( $initial_delay ) ?>"
				 data-notification_per_page="<?php echo esc_attr( $notification_per_page ) ?>"
				 data-display_time="<?php echo esc_attr( $display_time ) ?>"
				 data-next_time="<?php echo esc_attr( $next_time ) ?>"
				 data-display_effect="<?php echo empty( $message_display_effect ) ? esc_attr( 'fade-in' ) : esc_attr( $message_display_effect ); ?>"
				 data-hidden_effect="<?php echo empty( $message_hidden_effect ) ? esc_attr( 'fade-out' ) : esc_attr( $message_hidden_effect ); ?>"
				<?php echo $product_id_str ?>>

				<?php if ( $fisrt ) {
					echo $this->message_purchased();
				} ?>

			</div>
			<?php
		}

		return ob_get_clean();
	}

	/**
	 * Get data Ajax no cache
	 * @return bool
	 */
	protected function get_data_args() {
		$prefix   = woocommerce_notification_prefix();
		$params   = new VI_WNOTIFICATION_Admin_Settings();
		$products = get_transient( 'wn_data_nonajax' . $prefix );
		if ( ! is_array( $products ) || ! count( array_filter( $products ) ) ) {
			$products = $this->get_product( false );
			if ( count( $products ) ) {
				$new_data     = array();
				$product_link = $params->get_field( 'product_link' );
				foreach ( $products as $product ) {
					$p             = wc_get_product( $product['id'] );
					$data          = array();
					$data['title'] = $p->get_title();
					if ( $p->is_type( 'external' ) && $product_link ) {
						// do stuff for simple products
						$link = get_post_meta( $p->get_id(), '_product_url', '#' );
						if ( ! $link ) {
							$link = get_permalink( $p->get_id() );
							$link = wp_nonce_url( $link, 'wocommerce_notification_click', 'link' );
						}
					} else {
						// do stuff for everything else
						$link = get_permalink( $p->get_id() );
						$link = wp_nonce_url( $link, 'wocommerce_notification_click', 'link' );
					}
					$product_thumb = $params->get_field( 'product_sizes', 'shop_thumbnail' );

					if ( has_post_thumbnail( $product['id'] ) ) {
						$image_link = get_the_post_thumbnail_url( $product['id'], $product_thumb );
					} elseif ( $p->get_type() == 'variation' ) {
						$parent_id  = $p->get_parent_id();
						$image_link = get_the_post_thumbnail_url( $parent_id, $product_thumb );
					} else {
						$image_link = '';
					}

					$data['product_link'] = $link;
					$data['first_name']   = $product['first_name'];
					$data['time']         = $product['time'];
					$data['city']         = $product['city'];
					$data['state']        = $product['state'];
					$data['country']      = $product['country'];
					$data['image_link']   = $image_link;
					$data['custom']       = $this->get_custom_shortcode();
					$new_data[]           = $data;
				}
				shuffle( $new_data );
				if ( count( $new_data ) ) {
					set_transient( 'wn_data_nonajax' . $prefix, $new_data, 3600 );
				}
				if ( count( $new_data ) > 5 ) {
					return array_slice( $new_data, 0, 5 );
				} else {
					return $new_data;
				}
			} else {
				return false;
			}
		} else {
			shuffle( $products );
			if ( count( $products ) > 5 ) {
				return array_slice( $products, 0, 5 );
			} else {
				return $products;
			}
		}

	}

	/**
	 * Message purchased
	 *
	 * @param $product_id
	 */
	protected function message_purchased() {
		$params            = new VI_WNOTIFICATION_Admin_Settings();
		$message_purchased = $params->get_field( 'message_purchased' );
		$show_close_icon   = $params->get_field( 'show_close_icon' );
		$archive_page      = $params->get_field( 'archive_page' );
		$product_link      = $params->get_field( 'product_link' );
		$image_redirect    = $params->get_field( 'image_redirect' );
		if ( is_array( $message_purchased ) ) {
			$index             = rand( 0, count( $message_purchased ) - 1 );
			$message_purchased = $message_purchased[ $index ];
		}
		$messsage = '';
		$keys     = array(
			'{first_name}',
			'{city}',
			'{state}',
			'{country}',
			'{product}',
			'{product_with_link}',
			'{time_ago}',
			'{custom}'
		);


		$product = $this->get_product();

		if ( $product ) {
			$product_id = $product['id'];
		} else {
			return false;
		}

		$first_name = trim( $product['first_name'] );
		$city       = trim( $product['city'] );
		$state      = trim( $product['state'] );
		$country    = trim( $product['country'] );
		$time       = trim( $product['time'] );
		if ( ! $archive_page ) {
			$time = $this->time_substract( $time );
		}

		$_product = wc_get_product( $product_id );
		if ( woocommerce_version_check() ) {
			$product = esc_html( strip_tags( get_the_title( $product_id ) ) );
		} else {
			$prd_var_title = $_product->post->post_title;
			if ( $_product->get_type() == 'variation' ) {

				$prd_var_attr = $_product->get_variation_attributes();
				$attr_name1   = array_values( $prd_var_attr )[0];
				$product      = $prd_var_title . ' - ' . $attr_name1;
			} else {
				$product = $prd_var_title;
			}

			$product = strip_tags( $product );
		}

		if ( $_product->is_type( 'external' ) && $product_link ) {
			// do stuff for simple products
			$link = get_post_meta( $product_id, '_product_url', '#' );
			if ( ! $link ) {
				$link = get_permalink( $product_id );
				$link = wp_nonce_url( $link, 'wocommerce_notification_click', 'link' );
			}
		} else {
			// do stuff for everything else
			$link = get_permalink( $product_id );
			$link = wp_nonce_url( $link, 'wocommerce_notification_click', 'link' );
		}
		ob_start(); ?>
		<a target="_blank" href="<?php echo esc_url( $link ) ?>"><?php echo esc_html( $product ) ?></a>
		<?php $product_with_link = ob_get_clean();
		ob_start(); ?>
		<small><?php echo esc_html__( 'About', 'woocommerce-notification' ) . ' ' . esc_html( $time ) . ' ' . esc_html__( 'ago', 'woocommerce-notification' ) ?></small>
		<?php $time_ago = ob_get_clean();
		$product_thumb  = $params->get_field( 'product_sizes', 'shop_thumbnail' );

		if ( has_post_thumbnail( $product_id ) ) {
			if ( $image_redirect ) {
				$messsage .= '<a target="_blank" href="' . esc_url( $link ) . '">';
			}
			$messsage .= '<img src="' . esc_url( get_the_post_thumbnail_url( $product_id, $product_thumb ) ) . '" class="wcn-product-image"/>';
			if ( $image_redirect ) {
				$messsage .= '</a>';
			}
		} elseif ( $_product->get_type() == 'variation' ) {

			$parent_id = $_product->get_parent_id();
			if ( $parent_id ) {
				$messsage .= '<a target="_blank" href="' . esc_url( $link ) . '">';
			}
			$messsage .= '<img src="' . esc_url( get_the_post_thumbnail_url( $parent_id, $product_thumb ) ) . '" class="wcn-product-image"/>';
			if ( $image_redirect ) {
				$messsage .= '</a>';
			}
		}


		//Get custom shortcode
		$custom_shortcode = $this->get_custom_shortcode();
		$replaced         = array(
			$first_name,
			$city,
			$state,
			$country,
			$product,
			$product_with_link,
			$time_ago,
			$custom_shortcode
		);
		$messsage         .= str_replace( $keys, $replaced, '<p>' . strip_tags( $message_purchased ) . '</p>' );
		ob_start();
		if ( $show_close_icon ) {
			?>
			<span id="notify-close"></span>
			<?php
		}
		$messsage .= ob_get_clean();

		return $messsage;
	}

	/**
	 *
	 * @return mixed
	 */
	protected function get_custom_shortcode() {
		$params            = new VI_WNOTIFICATION_Admin_Settings();
		$message_shortcode = $params->get_field( 'custom_shortcode' );
		$min_number        = $params->get_field( 'min_number', 0 );
		$max_number        = $params->get_field( 'max_number', 0 );

		$number  = rand( $min_number, $max_number );
		$message = preg_replace( '/\{number\}/i', $number, $message_shortcode );

		return $message;
	}

	/**
	 * Process product
	 * @return bool
	 */
	protected function get_product( $ajax = true ) {

		$params = new VI_WNOTIFICATION_Admin_Settings();

		/*Check Single Product page*/
		$enable_single_product          = $params->get_field( 'enable_single_product' );
		$notification_product_show_type = $params->get_field( 'notification_product_show_type' );
		$product_id                     = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT );
		if ( $enable_single_product && $product_id ) {

			$product                        = wc_get_product( $product_id );
			$notification_product_show_type = $params->get_field( 'notification_product_show_type' );
			if ( ! $notification_product_show_type ) {
				/* Only show current product*/
				$enable_variable = $params->get_field( 'show_variation' );
				if ( $product->get_type() == 'variable' && $enable_variable ) {
					$temp_p = array_filter( get_transient( 'wn_product_child' . $product->get_id() ) );
					if ( count( $temp_p ) ) {
						$index = rand( 0, count( $temp_p ) - 1 );
						$p_id  = $temp_p[ $index ];
					} else {
						$temp_p = $product->get_children();
						if ( count( $temp_p ) ) {
							$index = rand( 0, count( $temp_p ) - 1 );
							$p_id  = $temp_p[ $index ];
							set_transient( 'wn_product_child' . $product->get_id(), $temp_p, 3600 );
						} else {
							$p_id = $product->get_id();
						}
					}
				} else {
					$p_id = $product->get_id();
				}
			} else {
				/* Show products in the same category*/
				$cates     = $product->get_category_ids();
				$args      = array(
					'post_type'      => 'product',
					'post_status'    => 'publish',
					'posts_per_page' => 50,
					'post__not_in'   => array( $product->get_id() ),
					'tax_query'      => array(
						array(
							'taxonomy'         => 'product_cat',
							'field'            => 'id',
							'terms'            => $cates,
							'include_children' => false,
							'operator'         => 'IN'
						)
					),
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) {
					$related_products = array();
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$related_products[] = get_the_ID();
					}
				}
				// Reset Post Data
				wp_reset_postdata();
				$index = rand( 0, count( $related_products ) - 1 );
				$p_id  = $related_products[ $index ];

			}
			$data = array(
				'id'         => $p_id,
				'time'       => '',
				'first_name' => '',
				'city'       => '',
				'state'      => '',
				'country'    => ''
			);

			$virtual_name   = $params->get_field( 'virtual_name' );
			$virtual_time   = $params->get_field( 'virtual_time' );
			$detect_country = $params->get_field( 'country' );

			if ( $virtual_name ) {
				$virtual_name = explode( "\n", $virtual_name );
				$virtual_name = array_filter( $virtual_name );
			}

			if ( ! $detect_country ) {
				$detect_data = $this->detect_country();

				$country = isset( $detect_data['country'] ) ? $detect_data['country'] : '';
				$city    = isset( $detect_data['city'] ) ? $detect_data['city'] : '';
			} else {
				$country = $params->get_field( 'virtual_country' );
				$city    = $params->get_field( 'virtual_city' );
				if ( $city ) {
					$city = explode( "\n", $city );
					$city = array_filter( $city );
				}
			}


			if ( is_array( $city ) ) {
				$index     = rand( 0, count( $city ) - 1 );
				$city_text = $city[ $index ];
			} else {
				$city_text = $city;
			}

			if ( is_array( $virtual_name ) ) {
				$index             = rand( 0, count( $virtual_name ) - 1 );
				$virtual_name_text = $virtual_name[ $index ];
			} else {
				$virtual_name_text = $virtual_name;
			}


			$data['time']       = $this->time_substract( current_time( 'timestamp' ) - rand( 10, $virtual_time * 3600 ), true );
			$data['first_name'] = $virtual_name_text;
			$data['city']       = $city_text;
			$data['country']    = $country;

			return $data;
		}

		/*Get All page*/
		$archive_page = $params->get_field( 'archive_page' );
		$prefix       = woocommerce_notification_prefix();
		/*Process section*/
		$cache = get_transient( $prefix );
		if ( ! is_array( get_transient( $prefix ) ) ) {
			$cache = array();
		}
		$data_cache = array_filter( $cache );
		$sec_datas  = count( $data_cache ) ? $data_cache : array();
		if ( count( $sec_datas ) ) {
			/*Process data with product up sell*/
			$index = rand( 0, count( $sec_datas ) - 1 );
			$data  = $sec_datas[ $index ];
			if ( ! $params->get_field( 'cache_enable' ) && $archive_page ) {
				$virtual_time = $params->get_field( 'virtual_time' );
				$data['time'] = $this->time_substract( current_time( 'timestamp' ) - rand( 10, $virtual_time * 3600 ), true );
				/*Change virtual name*/
				$virtual_name = $params->get_field( 'virtual_name' );
				if ( $virtual_name ) {
					$virtual_name       = explode( "\n", $virtual_name );
					$virtual_name       = array_filter( $virtual_name );
					$index              = rand( 0, count( $virtual_name ) - 1 );
					$virtual_name_text  = $virtual_name[ $index ];
					$data['first_name'] = $virtual_name_text;
				}
				/*Change city*/
				if ( $params->get_field( 'country' ) ) {
					/*Change city*/
					$city = $params->get_field( 'virtual_city' );
					if ( $city ) {
						$city         = explode( "\n", $city );
						$city         = array_filter( $city );
						$index        = rand( 0, count( $city ) - 1 );
						$city_text    = $city[ $index ];
						$data['city'] = $city_text;
					}
				}
			}

			return $data;

		}

		/*Check with Product get from Billing*/
		if ( ! $archive_page ) {
			/*Get from billing*/
			/*Parram*/
			$order_threshold_num  = $params->get_field( 'order_threshold_num' );
			$order_threshold_time = $params->get_field( 'order_threshold_time' );
			$exclude_products     = $params->get_field( 'exclude_products' );
			$order_statuses       = $params->get_field( 'order_statuses', array( 'wc-completed' ) );
			if ( ! is_array( $exclude_products ) ) {
				$exclude_products = array();
			}
			$current_time = '';
			if ( $order_threshold_num ) {
				switch ( $order_threshold_time ) {
					case 1:
						$time_type = 'days';
						break;
					case 2:
						$time_type = 'minutes';
						break;
					default:
						$time_type = 'hours';
				}
				$current_time = strtotime( "-" . $order_threshold_num . " " . $time_type );
			}
			$args = array(
				'post_type'      => 'shop_order',
				'post_status'    => $order_statuses,
				'posts_per_page' => '100',
				'orderby'        => 'date',
				'order'          => 'DESC'
			);
			if ( $current_time ) {
				$args['date_query'] = array(
					array(
						'after'     => array(
							'year'   => date( "Y", $current_time ),
							'month'  => date( "m", $current_time ),
							'day'    => date( "d", $current_time ),
							'hour'   => date( "H", $current_time ),
							'minute' => date( "i", $current_time ),
							'second' => date( "s", $current_time ),
						),
						'inclusive' => true,
						//(boolean) - For after/before, whether exact value should be matched or not'.
						'compare'   => '<=',
						//(string) - Possible values are '=', '!=', '>', '>=', '<', '<=', 'LIKE', 'NOT LIKE', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN', 'EXISTS' (only in WP >= 3.5), and 'NOT EXISTS' (also only in WP >= 3.5). Default value is '='
						'column'    => 'post_date',
						//(string) - Column to query against. Default: 'post_date'.
						'relation'  => 'AND',
						//(string) - OR or AND, how the sub-arrays should be compared. Default: AND.
					),
				);
			}
			$my_query = new WP_Query( $args );

			$products = array();
			if ( $my_query->have_posts() ) {
				while ( $my_query->have_posts() ) {
					$my_query->the_post();
					$order = new WC_Order( get_the_ID() );
					$items = $order->get_items();

					foreach ( $items as $item ) {
						if ( in_array( $item['product_id'], $exclude_products ) ) {
							continue;
						}
						if ( isset( $item['product_id'] ) ) {
							$p_data = wc_get_product( $item['product_id'] );
							if ( ! $p_data->is_in_stock() ) {
								continue;
							}
							$product['id']         = isset( $item['product_id'] ) ? $item['product_id'] : '';
							$product['time']       = get_the_date( "Y-m-d H:i:s" );
							$product['first_name'] = ucfirst( get_post_meta( get_the_ID(), '_billing_first_name', true ) );
							$product['city']       = ucfirst( get_post_meta( get_the_ID(), '_billing_city', true ) );
							$product['state']      = ucfirst( get_post_meta( get_the_ID(), '_billing_state', true ) );
							$product['country']    = ucfirst( WC()->countries->countries[ get_post_meta( get_the_ID(), '_billing_country', true ) ] );
							$products[]            = $product;
						}
					}
					$products = array_map( "unserialize", array_unique( array_map( "serialize", $products ) ) );

					if ( count( $products ) >= 100 ) {
						break;
					}
				}
				// Reset Post Data
				wp_reset_postdata();
			}

			if ( count( $products ) ) {
				if ( $ajax ) {
					$index = rand( 0, count( $products ) - 1 );
					$data  = $products[ $index ];
					set_transient( $prefix, $products, 3600 );
				} else {
					return $products;
				}
			} else {
				return false;
			}

		} else if ( $archive_page == 1 ) {
			/*Select Products*/
			/*Params from Settings*/
			$archive_products = $params->get_field( 'archive_products' );
			$virtual_name     = $params->get_field( 'virtual_name' );
			$virtual_time     = $params->get_field( 'virtual_time' );
			$detect_country   = $params->get_field( 'country' );

			if ( $virtual_name ) {
				$virtual_name = explode( "\n", $virtual_name );
				$virtual_name = array_filter( $virtual_name );
			}

			if ( ! $detect_country ) {
				$detect_data = $this->detect_country();

				$country = isset( $detect_data['country'] ) ? $detect_data['country'] : '';
				$city    = isset( $detect_data['city'] ) ? $detect_data['city'] : '';
			} else {
				$country = $params->get_field( 'virtual_country' );
				$city    = $params->get_field( 'virtual_city' );
				if ( $city ) {
					$city = explode( "\n", $city );
					$city = array_filter( $city );
				}
			}
			$archive_products = is_array( $archive_products ) ? $archive_products : array();

			if ( count( array_filter( $archive_products ) ) < 1 ) {
				$args      = array(
					'post_type'      => 'product',
					'post_status'    => 'publish',
					'posts_per_page' => '50',
					'orderby'        => 'rand',
					'meta_query'     => array(
						array(
							'key'     => '_stock_status',
							'value'   => 'outofstock',
							'compare' => '!='
						),
					)

				);
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) {
					$archive_products = array();
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$archive_products[] = get_the_ID();
					}
					// Reset Post Data
					wp_reset_postdata();
				}

			}
			$products = array();
			foreach ( $archive_products as $archive_product ) {
				$post_type = get_post_type( $archive_product );
				if ( $post_type == 'product_variation' || $post_type == 'product' ) {
				} else {
					continue;
				}

				if ( is_array( $city ) ) {
					$index     = rand( 0, count( $city ) - 1 );
					$city_text = $city[ $index ];
				} else {
					$city_text = $city;
				}

				if ( is_array( $virtual_name ) ) {
					$index             = rand( 0, count( $virtual_name ) - 1 );
					$virtual_name_text = $virtual_name[ $index ];
				} else {
					$virtual_name_text = $virtual_name;
				}


				$product['id']         = $archive_product;
				$product['time']       = $this->time_substract( current_time( 'timestamp' ) - rand( 10, $virtual_time * 3600 ), true );
				$product['first_name'] = $virtual_name_text;
				$product['city']       = $city_text;
				$product['state']      = '';
				$product['country']    = $country;
				$products[]            = $product;
			}

			if ( count( $products ) ) {
				if ( $ajax ) {
					$index = rand( 0, count( $products ) - 1 );
					$data  = $products[ $index ];
					set_transient( $prefix, $products, 3600 );
				} else {
					return $products;
				}
			} else {
				return false;
			}

		} elseif ( $archive_page == 2 ) {
			/*Latest Products*/
			/*Params from Settings*/
			$limit_product  = $params->get_field( 'limit_product', 50 );
			$virtual_name   = $params->get_field( 'virtual_name' );
			$virtual_time   = $params->get_field( 'virtual_time' );
			$detect_country = $params->get_field( 'country' );

			if ( $virtual_name ) {
				$virtual_name = explode( "\n", $virtual_name );
				$virtual_name = array_filter( $virtual_name );
			}

			if ( ! $detect_country ) {
				$detect_data = $this->detect_country();

				$country = isset( $detect_data['country'] ) ? $detect_data['country'] : '';
				$city    = isset( $detect_data['city'] ) ? $detect_data['city'] : '';
			} else {
				$country = $params->get_field( 'virtual_country' );
				$city    = $params->get_field( 'virtual_city' );
				if ( $city ) {
					$city = explode( "\n", $city );
					$city = array_filter( $city );
				}
			}

			$args      = array(
				'post_type'      => 'product',
				'post_status'    => 'publish',
				'posts_per_page' => $limit_product,
				'orderby'        => 'date',
				'order'          => 'DESC'
			);
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				$archive_products = array();
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$archive_products[] = get_the_ID();
				}
			}
			// Reset Post Data
			wp_reset_postdata();


			$products = array();
			foreach ( $archive_products as $archive_product ) {
				$post_type = get_post_type( $archive_product );
				if ( $post_type == 'product_variation' || $post_type == 'product' ) {
				} else {
					continue;
				}

				if ( is_array( $city ) ) {
					$index     = rand( 0, count( $city ) - 1 );
					$city_text = $city[ $index ];
				} else {
					$city_text = $city;
				}

				if ( is_array( $virtual_name ) ) {
					$index             = rand( 0, count( $virtual_name ) - 1 );
					$virtual_name_text = $virtual_name[ $index ];
				} else {
					$virtual_name_text = $virtual_name;
				}


				$product['id']         = $archive_product;
				$product['time']       = $this->time_substract( current_time( 'timestamp' ) - rand( 10, $virtual_time * 3600 ), true );
				$product['first_name'] = $virtual_name_text;
				$product['city']       = $city_text;
				$product['state']      = '';
				$product['country']    = $country;
				$products[]            = $product;
			}

			if ( count( $products ) ) {
				if ( $ajax ) {
					$index = rand( 0, count( $products ) - 1 );
					$data  = $products[ $index ];
					set_transient( $prefix, $products, 3600 );
				} else {
					return $products;
				}
			} else {
				return false;
			}
		} else {
			/*Select Categories*/
			$cates = $params->get_field( 'select_categories' );
			if ( count( $cates ) ) {
				$categories = get_terms( array(
					'taxonomy' => 'product_cat',
					'include'  => $cates
				) );

				$categories_checked = array();
				if ( count( $categories ) ) {
					foreach ( $categories as $category ) {
						$categories_checked[] = $category->term_id;
					}
				} else {
					return false;
				}

				/*Params from Settings*/
				$limit_product         = $params->get_field( 'limit_product', 50 );
				$cate_exclude_products = $params->get_field( 'cate_exclude_products', array() );
				$virtual_name          = $params->get_field( 'virtual_name' );
				$virtual_time          = $params->get_field( 'virtual_time' );
				$detect_country        = $params->get_field( 'country' );
				if ( ! is_array( $cate_exclude_products ) ) {
					$cate_exclude_products = array();
				}


				if ( $virtual_name ) {
					$virtual_name = explode( "\n", $virtual_name );
					$virtual_name = array_filter( $virtual_name );
				}

				if ( ! $detect_country ) {
					$detect_data = $this->detect_country();

					$country = isset( $detect_data['country'] ) ? $detect_data['country'] : '';
					$city    = isset( $detect_data['city'] ) ? $detect_data['city'] : '';
				} else {
					$country = $params->get_field( 'virtual_country' );
					$city    = $params->get_field( 'virtual_city' );
					if ( $city ) {
						$city = explode( "\n", $city );
						$city = array_filter( $city );
					}
				}

				$args      = array(
					'post_type'      => 'product',
					'post_status'    => 'publish',
					'posts_per_page' => $limit_product,
					'orderby'        => 'rand',
					'post__not_in'   => $cate_exclude_products,
					'tax_query'      => array(
						array(
							'taxonomy'         => 'product_cat',
							'field'            => 'id',
							'terms'            => $categories_checked,
							'include_children' => false,
							'operator'         => 'IN'
						)
					),
					'meta_query'     => array(
						array(
							'key'     => '_stock_status',
							'value'   => 'outofstock',
							'compare' => '!='
						),
					)
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) {
					$archive_products = array();
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$archive_products[] = get_the_ID();
					}
				}
				// Reset Post Data
				wp_reset_postdata();


				$products = array();
				foreach ( $archive_products as $archive_product ) {

					if ( is_array( $city ) ) {
						$index     = rand( 0, count( $city ) - 1 );
						$city_text = $city[ $index ];
					} else {
						$city_text = $city;
					}

					if ( is_array( $virtual_name ) ) {
						$index             = rand( 0, count( $virtual_name ) - 1 );
						$virtual_name_text = $virtual_name[ $index ];
					} else {
						$virtual_name_text = $virtual_name;
					}


					$product['id']         = $archive_product;
					$product['time']       = $this->time_substract( current_time( 'timestamp' ) - rand( 10, $virtual_time * 3600 ), true );
					$product['first_name'] = $virtual_name_text;
					$product['city']       = $city_text;
					$product['state']      = '';
					$product['country']    = $country;
					$products[]            = $product;
				}

				if ( count( $products ) ) {
					if ( $ajax ) {
						$index = rand( 0, count( $products ) - 1 );
						$data  = $products[ $index ];
						set_transient( $prefix, $products, 3600 );
					} else {
						return $products;
					}
				} else {
					return false;
				}
			}
		}

		return $data;

	}

	/**
	 * Detect country and city
	 *
	 * @return array
	 */
	protected function detect_country() {
		$ip = isset( $_COOKIE['ip'] ) ? $_COOKIE['ip'] : '';
		if ( $ip || isset( $_COOKIE['ip'] ) ) {
			$data['city'] = isset( $_COOKIE['city'] ) ? $_COOKIE['city'] : '';
			if ( ! $data['city'] && isset( $_COOKIE['city'] ) ) {
				$data['city'] = $_COOKIE['city'];
			}
			$data['country'] = isset( $_COOKIE['country'] ) ? $_COOKIE['country'] : '';
			if ( ! $data['country'] && isset( $_COOKIE['country'] ) ) {
				$data['country'] = $_COOKIE['country'];
			}
		} else {
			$data = $this->geoCheckIP( $this->getIP() );
		}

		return $data;
	}

	/**
	 * Get ip of client
	 *
	 * @return mixed ip of client
	 */
	protected function getIP() {
		$ip = new WC_Geolocation();

		return $ip->get_ip_address();
	}

	/**
	 * Get time
	 *
	 * @param      $time
	 * @param bool $number
	 * @param bool $calculate
	 *
	 * @return bool|string
	 */
	protected function time_substract( $time, $number = false, $calculate = false ) {
		if ( ! $number ) {
			if ( $time ) {
				$time = strtotime( $time );
			} else {
				return false;
			}
		}
		if ( ! $calculate ) {
			$current_time = current_time( 'timestamp' );
			//			echo "$current_time - $time";
			$time_substract = $current_time - $time;
		} else {
			$time_substract = $time;
		}
		if ( $time_substract > 0 ) {

			/*Check day*/
			$day = $time_substract / ( 24 * 3600 );
			$day = intval( $day );
			if ( $day > 1 ) {
				return $day . ' ' . esc_html__( 'days', 'woocommerce-notification' );
			} elseif ( $day > 0 ) {
				return $day . ' ' . esc_html__( 'day', 'woocommerce-notification' );
			}

			/*Check hour*/
			$hour = $time_substract / ( 3600 );
			$hour = intval( $hour );
			if ( $hour > 1 ) {
				return $hour . ' ' . esc_html__( 'hours', 'woocommerce-notification' );
			} elseif ( $hour > 0 ) {
				return $hour . ' ' . esc_html__( 'hour', 'woocommerce-notification' );
			}

			/*Check min*/
			$min = $time_substract / ( 60 );
			$min = intval( $min );
			if ( $min > 1 ) {
				return $min . ' ' . esc_html__( 'minutes', 'woocommerce-notification' );
			} elseif ( $min > 0 ) {
				return $min . ' ' . esc_html__( 'minute', 'woocommerce-notification' );
			}

			return intval( $time_substract ) . ' ' . esc_html__( 'seconds', 'woocommerce-notification' );

		} else {
			return esc_html__( 'a few seconds', 'woocommerce-notification' );
		}


	}

	/**
	 * Get an array with geoip-infodata
	 *
	 * @param $ip
	 *
	 * @return bool
	 */
	protected function geoCheckIP( $ip ) {
		$params   = new VI_WNOTIFICATION_Admin_Settings();
		$auth_key = $params->get_field( 'ipfind_auth_key' );
		if ( ! $auth_key ) {
			return false;
		}
		//check, if the provided ip is valid
		if ( ! filter_var( $ip, FILTER_VALIDATE_IP ) ) {
			throw new InvalidArgumentException( "IP is not valid" );
		}

		//contact ip-server
		$response = @file_get_contents( 'https://ipfind.co?ip=' . $ip . '&auth=' . trim( $auth_key ) );
		file_put_contents( VI_WNOTIFICATION_CACHE . 'ip.txt', "\n" . date( "H:i:s" ), FILE_APPEND );
		if ( empty( $response ) ) {
			return false;
			throw new InvalidArgumentException( "Error contacting Geo-IP-Server" );

		} else {
			$response = json_decode( $response );
		}

		$ipInfo["city"]    = $response->city;
		$ipInfo["country"] = $response->country;

		return $ipInfo;
	}

}