<?php 

/**
* 
*/
class JVRevSlider
{
	
	private $id;
	private $title;
	private $alias;
	private $arrParams;
	private $arrSlides = null;
	private $db;

	function __construct() {
		$this->db = new UniteDBRev();
	}
	/**
	 * 
	 * export slider from data, output a file for download
	 */
	public function exportSlider($useDummy = false){
		$export_zip = true;
		if(function_exists("unzip_file") == false){				
			if( UniteZipRev::isZipExists() == false)
				$export_zip = false;
		}
		
		if(!class_exists('ZipArchive')) $export_zip = false;
		
		if($export_zip){                  
            
            $sliderParams = $this->getParamsForExport();
            $arrSlides = $this->getSlidesForExport($useDummy);
            $arrStaticSlide = $this->getStaticSlideForExport($useDummy);
            
            $arrSliderExport = array("params"=>$sliderParams,"slides"=>$arrSlides);
            if(!empty($arrStaticSlide))
                $arrSliderExport['static_slides'] = $arrStaticSlide;
            
            $strExport = serialize($arrSliderExport);
                                                                                            
            $exportname =(!empty($this->alias)) ? $this->alias.'.zip' : "slider_export.zip";
            $urlExportZip = apply_filters( 'jvtheme_import_demos', 'theme01' );
            $urlExportZip = implode( '/', array( rtrim( $urlExportZip, '/' ), jvdatasample::$urlExportZip) );
            
            do_action( 'jvtheme_cfolder', $urlExportZip );
            
            $urlExportZip = implode( '/', array( rtrim( jvdatasample::getImportDataFolderPath(), '/' ), $urlExportZip, $exportname ) );
                                       
			$zip = new ZipArchive;
			$success = $zip->open( $urlExportZip, ZIPARCHIVE::CREATE | ZipArchive::OVERWRITE);
			
			if($success !== true)
				throwError("Can't create zip file: " . $urlExportZip);
			
			
			$usedCaptions = array();
			$usedAnimations = array();
			$usedImages = array();
			if(!empty($arrSlides) && count($arrSlides) > 0){
				foreach($arrSlides as $key => $slide){
					if(isset($slide['params']['image']) && $slide['params']['image'] != '') $usedImages[$slide['params']['image']] = true; //['params']['image'] background url
					
					if(isset($slide['layers']) && !empty($slide['layers']) && count($slide['layers']) > 0){
						foreach($slide['layers'] as $lKey => $layer){
							if(isset($layer['style']) && $layer['style'] != '') $usedCaptions[$layer['style']] = true;
							if(isset($layer['animation']) && $layer['animation'] != '' && strpos($layer['animation'], 'customin') !== false) $usedAnimations[str_replace('customin-', '', $layer['animation'])] = true;
							if(isset($layer['endanimation']) && $layer['endanimation'] != '' && strpos($layer['endanimation'], 'customout') !== false) $usedAnimations[str_replace('customout-', '', $layer['endanimation'])] = true;
							if(isset($layer['image_url']) && $layer['image_url'] != '') $usedImages[$layer['image_url']] = true; //image_url if image caption
						}
					}
				}
			}
			if(!empty($arrStaticSlide) && count($arrStaticSlide) > 0){
				foreach($arrStaticSlide as $key => $slide){
					if(isset($slide['params']['image']) && $slide['params']['image'] != '') $usedImages[$slide['params']['image']] = true; //['params']['image'] background url
					
					if(isset($slide['layers']) && !empty($slide['layers']) && count($slide['layers']) > 0){
						foreach($slide['layers'] as $lKey => $layer){
							if(isset($layer['style']) && $layer['style'] != '') $usedCaptions[$layer['style']] = true;
							if(isset($layer['animation']) && $layer['animation'] != '' && strpos($layer['animation'], 'customin') !== false) $usedAnimations[str_replace('customin-', '', $layer['animation'])] = true;
							if(isset($layer['endanimation']) && $layer['endanimation'] != '' && strpos($layer['endanimation'], 'customout') !== false) $usedAnimations[str_replace('customout-', '', $layer['endanimation'])] = true;
							if(isset($layer['image_url']) && $layer['image_url'] != '') $usedImages[$layer['image_url']] = true; //image_url if image caption
						}
					}
				}
			}
			
			$styles = '';
			if(!empty($usedCaptions)){
				$captions = array();
				foreach($usedCaptions as $class => $val){
					$cap = RevOperations::getCaptionsContentArray($class);
					if(!empty($cap))
						$captions[] = $cap;
				}
				$styles = UniteCssParserRev::parseArrayToCss($captions, "\n", false);
			}
			
			$animations = '';
			if(!empty($usedAnimations)){
				$animation = array();
				foreach($usedAnimations as $anim => $val){
					$anima = RevOperations::getFullCustomAnimationByID($anim);
					if($anima !== false) $animation[] = RevOperations::getFullCustomAnimationByID($anim);
					
				}
				if(!empty($animation)) $animations = serialize($animation);
			}
			
			//add images to zip
			if(!empty($usedImages)){
				$upload_dir = UniteFunctionsWPRev::getPathUploads();
				$upload_dir_multisiteless = wp_upload_dir();
				$cont_url = $upload_dir_multisiteless['baseurl'];
				$cont_url_no_www = str_replace('www.', '', $upload_dir_multisiteless['baseurl']);
				$upload_dir_multisiteless = $upload_dir_multisiteless['basedir'].'/';
				
				
				foreach($usedImages as $file => $val){
					if($useDummy == "true"){ //only use dummy images
						
					}else{ //use the real images
						if(strpos($file, 'http') !== false){
							$checkpath = str_replace(array($cont_url, $cont_url_no_www), '', $file);
							
							if(is_file($upload_dir.$checkpath)){
								$zip->addFile($upload_dir.$checkpath, 'images/'.$checkpath);
							}elseif(is_file($upload_dir_multisiteless.$checkpath)){
								$zip->addFile($upload_dir_multisiteless.$checkpath, 'images/'.$checkpath);
							}
						}else{
							if(is_file($upload_dir.$file)){
								$zip->addFile($upload_dir.$file, 'images/'.$file);
							}elseif(is_file($upload_dir_multisiteless.$file)){
								$zip->addFile($upload_dir_multisiteless.$file, 'images/'.$file);
							}
						}
					}
				}
			}
			
			
			$zip->addFromString("slider_export.txt", $strExport); //add slider settings
			if(strlen(trim($animations)) > 0) $zip->addFromString("custom_animations.txt", $animations); //add custom animations
			if(strlen(trim($styles)) > 0) $zip->addFromString("dynamic-captions.css", $styles); //add dynamic styles
			
			$static_css = RevOperations::getStaticCss();
			$zip->addFromString("static-captions.css", $static_css); //add slider settings
			$zip->close();       
            
		}else{ //fallback, do old export
			
		
			$sliderParams = $slider->getParamsForExport();
			$arrSlides = $slider->getSlidesForExport();
			
			$arrSliderExport = array("params"=>$sliderParams,"slides"=>$arrSlides);
			
			$strExport = serialize($arrSliderExport);
			
			if(!empty($slider->alias))
				$filename = $slider->alias.".txt";
			else
				$filename = "slider_export.txt";
			
			echo $strExport;
		}
	}
	
	/**
	 * 
	 * init slider by db data
	 * 
	 */
	public function initByDBData($arrData){
		
		$this->id = $arrData["id"];
		$this->title = $arrData["title"];
		$this->alias = $arrData["alias"];
		
		$params = $arrData["params"];
		$params = (array)json_decode($params);
		
		$this->arrParams = $params;
	}
	
	
	/**
	 * 
	 * init the slider object by database id
	 */
	public function initByID($sliderID){
		UniteFunctionsRev::validateNumeric($sliderID,"Slider ID");
		$sliderID = $this->db->escape($sliderID);
		
		try{
			$sliderData = $this->db->fetchSingle(GlobalsRevSlider::$table_sliders,"id={$sliderID}");								
		}catch(Exception $e){
			UniteFunctionsRev::throwError("Slider with ID: $sliderID Not Found");
		}
		
		$this->initByDBData($sliderData);
	}

	/**
	 * 
	 * get slider params for export slider
	 */
	private function getParamsForExport(){
		$exportParams = $this->arrParams;
		
		//modify background image
		$urlImage = UniteFunctionsRev::getVal($exportParams, "background_image");
		if(!empty($urlImage))
			$exportParams["background_image"] = $urlImage;
		
		return($exportParams);
	}

	/**
	 * 
	 * get slides for export
	 */
	private function getSlidesForExport($useDummy = false){
		$arrSlides = $this->getSlidesFromGallery(false, true);
		$arrSlidesExport = array();
		
		foreach($arrSlides as $slide){
			$slideNew = array();
			$slideNew["id"] = $slide->getID();
			$slideNew["params"] = $slide->getParamsForExport();
			$slideNew["slide_order"] = $slide->getOrder();
			$slideNew["layers"] = $slide->getLayersForExport($useDummy);
			$arrSlidesExport[] = $slideNew;
		}
		
		return($arrSlidesExport);
	}

	/**
	 * 
	 * get slides of the current slider
	 */
	public function getSlidesFromGallery($publishedOnly = false, $allwpml = false){
		
		$this->validateInited();
		
		$arrSlides = array();
		$arrSlideRecords = $this->db->fetch(GlobalsRevSlider::$table_slides,"slider_id=".$this->id,"slide_order");

		
		$arrChildren = array();
		
		foreach ($arrSlideRecords as $record){
			$slide = new RevSlide();
			$slide->initByData($record);
			
			$slideID = $slide->getID();
			$arrIdsAssoc[$slideID] = true;

			if($publishedOnly == true){
				$state = $slide->getParam("state","published");
				if($state == "unpublished"){
					continue;
				}
			}
			
			$parentID = $slide->getParam("parentid","");
			if(!empty($parentID)){
				$lang = $slide->getParam("lang","");
				if(!isset($arrChildren[$parentID]))
					$arrChildren[$parentID] = array();
				$arrChildren[$parentID][] = $slide;
				if(!$allwpml)
					continue;	//skip adding to main list
			}
			
			//init the children array
			$slide->setArrChildren(array());
			
			$arrSlides[$slideID] = $slide;
		}
		
		//add children array to the parent slides
		foreach($arrChildren as $parentID=>$arr){
			if(!isset($arrSlides[$parentID])){
				continue;
			}
			$arrSlides[$parentID]->setArrChildren($arr);
		}
		
		$this->arrSlides = $arrSlides;

		return($arrSlides);
	}
	/**
	 * 
	 * validate that the slider is inited. if not - throw error
	 */
	private function validateInited(){
		if(empty($this->id))
			UniteFunctionsRev::throwError("The slider is not inited!");
	}

	/**
	 * 
	 * get slides for export
	 */
	private function getStaticSlideForExport($useDummy = false){
		$arrSlidesExport = array();
		
		$slide = new RevSlide();
		
		$staticID = $slide->getStaticSlideID($this->id);
		if($staticID !== false){
			$slideNew = array();
			$slide->initByStaticID($staticID);
			$slideNew["params"] = $slide->getParamsForExport();
			$slideNew["slide_order"] = $slide->getOrder();
			$slideNew["layers"] = $slide->getLayersForExport($useDummy);
			$arrSlidesExport[] = $slideNew;
		}
		
		return($arrSlidesExport);
	}

	public function getAllSlider(){
        
		return $this->db->fetch( GlobalsRevSlider::$table_sliders );
        
	}
}