<?php 
/*
Template Name: Affiliate Page
*/
get_header(); ?>
<div class="affiliate-banner">
<?php if (have_posts()) :
	    global $post;
	    while (have_posts()) : the_post(); setup_postdata($post);
	       if ( has_post_thumbnail() ) {
	       		echo get_the_post_thumbnail( $_post->ID, 'full' );
	       }
	    endwhile;
endif; ?> 
</div>
<ul class="double-cloumn clearfix">     
	<div class="full-width affiliate-page">
		<?php echo do_shortcode('[affiliate_area]');?>
	</div>  
</ul>  
<?php get_footer(); ?>