<?php 
/*
Template Name: Full Width 
*/
get_header(); ?>    
<ul class="double-cloumn clearfix">     
	<div class="full-width">
		<ul> 
		<?php if (have_posts()) :
		    global $post;
		    while (have_posts()) : the_post(); setup_postdata($post);
		        get_template_part("/functions/fetch-post");
		    endwhile;
		else :
		    ocmx_no_posts();
		endif; ?> 
		<?php if(comments_open()) {comments_template();} ?>
		</ul>
	</div>  
</ul>  
<?php get_footer(); ?>