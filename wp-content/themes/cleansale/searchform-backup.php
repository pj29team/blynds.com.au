<div id="fullscreensearch">
	<div class="closesearch">
		<a href="#searchclose"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a>
	</div>	
	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<input type="search" class="search-input" placeholder="<?php echo esc_attr_x( 'Search BlindsCity...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		<input type="submit" class="search-submit" value="Search" />
		<input type="hidden" value="product" name="post_type" id="post_type" />
	</form>
</div>
<a class="dsk-s search-toggle"><i class="fa fa-search" aria-hidden="true"></i></a>
<script type="text/javascript">
	jQuery('.search-toggle').click(function(e){
		jQuery('#fullscreensearch').addClass('open');
		e.preventDefault();
	});
	jQuery('.closesearch').click(function(e){
		jQuery('#fullscreensearch').removeClass('open');
		e.preventDefault();
	});
</script>