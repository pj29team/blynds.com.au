	</div><!--End Content Container -->
	<div id="pre-footer-container">
		<div id="pre-footer" class="clearfix">
		<?php if ( is_active_sidebar( 'pf-left' ) ) : ?>
			<?php dynamic_sidebar( 'pf-left' ); ?>
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'pf-right' ) ) : ?>
			<?php dynamic_sidebar( 'pf-right' ); ?>
		<?php endif; ?>
		</div>
	</div>
	<div id="footer-container">
		<div id="footer" class="clearfix">
			<?php

			$subscribe_form_text = get_option('ocmx_subscribe_form_text');

			?>
			<fieldset id="footer-newsletter">
				<legend>Get the newsletter</legend>
				<p><?php echo $subscribe_form_text; ?></p>
				<?php if (function_exists('tve_leads_form_display')) { tve_leads_form_display(0, 22053); } ?>
			</fieldset>
			<fieldset id="footer-reviews">
				<p style="text-align: center;">
					<a href="https://www.productreview.com.au/p/blinds-city.html" target="_blank"><img class="aligncenter" src="//d2uod8gew2p4yv.cloudfront.net/badge/323965/light-ld.png" alt="Blinds City reviews" width="160px" /></a>
				</p>
			</fieldset>
			<fieldset id="footer-confidence">
				<legend>Shop with Confidence</legend>
				<!-- <img src="<?php bloginfo('template_directory'); ?>/images/new/secure.jpg" alt="" class="first"/> -->
				<p>Secure and safe transaction. We accept payments via:</p>
				<img src="<?php bloginfo('template_directory'); ?>/images/new/cards.jpg" alt=""/>
			</fieldset>
			<fieldset id="footer-links">
				<div id="footer-navigation-container">
					<?php wp_nav_menu(array(
						'menu' => 'Footer Nav',
						'menu_id' => 'footer-nav',
						'menu_class' => 'clearfix',
						'sort_column' 	=> 'menu_order',
						'theme_location' => 'secondary',
						'container' => 'ul',
						'fallback_cb' => 'ocmx_fallback_secondary')
					); ?>
				</div>
				<p><?php echo get_option("ocmx_cleansale_footer"); ?></p>
			</fieldset>
		</div>
		<!--End footer -->
	</div><!--end Footer Container -->
</div>
</div>
<?php wp_footer(); ?>
<div style="display:none" itemscope itemtype="http://schema.org/LocalBusiness">
<p itemprop="name">Blinds City</p>
<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"></p>
<p itemprop="streetAddress">84 Hotham St</p>
<p itemprop="addressLocality">Preston</p>
<p itemprop="addressRegion">Victoria</p>
<p itemprop="postalCode">3072</p>
<p itemprop="telephone">1300 055 888</p>
<meta itemprop="latitude" content="-37.746876" />
<meta itemprop="longitude" content="145.008321" />
</div>
</body>
</html>
