<?php 
/*
Template Name: New Homepage
*/
get_header(); ?>
<style>
	#home-content { padding: 0; margin-top: 0; }
	#home-content .copy, #home-content .copy h1 { margin: 0; }
	h2.post-title { display: none; }
</style>
<?php echo do_shortcode( '[rev_slider new-big-banner]' ); ?>
<div class="double-cloumn clearfix">   
	<div id="home-content-wrapper" class="homepage-wrapper">
		<ul id="home-content" class="clearfix">    
			<?php if (have_posts()) :
			    global $post;
			    while (have_posts()) : the_post(); setup_postdata($post);
			        get_template_part("/functions/fetch-post");
			    endwhile;
			else :
			    ocmx_no_posts();
			endif; ?> 
		</ul>
	</div>
</div>
<?php if ($green_delivery_text = get_option('ocmx_green_delivery_text')): ?>
<div id="free-delivery-green">
	<img src="<?php bloginfo('template_directory'); ?>/images/new/delivery-icon.png" alt=""/>
	<?php echo $green_delivery_text; ?>
</div>
<?php endif; ?>


<?php if ( is_active_sidebar( 'home-free-samples' ) ) : ?>
		<div class="free-samples-container">
			<?php dynamic_sidebar( 'home-free-samples' ); ?>
		</div>
<?php endif; ?>


<?php if ( is_active_sidebar( 'home-hi-pages' ) ) : ?>
		<div class="home-hipages-container">
			<?php dynamic_sidebar( 'home-hi-pages' ); ?>
		</div>
<?php endif; ?>

<div class="badge-icon-block">
	<h3>Why shop with blinds city?</h3>
	<img src="<?php bloginfo('template_directory'); ?>/images/badge-icons.png">
</div>
<?php get_footer(); ?>