<?php
    header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
?>
<?php  global $woocommerce; /* error_reporting(E_ALL | E_STRICT); */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> xmlns:og="http://ogp.me/ns/fb#" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:addthis="http://www.addthis.com/help/api-spec">
<head profile="http://gmpg.org/xfn/11">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<!--Set Viewport -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<!--Get Obox SEO -->
<?php if(get_option("ocmx_seo") == "yes") {
    echo ocmx_site_title();
    echo ocmx_meta_description();
    echo ocmx_meta_keywords();
} else { ?>
<title>
<?php
    global $page, $paged;
    /*wp_title( '|', true, 'right' );
    bloginfo( 'name' );*/
    wp_title();
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description";
    if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf( __( 'Page %s', 'ocmx' ), max( $paged, $page ) );
?>
</title>
<?php } ?>
<!-- Setup OpenGraph support-->
<?php if(get_option("ocmx_open_graph") !="yes") {
    $default_thumb = get_option('ocmx_site_thumbnail');
    $fb_image = get_fbimage();
    if (is_home()):
?>
<meta property="og:title" content="<?php bloginfo('name'); ?>"/>
<meta property="og:description" content="<?php bloginfo('description'); ?>"/>
<meta property="og:url" content="<?php echo home_url(); ?>"/>
<meta property="og:image" content="<?php if(isset($default_thumb) && $default_thumb !==""){echo $default_thumb; } else {echo $fb_image;}?>"/>
<meta property="og:type" content="<?php echo "website";?>"/>
<meta property="og:site_name" content="<?php bloginfo('name'); ?>"/>
<?php else : ?>
<meta property="og:title" content="<?php the_title(); ?>"/>
<meta property="og:description" content="<?php echo strip_tags($post->post_excerpt); ?>"/>
<meta property="og:url" content="<?php the_permalink(); ?>"/>
<meta property="og:image" content="<?php if($fb_image ==""){echo $default_thumb;} else {echo $fb_image;} ?>"/>
<meta property="og:type" content="<?php echo "article"; ?>"/>
<meta property="og:site_name" content="<?php bloginfo('name'); ?>"/>
<meta name="p:domain_verify" content="6c6671a3de5d15b0307daf3ca066b4f9"/>
<?php endif;
}?>
<meta name="google-site-verification" content="gWoa5OabFWFwvElIXmzae2fty5QV7RkBCGs0bqO0CxY" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- CUSTOM STYLES -->
<style type="text/css">
 
#menu-desktop {
    display: block;
}
#menu-mobile {
    display: none;
}
#menu-mobile .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:active {
    background-color: #1bbed1;
}
#menu-mobile .navbar-default .navbar-brand {
    color: #fff;
}
#menu-mobile .navbar-default .navbar-toggle .icon-bar {
    background-color: #fff;
}
#menu-mobile .navbar-default .navbar-toggle {
    border-color: #fff;
}
@media (max-width: 767px) {
    #menu-mobile ul#menu-main-menu > li > a {
        color: #fff;
    }
    #menu-mobile ul#menu-main-menu > li > ul > li > a {
        color: #106e79;
    }
    #menu-mobile ul#menu-main-menu > li > ul > li {
        background-color: #fff;
        padding: 10px;
        border-bottom: 1px solid #1bbed1;
        margin-left: -15px;
        margin-right: -15px;
    }

    #menu-desktop {
        display: none;
    }
    #menu-mobile {
        display: block;
    }
}
</style>

<!-- Begin Styling -->
<?php if(get_option("ocmx_custom_favicon") != "") : ?>
    <link href="<?php echo get_option("ocmx_custom_favicon"); ?>" rel="icon" type="image/png" />
<?php endif; ?>
<?php if(get_option("ocmx_rss_url")) : ?>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php echo get_option("ocmx_rss_url"); ?>" />
<?php else : ?>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<?php endif; ?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_head(); ?>
<!--Get Google Analytics -->
<?php
    if(get_option("ocmx_googleAnalytics")) :
        echo stripslashes(get_option("ocmx_googleAnalytics"));
    endif;
?>
<!-- mail chimp goal tracking -->
<script type="text/javascript">
    var $mcGoal = {'settings':{'uuid':'3760e725864a061771776da4a','dc':'us13'}};
    (function() {
         var sp = document.createElement('script'); sp.type = 'text/javascript'; sp.async = true; sp.defer = true;
        sp.src = ('https:' == document.location.protocol ? 'https://s3.amazonaws.com/downloads.mailchimp.com' : 'http://downloads.mailchimp.com') + '/js/goal.min.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sp, s);
    })(); 
</script>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1193124497399064');
fbq('track', "PageView");
fbq('track', 'ViewContent');
fbq('track', 'AddToCart');
fbq('track', 'AddToWishlist');
fbq('track', 'InitiateCheckout');
fbq('track', 'AddPaymentInfo');
fbq('track', 'Purchase', {value: '1.00', currency: 'USD'});
fbq('track', 'Lead');
fbq('track', 'CompleteRegistration');</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1193124497399064&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TVQLB2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TVQLB2');</script>
<!-- End Google Tag Manager -->

<!-- call tracking code -->
<script type="text/javascript">
(function(a,e,c,f,g,h,b,d){var k={ak:"925363324",cl:"KrU-CPbSgGwQ_NifuQM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
</script>
<!-- End Call tracking code -->

</head>
<body <?php body_class(''); ?>  onload="_googWcmGet('phone-number', '1300055888')" >
 
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TVQLB2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="main-wrapper">
<div id="sidebar-wrapper">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div id="navbar" class="sidebar-nav">
            <div class="mobile-logo">
                <a href="<?php bloginfo('url');?>">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2016/05/BlindsCity_Logo-3.png" />
                </a>
                <a href="#side-toggle" id="side-toggle"><i class="glyphicon glyphicon-remove"></i></a>
            </div>
            <?php 
                wp_nav_menu(array(
                    // Whatever you used to register the nav menu with
                    'theme_location' => 'primary',
                    // Instantiate our class & pass it as an argument
                    'walker' => new Clean_Walker_Nav(),
                    'container' => false,
                    )
                );
            ?>
            <!-- <ul>
                <li>
                    <a class="mbl-s search-toggle"><span>Search</span></a>
                </li>
            </ul> remove search form-->
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
</div>

<div id="page-content-wrapper">
    <div id="page-content-overlay"></div>

<?php if ($delivery_text = get_option('ocmx_top_delivery_text')): ?>
<div id="free-delivery-grey">
    <img src="<?php bloginfo("template_directory"); ?>/images/new/delivery-icon-grey.png" alt=""/>
    <?php echo $delivery_text; ?>
</div>
<?php endif; ?>
<div id="pre-header-container">
        <div id="pre-header" class="clearfix">
        <?php if ( is_active_sidebar( 'ph-left' ) ) : ?>
            <?php dynamic_sidebar( 'ph-left' ); ?>
        <?php endif; ?>
        <?php if ( is_active_sidebar( 'ph-right' ) ) : ?>
            <?php dynamic_sidebar( 'ph-right' ); ?>
        <?php endif; ?>
        </div>
</div>

<div id="header-container">
    <div id="header" class="clearfix">
        <?php if (class_exists( 'Woocommerce' )) {
            ocmx_cart_display();
        } ?>
        <div class="login-header">
            <a href="/my-account"><span class="glyphicon glyphicon-user"></span>My Account</a>
        </div>
        <!--Show Logo -->
        <div class="logo">
            
                <a href="<?php echo home_url(); ?>">
                    <?php if(get_option("ocmx_custom_logo")) : ?>
                        <img src="<?php echo get_option("ocmx_custom_logo"); ?>" alt="<?php bloginfo('name'); ?>" />
                    <?php else : ?>
                        <?php echo strip_tags(bloginfo('name')); ?>
                    <?php endif; ?>
                </a>
            
        </div>
        <!--Begin Nav -->
        <div id="nav-clear"></div>
        <ul id="nav" class="clearfix">
            <li id="menu-item-24" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item current_page_item menu-item-24"><a href="/"><span>Home</span></a></li>
            <li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="/how-to-measure/"><span>How to Measure</span></a></li>
            <li id="menu-item-246" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-246"><a href="/how-to-install/"><span>How to Install</span></a></li>
            <li id="menu-item-contact_us" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-contact_us"><a href="/contact-us/"><span>Contact Us</span></a></li>
        </ul>
    </div><!--End header -->
    <?php // ocmx_cart_popup(); ?>
</div><!--End header-container -->
<!-- start top prodcuts -->
 
<div id="top-products-container">
 <div id="menu-mobile">
    <div class="navbar-header navbar-default">
            <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> -->
            <a href="#menu-toggle" class="navbar-toggle" id="menu-toggle">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <a class="navbar-brand" href="#">BROWSE BLINDS</a>
          </div>
</div>
<div id="menu-desktop">
    <nav class="navbar yamm navbar-default" role="navigation">
      <div class="container">
        <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 3,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'Yamm_Nav_Walker_menu_fallback',
                'walker'            => new Yamm_Nav_Walker())
            );
        ?>
        </div>
    </nav>
</div>
<?php // get_search_form(); remove search form ?>
     
</div>
<!-- END top prodcuts -->
<?php if(is_active_sidebar("slider") && is_home()) : //Show Home Slider on Homepage ?>
    <div class="slider-block">
        <?php dynamic_sidebar("slider"); ?>
    </div>
<?php endif; ?>
<!--Begin Content -->
<div id="content-container" class="clearfix <?php echo get_option( "ocmx_sidebar_layout" ); ?> <?php echo get_option( "ocmx_shop_sidebar_layout" ); ?>">
