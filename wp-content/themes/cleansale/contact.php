<?php
/*
Template Name: Contact 
*/

get_header(); 

global $post;
$location = get_post_meta($post->ID, "map-location", true);
$latlong = get_post_meta($post->ID, "map-latlong", true);
$address_shown =  get_post_meta($post->ID, "address", true);
?>

<ul class="double-cloumn clearfix">

	<?php if($location !="" || $latlong !="") : ?>
		<li id="map-container">
			<div id="map" rel="<?php echo $location; ?>" <?php if($latlong != "") : ?>data-latlong="<?php echo $latlong; ?>"<?php endif; ?>></div>
		</li>
	<?php endif; ?>
	<li id="left-column">
		<ul class="page-container">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<li id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
					<div class="copy clearfix">
						<?php the_content(); ?>
					</div>
				</li>
			<?php endwhile; endif; ?>
		</ul>
	</li>
	<?php if(isset($address_shown) && $address_shown !='') : ?>
		<li id="right-column" class="contact-info">
			<ul class="widget-list blog-sidebar">
				<li class="widget">
					<h4 class="widgettitle"><span><?php _e('Find Us', 'ocmx'); ?></span></h4>
					<div class="copy">
						<p>
							<?php echo $address_shown; ?>
						</p>
					</div>
				</li>
			</ul>
		</li> 
	<?php else : ?>
		<?php get_sidebar(); ?>
	<?php endif; ?>
</ul>

<?php get_footer(); ?>