<?php header('Content-type: text/css'); ?>
<?php if(get_option("ocmx_ignore_colours") != "yes"): ?>

	<?php if(get_option("ocmx_general_font_color")) : ?>
		body{color: <?php echo get_option('ocmx_general_font_color');?>;}
	<?php endif; ?>
    
    <?php if(get_option("ocmx_logo_font_color")) : ?>
		.logo h1 a{color: <?php echo get_option('ocmx_logo_font_color');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_navigation_font_color")) : ?>
		ul#nav li a{color: <?php echo get_option('ocmx_navigation_font_color');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_navigation_hover")) : ?>
		ul#nav li a:hover{color: <?php echo get_option('ocmx_navigation_hover');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_slider_container")) : ?>
		.feature{background-color: <?php echo get_option('ocmx_slider_container');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_slider_arrows")) : ?>
		.next, .previous{background-color: <?php echo get_option('ocmx_slider_arrows');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_slider_arrows_hover")) : ?>
		.next:hover, .previous:hover{background-color: <?php echo get_option('ocmx_slider_arrows_hover');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_post_container")) : ?>
		.feature .overlay, ul#nav, #content-container, .widgettitle a, .widgettitle span, .header-cart .shopping-cart{background-color: <?php echo get_option('ocmx_post_container');?> !important;}
        .feature .overlay, .feature .left-column, .feature .right-column li{border-color: <?php echo get_option('ocmx_post_container');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_price_meta")) : ?>
		.price .amount, .amount, .price{color: <?php echo get_option('ocmx_price_meta');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_body_links")) : ?>
		.widgettitle a, .widgettitle span, .copy p a, .widget .content a{color: <?php echo get_option('ocmx_body_links');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_body_links_hover")) : ?>
		.logo h1 a:hover, .copy p a:hover, .widget .content a:hover{color: <?php echo get_option('ocmx_body_links_hover');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_widget_titles_font_color")) : ?>
		h3.widgettitle, h4.widgettitle, .widgettitle a, .section-title, .section-title a, .widgettitle a, .widgettitle span, .page-title, .page-title a{color: <?php echo get_option('ocmx_widget_titles_font_color');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_post_titles_font_color")) : ?>
		.post-title, .post-title a, .page-title, .four-column .post-title a, .three-column .post-title a{color: <?php echo get_option('ocmx_post_titles_font_color');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_post_titles_hover")) : ?>
		.products h3, .post-title:hover, .post-title a:hover, .page-title:hover{color: <?php echo get_option('ocmx_post_titles_hover');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_date_meta")) : ?>
		h5.date, .archives_list .date, .blog-main-post-container .post-title-block .date{color: <?php echo get_option('ocmx_date_meta');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_buttons")) : ?>
		.shipping-calculator-button, .ui-slider-handle, .more-info, .copy .button, textarea, input, button, .button, #submit, .submit_button, .submitbutton, .gform_footer input.button, .product-content button, .quantity .plus, .quantity .minus, div.product .woocommerce_tabs ul.tabs li{background-color: <?php echo get_option('ocmx_buttons');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_buttons_text")) : ?>
		.shipping-calculator-button, .more-info, .copy .button, textarea, input, button, .button, #submit, .submit_button, .submitbutton, .gform_footer input.button, .product-content button, .quantity .plus, .quantity .minus, div.product .woocommerce_tabs ul.tabs li{color: <?php echo get_option('ocmx_buttons_text');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_buttons_borders")) : ?>
		 .feature .previous, .feature .next, .shipping-calculator-button, .widget_product_search #searchform input#s, textarea, input, button, .more-info, .copy .button, .button, #submit, .submit_button, .submitbutton, .gform_footer input.button, .product-content button, .quantity .plus, .quantity .minus, div.product .woocommerce_tabs ul.tabs li, div.product .woocommerce_tabs .panel{border-color: <?php echo get_option('ocmx_buttons_borders');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_buttons_hover")) : ?>
		.more-info:hover, .copy .button:hover, .button:hover, #submit:hover, .submit_button:hover, .submitbutton:hover, .gform_footer input.button:hover, .product-content button:hover, .quantity .plus:hover, .quantity .minus:hover{background-color: <?php echo get_option('ocmx_buttons_hover');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_buttons_text_hover")) : ?>
		.more-info:hover, .copy .button:hover, .button:hover, #submit:hover, .submit_button:hover, .submitbutton:hover, .gform_footer input.button:hover, .product-content button:hover, .quantity .plus:hover, .quantity .minus:hover{color: <?php echo get_option('ocmx_buttons_text_hover');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_buttons_borders_hover")) : ?>
		.more-info:hover, .copy .button:hover, .button:hover, #submit:hover, .submit_button:hover, .submitbutton:hover, .gform_footer input.button:hover, .product-content button:hover, .quantity .plus:hover, .quantity .minus:hover{border-color: <?php echo get_option('ocmx_buttons_borders_hover');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_footer_container")) : ?>
		#footer-container{background-color: <?php echo get_option('ocmx_footer_container');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_widget_footer_titles_font_color")) : ?>
		#footer h4, #footer h4 a, #footer .widgettitle{color: <?php echo get_option('ocmx_widget_footer_titles_font_color');?> !important;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_footer_text")) : ?>
		.footer-text p, #footer{color: <?php echo get_option('ocmx_footer_text');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_footer_links")) : ?>
		.footer-text a, #footer a{color: <?php echo get_option('ocmx_footer_links');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_footer_links_hover")) : ?>
		#footer a:hover, .obox-credit a:hover{color: <?php echo get_option('ocmx_footer_links_hover');?>;}
	<?php endif; ?>
	
	<?php if(get_option("ocmx_border_color")) : ?>
		ul#nav, ul#nav li a, .feature, .archive .blog-main-post-container li.post, ul.widget-list li.widget, ul.widget-list li.widget li, .footer-text{border-color: <?php echo get_option('ocmx_border_color');?>;}
	<?php endif; ?>
	
<?php endif; ?>

<?php if(get_option("ocmx_custom_css") != ""): ?>
	<?php echo get_option("ocmx_custom_css"); ?>
<?php endif; ?>