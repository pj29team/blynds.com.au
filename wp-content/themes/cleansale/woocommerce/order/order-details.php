<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see         http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$order = wc_get_order( $order_id );

$show_purchase_note = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
?>
<div class="col-1">
    <h2><?php _e( 'Order Details', 'woocommerce' ); ?></h2>
    <ul class="woocommerce-thankyou-order-details order_details">
        <li class="order">
            <?php _e( 'Order Number:', 'woocommerce' ); ?>
            <strong><?php echo $order->get_order_number(); ?></strong>
        </li>
        <li class="date">
            <?php _e( 'Date:', 'woocommerce' ); ?>
            <strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
        </li>
        <li class="total">
            <?php _e( 'Total:', 'woocommerce' ); ?>
            <strong><?php echo $order->get_formatted_order_total(); ?></strong>
        </li>
        <?php if ( $order->payment_method_title ) : ?>
        <li class="method">
            <?php _e( 'Payment Method:', 'woocommerce' ); ?>
            <strong><?php echo $order->payment_method_title; ?></strong>
        </li>
        <?php endif; ?>
    </ul>
    <script>
        jQuery(document).ready(function (){
            var var_qty = jQuery('dd.variation-Quantity');
            if (var_qty.length != 0){
                var qty_n = var_qty.find('p').text();
                jQuery('strong.product-quantity').html('&times; ' + qty_n);
            }
        });
    </script>
    <table class="shop_table order_details">
        <thead>
            <tr>
                <th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
                <th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach( $order->get_items() as $item_id => $item ) {
                    $product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
                    $purchase_note = get_post_meta( $product->id, '_purchase_note', true );

                    wc_get_template( 'order/order-details-item.php', array(
                        'order'                 => $order,
                        'item_id'               => $item_id,
                        'item'                  => $item,
                        'show_purchase_note'    => $show_purchase_note,
                        'purchase_note'         => $purchase_note,
                        'product'               => $product,
                    ) );
                }
            ?>
            <?php do_action( 'woocommerce_order_items_table', $order ); ?>
        </tbody>
        <tfoot>
            <?php
                foreach ( $order->get_order_item_totals() as $key => $total ) {
                    ?>
                    <tr>
                        <th scope="row"><?php echo $total['label']; ?></th>
                        <td><?php echo $total['value']; ?></td>
                    </tr>
                    <?php
                }
            ?>
        </tfoot>
    </table>
</div>
<div class="col-2">
    <?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
    <?php wc_get_template( 'order/order-details-customer.php', array( 'order' =>  $order ) ); ?>
</div>
