<?php
/*
Template Name: Default Category
*/

get_header();
global $product, $woocommerce_loop;
$term =	$wp_query->queried_object;
$_product = $product; ?>
<style type="text/css"> 
	.default-cat span.amount, h2:not(.title-0) { display: none; }
	.products > li.product_cat-diy, h2.sub-product-list-title { clear:both; }
	.products > li.product_cat-diy ~ li.product_cat-diy { clear: none; }
	h2.sub-product-list-title { font-size: 22px; padding-top: 20px; margin-left: 10px; }
</style>
<div class="double-cloumn clearfix default-cat">
	<div class="full-width homepage-wrapper">
		<?php do_action('woocommerce_before_single_product', $post, $_product); ?>
		<h1 class="sub-product-list-title"><?php single_term_title(); ?></h1>
		<div class="products">
			<?php if ( have_posts() ) : ?>
			<?php
			
			$c = 0;
			$b = 0;

			do_action( 'woocommerce_before_shop_loop' ); 
			
			woocommerce_product_loop_start();
			woocommerce_product_subcategories(); 
			
			while ( have_posts() ) : the_post(); 
				$terms = get_the_terms( get_the_ID(), 'product_cat');
				foreach ( $terms as $term ) {
				    $termslug[] = $term->slug;
				}
				
				if ( in_array('diy', $termslug) && in_array('plantation-shutters', $termslug) ) {	
			?>
			<h2 class="sub-product-list-title title-<?php echo $c; ?>">DIY SHUTTERS</h2>
			<?php 
			$c++; 
			} elseif ( in_array('plantation-shutters', $termslug) ) { ?>
				<h2 class="sub-product-list-title title-<?php echo $b; ?>">INSTALLATION BY US</h2>
			<?php 
			$b++;
			} 
			?>
				<li <?php post_class( $classes ); ?>>
					<div class="home-product-wrapper archive-product-item">
					<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
					<a href="<?php the_permalink(); ?>">
						<?php
							/**
							 * woocommerce_before_shop_loop_item_title hook
							 *
							 * @hooked woocommerce_show_product_loop_sale_flash - 10
							 * @hooked woocommerce_template_loop_product_thumbnail - 10
							 */
							do_action( 'woocommerce_before_shop_loop_item_title' );

							/**
							 * woocommerce_shop_loop_item_title hook
							 *
							 * @hooked woocommerce_template_loop_product_title - 10
							 */
							

							/**
							 * woocommerce_after_shop_loop_item_title hook
							 *
							 * @hooked woocommerce_template_loop_rating - 5
							 * @hooked woocommerce_template_loop_price - 10
							 */
							do_action( 'woocommerce_after_shop_loop_item_title' );

						?>
					</a>
					<?php

						/**
						 * woocommerce_after_shop_loop_item hook
						 *
						 * @hooked woocommerce_template_loop_add_to_cart - 10
						 */
						do_action( 'woocommerce_after_shop_loop_item' );

					?>
					
						<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
						<?php echo woocommerce_price($product->get_price_including_tax()); ?>
						<a href="<?php the_permalink(); ?>" class="home-product-select-button">
							Select 
						</a>
				
					</div>
				</li>
			<?php
			endwhile; // end of the loop.
			
			?>
			<?php woocommerce_product_loop_end(); ?>
			<?php do_action( 'woocommerce_after_shop_loop' );?>
		<?php
		
		elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : 
			woocommerce_get_template( 'loop/no-products-found.php' ); 
		endif;
		
		?>
		</div>
		<?php motionpic_pagination("clearfix", "pagination clearfix"); ?>
	</div>
</div>
<?php get_footer(); ?>