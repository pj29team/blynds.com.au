<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 	2.4.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="variation clearfix">

	<?php 
	$col_indicator = 0;
	
	foreach ( $item_data as $data ) : 
		$col_indicator++;
		
		if( $col_indicator ==1 || $col_indicator%3==1){
			if ($data['key'] == 'Quantity'){ //separate Filter(1) Fields
				echo '<div class="varation-col" style="clear:both;">';
			} else {
			echo '<div class="varation-col"> ';
			}
		}

				// if($data['key']!='Quantity'){

				?>
					<p>
					<span> <?php echo wp_kses_post( $data['key'] ); ?>: </span>
					<?php echo str_replace( array('<p>','</p>'),'',wp_kses_post( wpautop( $data['display'] ) ) ); ?>
					</p>
	<?php 
				// }

		if( $col_indicator%3==0){
			echo '</div> ';
		}

	endforeach; 

	if( $col_indicator%3!=0){
			echo '</div> ';
		}

	?>
</div>
