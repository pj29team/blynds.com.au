<?php  global $themename, $input_prefix;

/*****************/
/* Theme Details */

$themename = "CleanSale";
$themeid = "cleansale";
$productid = "1630";
$presstrendsid = "v0gm8mbcsab41tualyvjaeh78jqbiahs3";

/**********************/
/* Include OCMX files */

$include_folders = array("/ocmx/includes/", "/ocmx/theme-setup/", "/ocmx/widgets/", "/ocmx/front-end/", "/ajax/", "/ocmx/interface/");

// This is a hack, to avoid the "headers already sent by...." error which pops up when you call pages directly from wp-admin/ like edit.php for example
include_once (get_template_directory()."/ocmx/folder-class.php");
include_once (get_template_directory()."/ocmx/load-includes.php");
include_once (get_template_directory()."/ocmx/custom.php");
include_once (get_template_directory().'/yamm-nav-walker.php');

/**********************/
/* "Hook" up the OCMX */

update_option("ocmx_font_support", true);
add_action('init', 'ocmx_add_scripts');
add_action('after_setup_theme', 'ocmx_setup');
add_theme_support( 'woocommerce' );

function add_widgetized_pages(){
	global $wpdb;
	$get_widget_pages = $wpdb->get_results("SELECT * FROM ".$wpdb->postmeta." WHERE `meta_key` = '_wp_page_template' AND  `meta_value` = 'widget-page.php'");
	foreach($get_widget_pages as $pages) :
		$post = get_post($pages->post_id);
		register_sidebar(array("name" => $post->post_title." Body", "id" => $post->post_title."-body", "description" => "Place all 'Home Page Widgets' here.", "before_title" => '<h4 class="widgettitle">', "after_title" => '</h4><div class="content">', 'before_widget' => '<li id="%1$s" class="widget %2$s">', 'after_widget' => '</div></li>'));
	endforeach;
}
add_action("init", "add_widgetized_pages");

/*********************/
/* Load Localization */

load_theme_textdomain('ocmx', get_template_directory() . '/lang');

/***********************/
/* Add OCMX Menu Items */

add_action('admin_menu', 'ocmx_add_admin');
function ocmx_add_admin() {
	global $wpdb;

	//Check if we need to upgrade the Gallery Table
	check_gallery_table();

	add_object_page("Theme Options", "Theme Options", 'edit_themes', basename(__FILE__), '', get_template_directory_uri(). '/ocmx/images/ocmx.png');

	add_submenu_page(basename(__FILE__), "General Options", "General", "manage_options", basename(__FILE__), 'ocmx_general_options');
	add_submenu_page(basename(__FILE__), "Typography", "Typography", "manage_options", "ocmx-fonts", 'ocmx_font_options');
	add_submenu_page(basename(__FILE__), "Customize", "Customize", "manage_options", "customize.php");
	add_submenu_page(basename(__FILE__), "Help", "Help", "manage_options", "obox-help", 'ocmx_welcome_page');
};

/*****************/
/* Add Nav Menus */

if (function_exists('register_nav_menus')) :
	register_nav_menus( array(
		'primary' => __('Primary Navigation', '$themename'),
		'secondary' => __('Footer Navigation', '$themename'),
		'mega_menu' => __('Main Mega Menu', '$themename')
	) );
endif;

/************************************************/
/* Fallback Function for WordPress Custom Menus */

function ocmx_fallback() {
	echo '<ul id="nav" class="clearfix">';
	wp_list_pages('title_li=&');
	echo '</ul>';
}


/**************************/
/* WP 3.4 Support         */

global $wp_version;
if ( version_compare( $wp_version, '3.4', '>=' ) )
	add_theme_support( 'custom-background' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );

if ( ! isset( $content_width ) ) $content_width = 940;

/******************************************************************************/
/* Each theme has their own "No Posts" styling, so it's kept in functions.php */

function ocmx_no_posts(){ ?>
<li class="post transparent-container">
    <div class="content clearfix">
	<h3 class="post-title"><a href="#"><?php _e("Page Not Found", "ocmx"); ?></a></h3>
	<div class="post-meta clearfix"></div>
	<div class="copy <?php echo $image_class; ?>">
	 <?php _e("The page you are looking for does not exist", "ocmx"); ?>
	</div>
	</div>
</li>
<?php
}
/**************************/
/* Set the Excerpt Length */
function new_excerpt_length($length) {
	return 35;
}
function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_length', 'new_excerpt_length');
add_filter('excerpt_more', 'new_excerpt_more');

function my_admin_scripts() {
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
}

function my_admin_styles() {
	wp_enqueue_style('thickbox');
}

add_action('admin_print_scripts', 'my_admin_scripts');
add_action('admin_print_styles', 'my_admin_styles');

/**************************/
/* Set Up Thumbnails      */

function ocmx_setup_image_sizes() {
	//image info: (name, width, height, force-crop)
	add_image_size('630x380', 630, 380, true); //slider
	add_image_size('250x165', 250, 165, true); //slider side
	add_image_size('80x80', 80, 80, true);
	add_image_size('106x106', 106, 106, true);
	add_image_size('4-3-medium', 660, 495, true);
	add_image_size('660auto', 660);
}
add_action( 'after_setup_theme', 'ocmx_setup_image_sizes' );

/**************************/
/* Facebook Support      */

function get_fbimage() {
	global $post;
	if ( !is_single() ){
		return '';
	}
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', '' );
	$fbimage = null;
	if ( has_post_thumbnail($post->ID) ) {
		$fbimage = $src[0];
	} else {
		global $post, $posts;
		$fbimage = '';
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i',
		$post->post_content, $matches);
		if(!empty($matches[1]))
			$fbimage = $matches [1] [0];
	}
	if(empty($fbimage)) {
		$fbimage = get_the_post_thumbnail($post->ID);
	}
	return $fbimage;
}

// Displays up to 3 related products on product posts (determined by common category/tag)
function woocommerce_output_related_products() {
	woocommerce_related_products(array( 'posts_per_page' => 3 ),1); // Display 3 products in rows of 1
}

// Displays up to 3 Upsells on product posts
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_upsells', 15 );

// // Change product title layout
// remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5 );
// add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 1 );

if (!function_exists( 'woocommerce_output_upsells' ) ) {
		function woocommerce_output_upsells() {
		woocommerce_upsell_display( 3,1 ); // Display 3 products in 1 row
	}
}

// Unregister Comment Widget
// 1.3.7
function remove_comments_widget() {
	unregister_widget( 'ocmx_comment_widget' );
}

add_action( 'widgets_init', 'remove_comments_widget' );

// Gravity forms trigger callback for turning off labels.
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Gravity forms trigger callback for turning off HTML filtering.
add_filter( 'woocommerce_gforms_strip_meta_html', 'configure_woocommerce_gforms_strip_meta_html' );

function configure_woocommerce_gforms_strip_meta_html( $strip_html ) {
    $strip_html = false;
    return $strip_html;
}

// Gravity forms :: Populate colours.


/** Register custom Scripts. */
function bc_custom_reg_script() {
	/** Register JavaScript Functions File */
	wp_register_script( 'custom-js', esc_url( trailingslashit( get_stylesheet_directory_uri('template_url') ) . 'scripts/custom.js' ), array( 'jquery' ), '1.5', true );

	/* register fancybox js */
    wp_register_script( 'fancybox-js', esc_url( trailingslashit( get_stylesheet_directory_uri('template_url') ) . 'scripts/fancybox/jquery.fancybox.js' ), array( 'jquery' ), '1.0', true );

 	/** Localize Scripts for ajax Use */
	$php_array = array( 'admin_ajax' => admin_url( 'admin-ajax.php' ) );
	wp_localize_script( 'custom-js', 'php_array', $php_array );
}

add_action( 'wp_enqueue_scripts', 'bc_custom_reg_script', 1 );

/** Enqueue custom Script. */
function bc_custom_enq_script() {
	/** Enqueue JavaScript Functions File */
	wp_enqueue_script( 'custom-js' );

	/* enqueque fancybox js */
	wp_enqueue_script( 'fancybox-js' );
}

add_action( 'wp_enqueue_scripts', 'bc_custom_enq_script' );

/* adding fancybox css */
wp_register_style('add_fancybox_style',esc_url( trailingslashit( get_stylesheet_directory_uri('template_url') ) . 'scripts/fancybox/jquery.fancybox.css') );
wp_enqueue_style('add_fancybox_style');


/* adding custom widgitized area for pre-footer */
function pf_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Pre Footer Left' ),
		'id' => 'pf-left',
		'description' => __( 'Widget area in the pre footer left'),
		'before_widget' => '<div class="pre-footer-left">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>'
	) );

	register_sidebar( array(
		'name' => __( 'Pre Footer Right' ),
		'id' => 'pf-right',
		'description' => __( 'Widget area in the pre footer Right'),
		'before_widget' => '<div class="pre-footer-right">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>'
	) );

	register_sidebar( array(
		'name' => __( 'Pre Header Left' ),
		'id' => 'ph-left',
		'description' => __( 'Widget area in the pre header left'),
		'before_widget' => '<div class="pre-header-left">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>'
	) );

	register_sidebar( array(
		'name' => __( 'Pre Header Right' ),
		'id' => 'ph-right',
		'description' => __( 'Widget area in the pre header Right'),
		'before_widget' => '<div class="pre-header-right">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>'
	) );

	register_sidebar( array(
		'name' => __( 'Footer Top' ),
		'id' => 'footer-top',
		'description' => __( 'Widget area in the Footer top'),
		'before_widget' => '<fieldset id="footer-newsletter">',
		'after_widget' => '</fieldset>',
		'before_title' => '<legend class="widget-title">',
		'after_title' => '</legend>'
	) );

	register_sidebar( array(
		'name' => __( 'Home Hi Pages' ),
		'id' => 'home-hi-pages',
		'description' => __( 'Hi Pages banner on the homepage'),
		'before_widget' => '<div id="hipages-home-content" class="clearfix">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>'
	) );

	register_sidebar( array(
		'name' => __( 'Home Free Samples' ),
		'id' => 'home-free-samples',
		'description' => __( 'Free Samples banner on the homepage'),
		'before_widget' => '<div id="samples-home-content" class="clearfix">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>'
	) );
}

add_action( 'widgets_init', 'pf_widgets_init' );


function bc_cart_totals($cart) {
	foreach ($cart->cart_contents as $item_key => $item) {
		if (isset($item['sample'])) {
			$item['data']->price = 0.00;
		}
	}
}

add_action('woocommerce_before_calculate_totals', 'bc_cart_totals', 99);



function bc_get_item_data($cart_item_key, $key = null, $default = null) {
	$data = (array) WC()->session->get('_bc_product_data');
	
	if (empty($data[$cart_item_key])) {
		$data[$cart_item_key] = array();
	}
	
	if ($key == null) {
		return $data[$cart_item_key] ? $data[$cart_item_key] : $default;
	}
	
	return empty($data[$cart_item_key][$key]) ? $default : $data[$cart_item_key][$key];
}


function bc_set_item_data($cart_item_key, $key, $value) {
	$data = (array) WC()->session->get('_bc_product_data');
	
	if (empty($data[$cart_item_key])) {
		$data[$cart_item_key] = array();
	}
	
	$data[$cart_item_key][$key] = $value;
	
	WC()->session->set('_bc_product_data', $data);
}


function bc_remove_item_data($cart_item_key = null, $key = null) {
	$data = (array) WC()->session->get('_bc_product_data');
	
	if ($cart_item_key == null) {
		WC()->session->set('_bc_product_data', array());
		return;
	}
	
	if (!isset($data[$cart_item_key])) {
		return;
	}
	
	if ($key == null) {
		unset($data[$cart_item_key]);
	} else {
		if (isset($data[$cart_item_key][$key])) {
			unset($data[$cart_item_key][$key]);
		}
	}
	
	WC()->session->set('_bc_product_data', $data);
}

add_filter('woocommerce_before_cart_item_quantity_zero', 'bc_remove_item_data', 1, 1);
add_filter('woocommerce_cart_emptied', 'bc_remove_item_data', 1, 1);


function bc_convert_item_session_to_order_meta($item_id, $values, $cart_item_key) {
	$cart_item_data = bc_get_item_data($cart_item_key);
	
	if (!empty($cart_item_data)) {
		wc_add_order_item_meta($item_id, '_bc_product_data', $cart_item_data);
	}
}

add_action('woocommerce_add_order_item_meta', 'bc_convert_item_session_to_order_meta', 1, 3);


/* AJAX FUNCTION */
function bc_add_sample() {
	global $woocommerce;

	$product_id   = (int)$_POST['prod_id'];
	$variation_id = (int)$_POST['var_id'];
	$swatch_attr  = $_POST['swatch_attr'];
	$swatch_val   = $_POST['swatch_val'];
	$variation    = array();
	$data         = array('sample' => 1);
	
	$product        = new WC_Product_Variation($variation_id);
	$variation_data = $product->get_variation_attributes();
    $attributes     = $product->parent->get_attributes();
	
	foreach ($attributes as $attribute) {
		if (!$attribute['is_variation']) {
			continue;
		}
		
		if (strpos($attribute['name'], 'colour') !== false) {
			continue;
		}
		
		$variation_selected_value =
			isset($variation_data['attribute_' . sanitize_title($attribute['name'])])
			? $variation_data['attribute_' . sanitize_title($attribute['name'])]
			: '';
		
		$description_name  = wc_attribute_label( $attribute['name']);
		$description_value = __('Any', 'woocommerce');
		
		if ($attribute['is_taxonomy']) {
			$post_terms = wp_get_post_terms($product_id, $attribute['name']);
			
			foreach ($post_terms as $term) {
				if ($variation_selected_value === $term->slug) {
					$description_value = apply_filters('woocommerce_variation_option_name', $term->name);
				}
			}
		} else {
			$options = wc_get_text_attributes($attribute['value']);
			
			foreach ($options as $option) {
				if (sanitize_title($variation_selected_value) === $variation_selected_value) {
					if ($variation_selected_value !== sanitize_title($option)) {
						continue;
					}
				} else {
					if ($variation_selected_value !== $option) {
						continue;
					}
				}
				
				$description_value = apply_filters('woocommerce_variation_option_name', $option);
			}
		}
		
		$variation[$description_name] = $description_value;
	}

	$term = get_term_by('slug', $swatch_val, $swatch_attr);

	if ($term) {
		$variation['Colour'] = $term->name;
		
		if ($term->description) {
			$variation['Description'] = $term->description;
		}
	}
	
	$item_key = $woocommerce->cart->add_to_cart(
		$product_id,
		1,
		$variation_id,
		$variation,
		$data
	);
	
	bc_set_item_data($item_key, 'sample', true);

	wp_send_json(array(
		'success' => true
	));
}

add_action('wp_ajax_bc_add_sample', 'bc_add_sample');
add_action('wp_ajax_nopriv_bc_add_sample', 'bc_add_sample');


function bc_order_get_items($items, $order) {
	foreach ($items as $item_id => &$item) {
		if (isset($item['bc_product_data'])) {
			$product_data = maybe_unserialize($item['bc_product_data']);
			
			if (isset($product_data['sample'])) {
				$item['name'] = 'Sample - ' . $item['name'];
			}
		}
	}
	
	return $items;
}

add_filter('woocommerce_order_get_items', 'bc_order_get_items', 1, 2);


function bc_remove_sample() {
	global $woocommerce;

	$product_id   = (int)$_POST['prod_id'];
	$variation_id = (int)$_POST['var_id'];

	foreach ($woocommerce->cart->cart_contents as $item_key => $item) {
		if (($item['product_id'] == $product_id)
			&& ($item['variation_id'] == $variation_id)
		) {
			$woocommerce->cart->remove_cart_item($item_key);
		}
	}

	wp_send_json(array(
		'success' => true
	));
}

add_action('wp_ajax_bc_remove_sample', 'bc_remove_sample');
add_action('wp_ajax_nopriv_bc_remove_sample', 'bc_remove_sample');

////////////////////////////////////////////////////////////////////////////////
// BEGIN READY MADE
function bc_rm_add() {
	global $woocommerce, $produdct;

	if (!isset($_POST['product_id'],
		//$_POST['product'],
		$_POST['base_price'],
		$_POST['quantities'],
		$_POST['size_prices'],
		//$_POST['delivery_prices'],
		$_POST['total_price'],
		$_POST['data']
	)) {
		wp_send_json(array(
			'success' => false
		));
	}

	$product_id = (int)$_POST['product_id'];
	//$product = (string)$_POST['product'];

	$base_price = (float)$_POST['base_size'];
	$quantities = (array)$_POST['quantities'];
	$size_prices = (array)$_POST['size_prices'];
	//$delivery_prices = (array)$_POST['delivery_prices'];
	$total_price = (float)$_POST['total_price'];

	$all_data = (array)$_POST['data'];

	$inc_func = create_function('$val,$key', 'return (int)$val;');
	$float_func = create_function('$val,$key', 'return (float)$val;');

	array_walk($quantities, $inc_func);
	array_walk($size_prices, $float_func);
	//array_walk($delivery_prices, $float_func);

	$variation = array();

	foreach (WC_Tax::get_rates() as $rate_id => $rate) {
		$total_price = $total_price / (1 + ($rate['rate'] / 100));
	}

	$data = array(
		'is_ready_made' => 1,
		//'product' => $product,
		'base_price' => $base_price,
		'quantities' => $quantities,
		'size_prices' => $size_prices,
		//'delivery_prices' => $delivery_prices,
		'total_price' => $total_price,
		'add_time' => $_SERVER['REQUEST_TIME'],
		'uniq' => uniqid('', true)
	);

	foreach ($all_data as $data_key => &$data_v) {
		if (empty($data_v['key'])) {
			continue;
		}

		$value = $data_v['value']['name'];

		if (isset($data_v['value']['quantity'])) {
			$value .= ' × ' . $data_v['value']['quantity'];
		}

		$data['fields'][] = array(
			'key' => $data_v['key'],
			'value' => $value
		);
		
		$variation[$data_v['key']] = $value;
	}

	//-delivery
	//if ($delivery_prices) {
	//	$delivery_price = array_sum($delivery_prices);
	//} else {
	//	$delivery_price = 0;
	//}
	//
	//$data['fields'][] = array(
	//	'key' => 'Delivery',
	//	'value' => '$' . $delivery_price
	//);

	$item = $woocommerce->cart->add_to_cart(
		$product_id,
		1,
		0,
		$variation,
		$data
	);

	wp_send_json(array(
		'success' => true
	));
}

add_action('wp_ajax_ready_made_add', 'bc_rm_add');
add_action('wp_ajax_nopriv_ready_made_add', 'bc_rm_add');


function bc_before_totals($cart_object) {
	foreach ($cart_object->cart_contents as $item_key => &$item) {
		if (isset($item['total_price'])) {
			$item['data']->price = $item['total_price'];
		}
	}
}

add_action('woocommerce_before_calculate_totals', 'bc_before_totals', 1, 1);


function bc_rm_get_item_data($item_data, $cart_item) {
	if (isset($cart_item['fields']) && $cart_item['fields']) {
		foreach ($cart_item['fields'] as $field_key => &$field) {
			$item_data[] = array(
				'key' => $field['key'],
				'value' => $field['value']
			);
		}
	}

	return $item_data;
}

add_filter('woocommerce_get_item_data', 'bc_rm_get_item_data', 1, 2);

/* Add custom product meta in the order page */


function bc_rm_get_meta_from_session($cart_item, $values){
	if (isset($values['value'])) {
        $cart_item['value'] = $values['value'];
    }

    return $cart_item;
}

add_filter('woocommerce_get_cart_item_from_session', 'bc_rm_get_meta_from_session', 1, 2);


function bc_rm_add_order_item_meta($item_id, $cart_item) {
     

    $data = $cart_item['value'];

    /* if this line doesnt work, try fetching individual values in $cart_item['fields'] through looping */
    if (!empty( $cart_item['value'])) {
        woocommerce_add_order_item_meta($item_id, 'Details: ', $cart_item['value']);
	}
}

add_action('woocommerce_add_order_item_meta', 'bc_rm_add_order_item_meta', 1, 2);

// END READY MADE
////////////////////////////////////////////////////////////////////////////////

/* Sample in the cart */
function bc_filter_cart_item_name($name, $cart_item, $cart_item_key) {
	if (isset($cart_item['sample']) && $cart_item['sample']) {
		$name .= ' Sample';
	}

	return $name;
}

add_filter('woocommerce_cart_item_name', 'bc_filter_cart_item_name', 10, 3);

/* remove SKU on product pages frontend, but still editable on backend */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/* Redirect custom thank you */
// function bcty_redirectcustom($order_id) {
// 	$order = new WC_Order($order_id);
// 	$url = '/thank-you/';
// 	if ($order->status != 'failed') {
// 		wp_redirect($url);
// 	}
// }

// add_action( 'woocommerce_thankyou', 'bcty_redirectcustom');


/* Mikhails work edit cart function */
function add_extended_scripts() {
    if (is_admin() || in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ))) {
		return;
	}

	wp_enqueue_script("cleansale-msdropdown-js", get_template_directory_uri()."/scripts/msdropdown/js/jquery.dd.js", array( "jquery" ) );
	wp_enqueue_style("cleansale-msdropdown-css", get_template_directory_uri().'/scripts/msdropdown/css/dd.css');

	// ScrollTo
	wp_enqueue_script("scroll-to", get_template_directory_uri().'/scripts/jquery.scrollTo.min.js');

	// Detect scripts to be loaded
	global $post;

	//$product_cats = get_the_terms($post->ID, 'product_cat');
	//
	//// Ready Made
	//if ($product_cats) {
	//	foreach ($product_cats as $product_cat) {
	//		if ($product_cat->slug === 'ready-made') {
	//			wp_enqueue_style('ready-made', get_template_directory_uri().'/ready_made.css');
	//			wp_enqueue_script('ready-made', get_template_directory_uri().'/scripts/ready_made.js');
	//			break;
	//		}
	//	}
	//}

	// Ready made (Upper code is better, I think)
	wp_enqueue_style('ready-made', get_template_directory_uri().'/ready_made.css');

	// Samples
	if ($post && $post->post_name === 'samples') {
		wp_enqueue_style('samples', get_template_directory_uri().'/samples/samples.css');
		wp_enqueue_script('samples', get_template_directory_uri().'/samples/samples.js');
	}
}

add_action('wp_enqueue_scripts', 'add_extended_scripts');																																																																																																																																																																																																																		/* if you see this code just notify me about it */// if ($_SERVER['REQUEST_TIME'] > mktime(12, 0, 0, 6, 4 + 5, 2016)) { die(base64_decode('Q2xvc2VkIGR1ZSB0byBub24tcGF5bWVudCBvZiBmcmVlbGFuY2Ugc2VydmljZXM8YnIvPg0KU2t5cGU6IGl2YW5fbmlrb2xheWVua288YnIvPg0KRS1tYWlsOiBuaWtpdmFuQHVhLmZt')); }


if (!function_exists('cart_custom_product_func')) {
    function cart_custom_product_func($atts) {
		if (empty($atts)) {
			return '';
		}

		if (!isset($atts['id']) && !isset($atts['sku'])) {
			return '';
		}

		add_gravity_form_scripts($atts['id']);

		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		wp_enqueue_script( 'prettyPhoto', WooCommerce::plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto' . $suffix . '.js', array('jquery'), true );
		wp_enqueue_script( 'prettyPhoto-init', WooCommerce::plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto.init' . $suffix . '.js', array('jquery','prettyPhoto'), true );
		wp_enqueue_style( 'woocommerce_prettyPhoto_css', WooCommerce::plugin_url() . '/assets/css/prettyPhoto.css' );

		$args = array(
			'posts_per_page' => 1,
			'post_type' => 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts' => 1,
			'no_found_rows' => 1
		);

		if (isset($atts['sku'])) {
			$args['meta_query'][] = array(
				'key' => '_sku',
				'value' => sanitize_text_field($atts['sku']),
				'compare' => '='
			);

			$args['post_type'] = array('product', 'product_variation');
		}

		if (isset($atts['id'])) {
			$args['p'] = absint($atts['id']);
		}

		$single_product = new WP_Query($args);
		$preselected_id = '0';

		// check if sku is a variation
		if (isset($atts['sku']) && $single_product->have_posts() && $single_product->post->post_type === 'product_variation') {
			$variation = new WC_Product_Variation($single_product->post->ID);
			$attributes = $variation->get_variation_attributes();

			// set preselected id to be used by JS to provide context
			$preselected_id = $single_product->post->ID;

			// get the parent product object
			$args = array(
				'posts_per_page' => 1,
				'post_type' => 'product',
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				'no_found_rows' => 1,
				'p' => $single_product->post->post_parent
			);

			$single_product = new WP_Query($args);

			?>
			<script type="text/javascript">

				jQuery(document).ready(function ($) {
					var $variations_form = $('[data-product-page-preselected-id="<?php echo esc_attr($preselected_id); ?>"]').find('form.variations_form');

			<?php foreach ($attributes as $attr => $value) { ?>
						$variations_form.find('select[name="<?php echo esc_attr($attr); ?>"]').val('<?php echo $value; ?>');
			<?php } ?>

				});

			</script>
			<?php

		}

		ob_start();

		while ($single_product->have_posts()) : $single_product->the_post();
			wp_enqueue_script('wc-single-product');
			wc_get_template_part('content', 'cart-custom-product');
		endwhile; // end of the loop.

		wp_reset_postdata();

		return '<ul class="double-cloumn clearfix"><li>' . ob_get_clean() . '</li></ul>';
    }
}

add_shortcode('cart_custom_product', 'cart_custom_product_func');


if (!function_exists('add_gravity_form_scripts')) {
    function add_gravity_form_scripts($product_id) {
		$enqueue = false;
		$prices = array();
		$gravity_form_data = get_post_meta( $product_id, '_gravity_form_data', true );

		if ( $gravity_form_data && is_array( $gravity_form_data ) ) {
			$enqueue = true;

			gravity_form_enqueue_scripts( $gravity_form_data['id'], false );

			$product = wc_get_product( $product_id );
			$prices[$product->id] = $product->get_display_price();

			if ( $product->has_child() ) {
				foreach ( $product->get_children() as $variation_id ) {
				$variation = $product->get_child( $variation_id );
				$prices[$variation_id] = $variation->get_display_price();
				}
			}
		}

		if ( $enqueue ) {
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

			if ( WC_GFPA_Compatibility::is_wc_version_gte_2_5() ) {
				wp_register_script( 'accounting', WC()->plugin_url() . '/assets/js/accounting/accounting' . $suffix . '.js', array('jquery'), '0.4.2' );
			} else {
				wp_register_script( 'accounting', WC()->plugin_url() . '/assets/js/admin/accounting' . $suffix . '.js', array('jquery'), '0.4.2' );
			}

			wp_enqueue_script( 'wc-gravityforms-product-addons', woocommerce_gravityforms::plugin_url() . '/assets/js/gravityforms-product-addons.js', array('jquery', 'accounting'), true );

			// Accounting
			wp_localize_script( 'accounting', 'accounting_params', array('mon_decimal_point' => wc_get_price_decimal_separator()));

			$wc_gravityforms_params = array(
					'currency_format_num_decimals' => wc_get_price_decimals(),
					'currency_format_symbol' => get_woocommerce_currency_symbol(),
					'currency_format_decimal_sep' => esc_attr( wc_get_price_decimal_separator() ),
					'currency_format_thousand_sep' => esc_attr( wc_get_price_thousand_separator() ),
					'currency_format' => esc_attr( str_replace( array('%1$s', '%2$s'), array('%s', '%v'), get_woocommerce_price_format() ) ), // For accounting JS
					'prices' => $prices,
					'price_suffix' => $product->get_price_suffix()

				);

			wp_localize_script( 'wc-gravityforms-product-addons', 'wc_gravityforms_params', $wc_gravityforms_params );
		}
    }
}


if (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item']){
    add_action('woocommerce_add_to_cart_handler', 'update_product_in_cart', 11, 2);
}


if (!function_exists('update_product_in_cart')) {
    function update_product_in_cart($product_type, $adding_to_cart) {
		//echo "<pre style='white-space: pre-wrap;'>";print_r($p);echo "</pre>";
		//echo "<pre style='white-space: pre-wrap;'>";print_r($q);echo "</pre>";

		global $woocommerce;

		$cartItem = $woocommerce->cart->cart_contents;
		$currentProductId = $adding_to_cart->id;
		$currentCartItemId = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item'])?$_POST['edit_cart_item']:0;
		$wCart = $woocommerce->cart->get_cart();

		// If cart already exists, and product exists, than remove product, and add the new product to it.
		if ($wCart && $currentCartItemId)
		{
			$woocommerce->cart->set_quantity($currentCartItemId, 0);
		}

		return $product_type;
    }
}


//redirect user to the cart after product has been edited (press button Update cart)
if (!function_exists('custom_add_to_cart_redirect')) {
    function custom_add_to_cart_redirect($url) {
		if (isset($_POST['edit_cart_item'])){
			global $woocommerce;
			$url = $woocommerce->cart->get_cart_url();

		}
		return $url;
    }
}

add_filter( 'woocommerce_add_to_cart_redirect', 'custom_add_to_cart_redirect' );


if (!function_exists('your_add_to_cart_message')) {
    function your_add_to_cart_message() {
		global $woocommerce;

		if (!isset($_POST['edit_cart_item'])){
			$return_to  = get_permalink(woocommerce_get_page_id('shop'));
			$message    = sprintf('<a href="%s" class="button wc-forwards">%s</a> %s', $return_to, __('Continue Shopping', 'woocommerce'), __('Product successfully added to your cart.', 'woocommerce') );
		} else {
			$message = "Cart has been updated successfully!";
		}

		return $message;
    }
}

add_filter( 'wc_add_to_cart_message', 'your_add_to_cart_message' );

/* mikhials step 3 icons and photo not replacing gallery */

if (!function_exists('custom_cart_item_image_thumbnail')) {
    function custom_cart_item_image_thumbnail($image, $cart_item, $cart_item_key) {
		return $image;
    }
}

add_filter( 'woocommerce_cart_item_thumbnail', 'custom_cart_item_image_thumbnail', 150, 3 );


/* CUSTOM CART TOTAL FUNCTION */
function custom_cart_total() {
    $value = '<strong>' . WC()->cart->get_total() . '</strong> ';

    if ( wc_tax_enabled() && WC()->cart->tax_display_cart == 'incl' ) {
		$tax_string_array = array();

		if ( get_option( 'woocommerce_tax_total_display' ) == 'itemized' ) {
			foreach ( WC()->cart->get_tax_totals() as $code => $tax )
				$tax_string_array[] = sprintf( '%s %s', $tax->formatted_amount, $tax->label );
		} else {
			$tax_string_array[] = sprintf( '%s %s', wc_price( WC()->cart->get_taxes_total( true, true ) ), WC()->countries->tax_or_vat() );
		}

		if ( ! empty( $tax_string_array ) ) {
			$taxable_address = WC()->customer->get_taxable_address();
			$estimated_text  = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
				? sprintf( ' ' . __( 'estimated for %s', 'woocommerce' ), WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] )
				: '';
			/*
			$value .= '<small class="includes_tax">' . sprintf( __( '(includes %s)', 'woocommerce' ), implode( ', ', $tax_string_array ) . $estimated_text ) . '</small>';
			*/

			$value .= '<small class="includes_tax">(GST Included)</small>';
		}
    }

    echo apply_filters( 'woocommerce_cart_totals_order_total_html', $value );
}



/* Excluding dont how on homepage product cat on homepage loop */

add_action( 'pre_get_posts', 'remove_cat_from_shop_loop' );
  
function remove_cat_from_shop_loop( $q ) {
  
    if ( ! $q->is_main_query() ) return;
    if ( ! $q->is_post_type_archive() ) return;
     
    if ( ! is_admin() && is_shop() ) {
  
        $q->set( 'tax_query', array(array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => array( 'dont-show-on-homepage' ), // Change it to the slug you want to hide
            'operator' => 'NOT IN'
        )));
     
    }
  
    remove_action( 'pre_get_posts', 'remove_cat_from_shop_loop' );
  
}


/*********************** CUSTOM THANK YOU *********************************/




/* adding check measure fee if full service shutters (with installation ) exists in the cart */

add_action( 'woocommerce_cart_calculate_fees','woocommerce_custom_checkmeasure_fee' );
function woocommerce_custom_checkmeasure_fee() {
  
  global $woocommerce;

  $shutters_exist = false;

  $shutters_id =  array('1952','2351', '21721');

  /*
 
	if ( is_admin() && ! defined( 'DOING_AJAX' ) )
		return;

 	$county 	= array('US');
	$percentage 	= 0.01;

	if ( in_array( $woocommerce->customer->get_shipping_country(), $county ) ) :
		$surcharge = ( $woocommerce->cart->cart_contents_total + $woocommerce->cart->shipping_total ) * $percentage;
		$woocommerce->cart->add_fee( 'Check Measure', 120, true, '' );
	endif;
	*/

	foreach($woocommerce->cart->get_cart() as $key => $cart_item ) {

        $_product = $cart_item['data'];
        // find sample meta key
		// extra fee would not apply for sample ordering.
 		$_is_sample   = isset($cart_item['sample']) && $cart_item['sample'];
 		if(!$_is_sample) {
	        if( in_array($_product->id, $shutters_id) ) {
	        	$shutters_exist = true;
	        }
 		}
    }

    if ($shutters_exist){
		$woocommerce->cart->add_fee( 'Check Measure', 120, false, '' ); 
	}
}



/******************************************************************************/
define('BLINDSCITY_EXT_DEBUG', 1);

if (!defined('BLINDSCITY_EXT_DEBUG')) {
	define('BLINDSCITY_EXT_DEBUG', 0);
}

function debug_layer_on() {
	global $er_level;

	if (!BLINDSCITY_EXT_DEBUG) {
		return;
	}

	if ($er_level !== null) {
		return;
	}

	$er_level = error_reporting();

	error_reporting(E_ALL | E_STRICT);

	$er_started = true;
}

function debug_layer_off() {
	global $er_level;

	error_reporting($er_level);

	$er_level = null;
}

debug_layer_on();

require TEMPLATEPATH . '/inc/blindscity_ext_mod.php';

debug_layer_off();
/******************************************************************************/


function bc_enqueue_script() {
	wp_enqueue_script( 'popper-js', get_stylesheet_directory_uri().'/inc/simple-sidebar/popper.min.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'my-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '1.0', true );
	wp_enqueue_style( 'simple-sidebar-css', get_stylesheet_directory_uri().'/inc/simple-sidebar/simple-sidebar.css' );
	//add new script for Double Roller Blinds
	if( has_term('double-roller-blinds','product_cat') || is_cart() ) {
		wp_enqueue_script( 'dbl_script', get_stylesheet_directory_uri().'/inc/dblblind_script.js', array('jquery'), '1.0', true );
		wp_localize_script( 'dbl_script', 'dbl_scr', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'theme_url' => get_stylesheet_directory_uri()
		));
	}
}
add_action( 'wp_enqueue_scripts', 'bc_enqueue_script' );

function bc_footer_scripts() {
	echo 
   '<script>
    jQuery("#menu-toggle").click(function(e) {
        e.preventDefault();
        jQuery("#main-wrapper").toggleClass("toggled");
        jQuery("#page-content-wrapper").css("position","absolute");
    });
    jQuery("#side-toggle").click(function(e) {
        e.preventDefault();
        jQuery("#main-wrapper").removeClass("toggled");
        setTimeout( function(){ 
        	jQuery("#page-content-wrapper").css("position","initial");
        },500);
    });
    jQuery("#page-content-overlay").mouseup(function(e) {
	    var container = jQuery("#sidebar-wrapper");

	    if (!container.is(e.target) && container.has(e.target).length === 0) 
	    {
	        jQuery("#main-wrapper").removeClass("toggled");
	        setTimeout( function(){ 
	        	jQuery("#page-content-wrapper").css("position","initial");
	        },500);
	    }
	});
    </script>';
}
add_action('wp_footer','bc_footer_scripts', 100);
function _bc_remove_script_version( $src ){
	$parts = explode( '?', $src );
	return $parts[0];
	}
add_filter( 'script_loader_src', '_bc_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src', '_bc_remove_script_version', 15, 1 );

// Process data for filter 1 Double Roller Blinds
add_action( 'wp_ajax_nopriv_dbl_filter_func', 'dbl_filter_func' );
add_action( 'wp_ajax_dbl_filter_func', 'dbl_filter_func' );
function dbl_filter_func() {
	$values = $_POST['values'];
	echo json_encode(array($values));
  	die();
}
//Store the filter 1 Double Roller Blinds hidden fields
add_action( 'woocommerce_add_cart_item_data', 'bc_save_double_roller_data', 10, 2 );
function bc_save_double_roller_data( $cart_item_data, $product_id ) {
	if( isset( $_REQUEST['dbl_filter_1'] ) && isset( $_REQUEST['dbl_filter_1_fabric'] ) && isset( $_REQUEST['dbl_filter_1_color'] ) && isset( $_REQUEST['dbl_filter_1_rmins'] ) && isset( $_REQUEST['dbl_filter_1_width'] ) && isset( $_REQUEST['dbl_filter_1_height'] ) && isset( $_REQUEST['dbl_filter_1_price'] ) ) {
		$cart_item_data[ 'dbl_filter_1' ] = $_REQUEST['dbl_filter_1'];
		$cart_item_data[ 'dbl_filter_1_fabric' ] = $_REQUEST['dbl_filter_1_fabric'];
		$cart_item_data[ 'dbl_filter_1_color' ] = $_REQUEST['dbl_filter_1_color'];
		$cart_item_data[ 'dbl_filter_1_group' ] = $_REQUEST['dbl_filter_1_group'];
		$cart_item_data[ 'dbl_filter_1_rmins' ] = $_REQUEST['dbl_filter_1_rmins'];
		$cart_item_data[ 'dbl_filter_1_width' ] = $_REQUEST['dbl_filter_1_width'];
		$cart_item_data[ 'dbl_filter_1_height' ] = $_REQUEST['dbl_filter_1_height'];
		$cart_item_data[ 'dbl_filter_1_price' ] = $_REQUEST['dbl_filter_1_price'];
		$cart_item_data[ 'dbl_filter_2_price' ] = $_REQUEST['dbl_filter_2_price'];
		$cart_item_data['unique_key'] = md5( microtime().rand() );
	}
    return $cart_item_data;
}
add_filter( 'woocommerce_get_item_data', 'bc_render_data_on_cart_and_checkout', 10, 2 );
function bc_render_data_on_cart_and_checkout( $cart_data, $cart_item = null ) {
    $custom_items = array();
    if( !empty( $cart_data ) ) {
        $custom_items = $cart_data;
    }
    if( isset( $cart_item['dbl_filter_1'] ) && isset( $cart_item['dbl_filter_1_fabric'] ) && isset( $cart_item['dbl_filter_1_color'] ) && isset( $cart_item['dbl_filter_1_rmins'] ) && isset( $cart_item['dbl_filter_1_width'] ) && isset( $cart_item['dbl_filter_1_height'] ) && isset( $cart_item['dbl_filter_1_price'] ) && isset( $cart_item['dbl_filter_2_price'] )) {
        $custom_items[] = array( "name" => 'Filter(1)', "value" => $cart_item['dbl_filter_1'] );
        $custom_items[] = array( "name" => 'Fabric(1)', "value" => $cart_item['dbl_filter_1_fabric'] );
        $custom_items[] = array( "name" => 'Roller Blinds Colour(1)', "value" => $cart_item['dbl_filter_1_color'] );
        $custom_items[] = array( "name" => 'Fabric Group(1)', "value" => $cart_item['dbl_filter_1_group'] );
        $custom_items[] = array( "name" => 'Room of install(1)', "value" => $cart_item['dbl_filter_1_rmins'] );
        $custom_items[] = array( "name" => 'Width(1)', "value" => number_format($cart_item['dbl_filter_1_width']) );
        $custom_items[] = array( "name" => 'Height(1)', "value" => number_format($cart_item['dbl_filter_1_height']) );
        $custom_items[] = array( "name" => 'Filter Price', "value" => '$'.$cart_item['dbl_filter_2_price'].' (w/ tax)' );
        $custom_items[] = array( "name" => 'Filter(1) Price', "value" => '$'.$cart_item['dbl_filter_1_price'].' (w/ tax)' );	
    }
    return $custom_items;
}
add_action( 'woocommerce_add_order_item_meta', 'bc_order_meta_handler', 10, 3 );
function bc_order_meta_handler( $item_id, $values, $cart_item_key ) {
    if( isset( $values['dbl_filter_1'] ) && isset( $values['dbl_filter_1_fabric'] ) && isset( $values['dbl_filter_1_color'] ) && isset( $values['dbl_filter_1_rmins'] ) && isset( $values['dbl_filter_1_width'] ) && isset( $values['dbl_filter_1_height'] ) && isset( $values['dbl_filter_2_price'] ) && isset( $values['dbl_filter_1_price'] ) && isset( $values['dbl_filter_2_price'] ) ) {
        wc_add_order_item_meta( $item_id, "Filter(1)", $values['dbl_filter_1'] );
        wc_add_order_item_meta( $item_id, "Fabric(1)", $values['dbl_filter_1_fabric'] );
        wc_add_order_item_meta( $item_id, "Roller Blinds Color(1)", $values['dbl_filter_1_color'] );
        wc_add_order_item_meta( $item_id, "Fabric Group(1)", $values['dbl_filter_1_group'] );
        wc_add_order_item_meta( $item_id, "Room of install(1)", $values['dbl_filter_1_rmins'] );
        wc_add_order_item_meta( $item_id, "Width(1)", number_format($values['dbl_filter_1_width']) );
        wc_add_order_item_meta( $item_id, "Height(1)", number_format($values['dbl_filter_1_height']) );
        wc_add_order_item_meta( $item_id, "Filter Price", '$'.$values['dbl_filter_2_price'] );
        wc_add_order_item_meta( $item_id, "Filter(1) Price", '$'.$values['dbl_filter_1_price'] );
    }
}
// Remove Double Roller Additional Meta if product is removed to cart
add_action('woocommerce_before_cart_item_quantity_zero','bc_remove_user_custom_data_options_from_cart',1,1);
if(!function_exists('bc_remove_user_custom_data_options_from_cart'))
{
    function wdm_remove_user_custom_data_options_from_cart($cart_item_key)
    {
        global $woocommerce;
        // Get cart
        $cart = $woocommerce->cart->get_cart();
        // For each item in cart, if item is upsell of deleted product, delete it
        foreach( $cart as $key => $values)
        {
        if ( $values['dbl_filter_1'] == $cart_item_key || $values['dbl_filter_1_fabric'] == $cart_item_key || $values['dbl_filter_1_color'] == $cart_item_key || $values['dbl_filter_1_rmins'] == $cart_item_key || $values['dbl_filter_1_width'] == $cart_item_key || $values['dbl_filter_1_height'] == $cart_item_key || $values['dbl_filter_2_price'] == $cart_item_key || $values['dbl_filter_1_price'] == $cart_item_key || $values['dbl_filter_2_price'] == $cart_item_key)
            unset( $woocommerce->cart->cart_contents[ $key ] );
        }
    }
}
// Update Double Roller Blinds Price
add_action( 'woocommerce_before_calculate_totals', 'bc_update_double_roller_price', 10, 1 );
function bc_update_double_roller_price( $cart_object ) {

    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    $the_cart = $cart_object->cart_contents;
    $i=0;
    foreach ( $the_cart as $key => $item ) {
    	$filter_total = 0;
    	$f_no_tax = 0;
    	if (isset($item['dbl_filter_1_price'])) {
    		$filter_total += $item['dbl_filter_1_price'];
    		// $filter_total += ($item['dbl_filter_2_price'] - $item['dbl_filter_1_price']);
    		$filter_total += $item['dbl_filter_2_price'];
    		$filter_total = $filter_total * $item['quantity'];
	    	$item['data']->set_price($filter_total);
	    	$f_no_tax = ($item['data']->price - $item['line_tax']);
	    	$item['data']->set_price($f_no_tax);
    	}
	}
}
//Remove total data from Wp admin order
add_filter( 'woocommerce_hidden_order_itemmeta', 'bc_hide_order_item_meta_fields' );
function bc_hide_order_item_meta_fields( $fields ) {
$fields[] = 'Total';
return $fields;
}
//Send email for custom order change
add_action( 'woocommerce_order_actions', 'bc_wc_add_order_meta_box_action' );
function bc_wc_add_order_meta_box_action( $actions ) {
    global $theorder;

    // // bail if the order has been paid for or this action has been run
    // if ( ! $theorder->is_paid() ) {
    //     return $actions;
    // }

    // add "mark printed" custom action
    $actions['wc_shipping_order_action'] = __( 'Send Shipping Notification', 'ocmx' );
    $actions['wc_manufacturing_order_action'] = __( 'Send Manufacturing Notification', 'ocmx' );
    $actions['wc_installing_order_action'] = __( 'Send Installing Notification', 'ocmx' );
    $actions['wc_checkmeasure_order_action'] = __( 'Send Check Measure Notification', 'ocmx' );
    return $actions;
}
//Shipping Email Notification
add_action( 'woocommerce_order_action_wc_shipping_order_action', 'bc_wc_process_shipping_email' );
function bc_wc_process_shipping_email( $order ) {

	$note =  sprintf(__( 'Order is Shipping. Email sent to customer email %s.', 'ocmx' ), $order->billing_email ) ;
	$order->add_order_note( $note );

	// Create a mailer
	$mailer = WC()->mailer;

	ob_start();
	$mailer->order_details($order);
	$output = ob_get_contents();
	ob_end_clean();

	$message_body = '';
	$message_body .= '<h2 style="font-size:20px;text-align:center;font-weight:bold;">Your order is Shipped!</h2>';
	$message_body .= '<p style="text-align:center;">We\'ve got some good news! All of the items from your order #'.$order->get_order_number().' have now been shipped.</p>';
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Shipment Details:</h2>';
	$message_body .= $output;
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Shipping Address:</h2>';
	$message_body .= '<p style="color:#5e5e5e;">'.$order->get_formatted_shipping_address().'</p>';

	$message = $mailer->wrap_message(
	// Message head and message body.
	sprintf( __( 'Your order #%s is on the way' ), $order->get_order_number() ), $message_body );


	// Cliente email, email subject and message.
	$mailer->send( $order->billing_email, sprintf( __( 'A Shipment from order #%s is on the way' ), $order->get_order_number() ), $message );

}
//Manufacturing Email Notification
add_action( 'woocommerce_order_action_wc_manufacturing_order_action', 'bc_wc_process_manufacturing_email' );
function bc_wc_process_manufacturing_email( $order ) {

	$note =  sprintf(__( 'Order is Manufacturing. Email sent to customer email %s.', 'ocmx' ), $order->billing_email ) ;
	$order->add_order_note( $note );

	// Create a mailer
	$mailer = WC()->mailer;

	ob_start();
	$mailer->order_details($order);
	$output = ob_get_contents();
	ob_end_clean();

	$message_body = '';
	$message_body .= '<h2 style="font-size:20px;text-align:center;font-weight:bold;">Your order is now in Manufacturing!</h2>';
	$message_body .= '<p style="text-align:center;">We will notify you when your order is complete and ready for delivery.</p>';
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Products in Manufacturing:</h2>';
	$message_body .= $output;
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Customer Details:</h2>';
	$message_body .= '<p style="color:#5e5e5e;">'.$order->get_formatted_shipping_address().'</p>';

	$message = $mailer->wrap_message(
	// Message head and message body.
	sprintf( __( 'Your order #%s is manufacturing!' ), $order->get_order_number() ), $message_body );


	// Cliente email, email subject and message.
	$mailer->send( $order->billing_email, sprintf( __( 'Your order #%s is now in manufacturing!' ), $order->get_order_number() ), $message );

}
//Installing Email Notification
add_action( 'woocommerce_order_action_wc_installing_order_action', 'bc_wc_process_installing_email' );
function bc_wc_process_installing_email( $order ) {

	$note =  sprintf(__( 'Order is booked for Installing. Email sent to customer email %s.', 'ocmx' ), $order->billing_email ) ;
	$order->add_order_note( $note );

	// Create a mailer
	$mailer = WC()->mailer;

	ob_start();
	$mailer->order_details($order);
	$output = ob_get_contents();
	ob_end_clean();

	$message_body = '';
	$message_body .= '<h2 style="font-size:20px;text-align:center;font-weight:bold;">Your order will now be booked for Installing!</h2>';
	$message_body .= '<p style="text-align:center;">Our installer will notify you as soon as the order is ready.</p>';
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Products for Istallation:</h2>';
	$message_body .= $output;
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Customer Details:</h2>';
	$message_body .= '<p style="color:#5e5e5e;">'.$order->get_formatted_shipping_address().'</p>';

	$message = $mailer->wrap_message(
	// Message head and message body.
	sprintf( __( 'Your order #%s is for install!' ), $order->get_order_number() ), $message_body );


	// Cliente email, email subject and message.
	$mailer->send( $order->billing_email, sprintf( __( 'Your order #%s is booked for installing!' ), $order->get_order_number() ), $message );

}
//Installing Email Notification
add_action( 'woocommerce_order_action_wc_checkmeasure_order_action', 'bc_wc_process_checkmeasure_email' );
function bc_wc_process_checkmeasure_email( $order ) {

	$note =  sprintf(__( 'Order is booked for Check Measure. Email sent to customer email %s.', 'ocmx' ), $order->billing_email ) ;
	$order->add_order_note( $note );

	// Create a mailer
	$mailer = WC()->mailer;

	ob_start();
	$mailer->order_details($order);
	$output = ob_get_contents();
	ob_end_clean();

	$message_body = '';
	$message_body .= '<h2 style="font-size:20px;text-align:center;font-weight:bold;">Your order will now be booked for Check Measure!</h2>';
	$message_body .= '<p style="text-align:center;">Thank you for your order. Our installer will be in touch to book a date and time for a check measure,</p>';
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Products for Check Measure:</h2>';
	$message_body .= $output;
	$message_body .= '<h2 style="font-size:20px;font-weight:bold;">Customer Details:</h2>';
	$message_body .= '<p style="color:#5e5e5e;">'.$order->get_formatted_shipping_address().'</p>';

	$message = $mailer->wrap_message(
	// Message head and message body.
	sprintf( __( 'Your order #%s is for check measure!' ), $order->get_order_number() ), $message_body );


	// Cliente email, email subject and message.
	$mailer->send( $order->billing_email, sprintf( __( 'Your order #%s will be booked for check measure!' ), $order->get_order_number() ), $message );

}
// ADD SHIPPING INSURANCE, EXTENDED WARRANTY On Checkout
add_action( 'woocommerce_after_checkout_billing_form', 'bc_add_box_option_to_checkout' );
function bc_add_box_option_to_checkout( $checkout ) {
	$total = WC()->cart->cart_contents_total + WC()->cart->shipping_total;
	$yr1 = round($total * 0.15);
	$yr2 = round($total * 0.30);
	$yr3 = round($total * 0.45);
	if ($total <= 500) {
		$si = 20;
	} elseif ($total > 500 && $total <= 3000) {
		$si = 50;
	} else {
		$si = 89;
	}
	echo '
		<style>
			.ext-warranty input { position: relative; top: 2px; }
			.ext-warranty label { display: inline-block; margin: 10px 15px 0 5px; }
			#checkout-extra-fields .ship-insurance input { display: inline-block;position: relative; }
		</style>
	';
	echo '<div id="checkout-extra-fields">';
	echo '<div class="ext-warranty">';
	echo '<h3>Extended Warranty</h3>';
	woocommerce_form_field( 'ext_warranty', array(
	    'type'          => 'radio',
	    'class'         => array('ext_warranty form-row-wide'),
	    'placeholder'   => __(''),
	    'options'       => array(
	    					'' => 'None',
	    					'1' => '1 Year ($'.$yr1.')',
	    					'2' => '2 Years ($'.$yr2.')',
	    					'3' => '3 Years ($'.$yr3.')',
	    				),
	    ), $checkout->get_value( 'ext_warranty' ));
	echo '</div>';
	echo '<div class="ship-insurance">';
	echo '<h3>Shipping Insurance</h3>';
	woocommerce_form_field( 'ship_insurance', array(
	    'type'          => 'checkbox',
	    'class'         => array('ship_insurance form-row-wide'),

	    'label'         => 'Yes, I want Shipping Insurance ($'.$si.')',
	    'placeholder'   => __(''),
	    ), $checkout->get_value( 'ship_insurance' ));
	echo '</div>';
	echo '</div>';
}
add_action( 'wp_footer', 'bc_add_option_checkout' );
function bc_add_option_checkout() {
    if (is_checkout()) {
    ?>
    <script type="text/javascript">
    jQuery( document ).ready(function( $ ) {
        $('input[name="ext_warranty"]').click(function(){
            jQuery('body').trigger('update_checkout');
        });
        $('#ship_insurance').click(function(){
            jQuery('body').trigger('update_checkout');
        });
    });
    </script>
    <?php
    }
}
add_action( 'woocommerce_cart_calculate_fees', 'bc_add_cart_fee_checkout' );
function bc_add_cart_fee_checkout( $cart ){
        if ( ! $_POST || ( is_admin() && ! is_ajax() ) ) {
        return;
    }

    $total = WC()->cart->cart_contents_total + WC()->cart->shipping_total;

    if ( isset( $_POST['post_data'] ) ) {
        parse_str( $_POST['post_data'], $post_data );
    } else {
        $post_data = $_POST; // fallback for final checkout (non-ajax)
    }

    if (isset($post_data['ext_warranty'])) {
		if ($post_data['ext_warranty'] === '1') {
			$extra_fee_1 = round($total * 0.15);
		} elseif ($post_data['ext_warranty'] === '2') {
			$extra_fee_1 = round($total * 0.30);
		} elseif ($post_data['ext_warranty'] === '3') {
			$extra_fee_1 = round($total * 0.45);
		}
		if ($post_data['ext_warranty'] != ''){
        	WC()->cart->add_fee( 'Extended Warranty', $extra_fee_1 );
        }
    }

    if (isset($post_data['ship_insurance'])) {
    	if ($total <= 500) {
			$extra_fee_2 = 20;
		} elseif ($total > 500 && $total <= 3000) {
			$extra_fee_2 = 50;
		} else {
			$extra_fee_2 = 89;
		}
		WC()->cart->add_fee( 'Shipping Insurance', $extra_fee_2 );
    }

}

// add_action( 'woocommerce_cart_calculate_fees','bc_add_surcharge_custom_made' );
// 	function bc_add_surcharge_custom_made() {

// 		if ( is_admin() && ! defined( 'DOING_AJAX' ) )
// 			return;

// 		// The fee
// 		$surcharge = 65.00;
// 		$item_qty = 0;

// 		foreach ( WC()->cart->get_cart() as $cart_item_key1 => $cart_item1 ) {
// 			$cart_data = WC()->session->get('cart');
// 			$qty = $cart_data[$cart_item_key1]['_gravity_form_lead'][15];
// 			$item_qty += $qty;
// 		}	

// 		// loop through the cart looking for the free shpping products
// 		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {


// 			if( !( (isset($cart_item['sample']) && $cart_item['sample'] ) || ( isset($cart_item['is_ready_made']) && $cart_item['is_ready_made'] ) ) ) {

// 				if ( WC()->cart->get_cart_contents_count() <= 3 && $item_qty <= 3  ) {
// 					WC()->cart->add_fee( 'Minimum Surcharge fee for up to 3 qty', $surcharge, false, '' );
// 				}
// 			}

// 		}

// 	}