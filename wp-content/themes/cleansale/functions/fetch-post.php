<style type="text/css">
	.blog-main-post-container li.post img {
		margin: 0px !important;
		padding: 0px !important;
	}
</style>	
<?php 
$link = get_permalink($post->ID);
$args  = array('postid' => $post->ID, 'width' => 980, 'hide_href' => false, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => 'large', 'exclude_video' => false);
$image = get_obox_media($args);
?>
<li id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

	<h2 class="post-title" style="text-align: center;">
		<a href="<?php echo $link; ?>"><?php the_title(); ?></a>
	</h2>
	<div class="post-title-block" style="display: none;"> 
		<!--Show the Title -->
		<h1 class="<?php if(!is_page()) : ?>post-title<?php else : ?>page-title<?php endif; ?>"><?php the_title(); ?></h1>
		<?php if( !is_page()) : ?>  
			<h5 class="date">
				<?php if( get_option("ocmx_meta_date") != "false" ) {echo the_time(get_option('date_format'));} // Hide the date unless enabled in Theme Options ?>
				<?php if( get_option( "ocmx_meta_author" ) != "false" ) {_e(" written by ", "ocmx"); ?> <?php the_author_posts_link();} //Hide the author unless enabled in Theme Options ?>
				<?php if(get_option( "ocmx_meta_category" ) !="false" ) { _e(" in ",'ocmx'); ?> <?php the_category(", ",'ocmx');} ?> 
			</h5>
		<?php endif; ?>
	</div>
	
	<?php if(is_single() && $image != "") : // Show the Featured Image if this is a post ?> 
		<div class="post-image fitvid"> 
			<?php echo $image; ?>
		</div>
	<?php endif; ?>
	<!--Get the Content -->
	<div class="copy clearfix">
		<?php the_content(); ?>
	</div>
	
	<?php if( is_single() ) : ?>
		<div class="post-meta clearfix">
			<!--Show tags if enabled -->
			<?php if( get_option("ocmx_meta_tags") != "false" ) : // Show tags if enabled in Theme Options ?>
				<ul class="tags">
					<?php the_tags('<li>','</li><li>','</li>'); ?>
				</ul>
			<?php endif; ?>
			<!--Show Social Buttons if enabled --> 
			<?php if( get_option("ocmx_meta_social") != "false" && !is_page() ) : ?>
				<ul class="social">
					<li class="addthis">
						<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style ">
							<a class="addthis_button_facebook_like"></a>
							<a class="addthis_button_tweet"></a>
							<a class="addthis_button_google_plusone" g:plusone:size="medium"></a> 
							<a class="addthis_counter addthis_pill_style"></a>
						</div>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-507462e4620a0fff"></script>
						<!-- AddThis Button END -->
					</li>
				</ul>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</li>                        