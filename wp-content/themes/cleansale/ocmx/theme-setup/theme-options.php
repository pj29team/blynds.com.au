<?php function ocmx_theme_options(){
	global $obox_meta, $theme_options, $themeid;
	if(!isset($theme_options))
		$theme_options = array();
	$theme_options["general_site_options"] =
			array(
				array("label" => "Custom Logo", "description" => "Full URL or folder path to your custom logo.", "name" => "ocmx_custom_logo", "default" => "", "id" => "upload_button", "input_type" => "file", "args" => array("width" => 90, "height" => 75)),
				array("label" => "Favicon", "description" => "Select a favicon for your site", "name" => "ocmx_custom_favicon", "default" => "", "id" => "upload_button_favicon", "input_type" => "file", "sub_title" => "favicon", "args" => array("width" => 16, "height" => 16)),	
				array("label" => "Custom Login Logo", "description" => "Select a custom login logo, recommended dimensions (326px x 82px)", "name" => "ocmx_custom_login", "default" => "", "id" => "upload_button_login", "input_type" => "file", "sub_title" => "login logo", "args" => array("width" => 326, "height" => 82)),
				array(
					"main_section" => "Facebook Sharing Options",
					"main_description" => "Set a default image URL to appear on Facebook shares if no featured image is found. Recommended size 200x200.",
					"sub_elements" =>
						array(
							array("label" => "Disable OpenGraph?", "description" => "Select No if you want to disable the theme's OpenGraph support(do this only if using a conflicting plugin)", "name" => "ocmx_open_graph", "default" => "no", "id" => "ocmx_open_graph", "input_type" => 'select', 'options' => array('Yes' => 'yes', 'No' => 'no')
							),

							array("label" => "Image URL", "description" => "", "name" => "ocmx_site_thumbnail", "sub_title" => "Open Graph image", "default" => "", "id" => "upload_button_ocmx_site_thumbnail", "input_type" => "file", "args" => array("width" => 80, "height" => 80)
							)
						)
				),
				array("label" => "Product Images", "description" => "Choose to show a slider or gallery on the product page.", "name" => "ocmx_product_image", "default" => "Gallery", "id" => "ocmx_product_image", "input_type" => 'select', 'options' => array('Gallery' => 'gallery', 'Slider' => 'slider')),   
				array(
					"main_section" => "Custom Styling",
					"main_description" => "Set your own custom font and CSS for testimonials any any other element you wish to restyle.",
					"sub_elements" => 
						array(
				  
							array("label" => "Custom CSS", "description" => "Enter changed classes from the theme stylesheet, or custom CSS here.", "name" => "ocmx_custom_css", "default" => "", "id" => "ocmx_custom_css", "input_type" => "memo"),
				             )
				    ),
				array(
						"main_section" => "Post Meta",
						"main_description" => "These settings control which post meta is displayed in posts,pages and some widgets.",
						"sub_elements" => 
							array(
								array("label" => "Date", "description" => "Uncheck to hide the date. ","name" => "ocmx_meta_date", "", "default" => "true", "id" => "ocmx_meta_date", "input_type" => "checkbox"),
								array("label" => "Author Link", "description" => "Uncheck to hide the Author on posts.", "name" => "ocmx_meta_author", "default" => "true", "id" => "ocmx_meta_author", "input_type" => "checkbox"),
								array("label" => "Tags", "description" => "Check to show tags on single posts", "name" => "ocmx_meta_tags", "default" => "false", "id" => "ocmx_meta_tags", "input_type" => "checkbox"),
								array("label" => "Comment Link", "description" => "Uncheck to hide the comment link on archives.", "name" => "ocmx_meta_comments", "default" => "true", "id" => "ocmx_meta_comments", "input_type" => "checkbox"),
								array("label" => "Category Link", "description" => "Uncheck to hide the comment link on archives.", "name" => "ocmx_meta_category", "default" => "true", "id" => "ocmx_meta_category", "input_type" => "checkbox"),
								array("label" => "Social Sharing", "description" => "Uncheck to hide the Sharing buttons on posts and products.", "name" => "ocmx_meta_social", "default" => "true", "id" => "ocmx_meta_social", "input_type" => "checkbox"),
							)
						),
				array("label" => "Custom RSS URL", "description" => "Paste the URL to your custom RSS feed, such as Feedburner.", "name" => "ocmx_rss_url", "default" => "", "id" => "", "input_type" => "text"),	   
				array(
					"main_section" => "Press Trends Analytics",
					"main_description" => "Select Yes Opt out. No personal data is collected.",
					"sub_elements" => 
					array(
						array("label" => "Disable Press Trends?", "description" => "PressTrends helps Obox build better themes and provide awesome support by retrieving aggregated stats. PressTrends also provides a <a href='http://wordpress.org/extend/plugins/presstrends/' title='PressTrends Plugin for WordPress' target='_blank'>plugin for you</a> that delivers stats on how your site is performing against similar sites like yours. <a href='http://www.presstrends.me' title='PressTrends' target='_blank'>Learn more…</a>","name" => "ocmx_disable_press_trends", "default" => "no", "id" => "ocmx_disable_press_trends", "input_type" => 'select', 'options' => array('Yes' => 'yes', 'No' => 'no'))
		                 )
				     )
			);
	$theme_options["home_options"] = array(
					 array(
						"main_section" => "Header Image",
						"main_description" => "Use these settings to add a call-to-action button and text to your header. To set a header image, click <a href='customize.php'>here</a>, otherwise the collage will be shown by default. Recommended dimensions 1920x380 or at least 980px wide.",
	
						"sub_elements" => 
							array(
								array("label" => "Header Title", "description" => "", "name" => "ocmx_header_title", "default" => "", "id" => "ocmx_header_title", "input_type" => "text"),
								array("label" => "Header Excerpt", "description" => "", "name" => "ocmx_header_excerpt", "default" => "", "id" => "ocmx_header_excerpt", "input_type" => "memo")
							)
						)
					
					);
					
	$theme_options["footer_options"] = array(
					array("label" => "Custom Footer Text", "description" => "", "name" => "ocmx_".$themeid."_footer", "default" => "Copyright ".date("Y")." CleanSale was created in WordPress by Obox Themes."	, "id" => "ocmx_cleansale_footer", "input_type" => "memo"),
					array("label" => "Hide Obox Logo", "description" => "Hide the Obox Logo from the footer.", "name" => "ocmx_logo_hide", "default" => "false", "id" => "ocmx_logo_hide", "input_type" => "checkbox"),
					array("label" => "Site Analytics", "description" => "Enter in the Google Analytics Script here.","name" => "ocmx_googleAnalytics", "default" => "", "id" => "","input_type" => "memo")
	);
	
	$theme_options["blindscity_options"] = array(
		array(
			'label' => 'Top Delivery Text',
			'description' => '',
			'name' => 'ocmx_top_delivery_text',
			'default' => 'Free delivery on orders $100+',
			'id' => 'ocmx_top_delivery_text',
			'input_type' => 'text'
		),
		array(
			'label' => 'Green Delivery Text',
			'description' => '',
			'name' => 'ocmx_green_delivery_text',
			'default' => 'Free delivery Australia on all orders above $100',
			'id' => 'ocmx_green_delivery_text',
			'input_type' => 'text'
		),
		array(
			'label' => 'Subscribe Form Text',
			'description' => '',
			'name' => 'ocmx_subscribe_form_text',
			'default' => 'Join BlindsCity.com.au to get exclusive deals and save up to 80% on custom made blinds email newsletter.',
			'id' => 'ocmx_subscribe_form_text',
			'input_type' => 'text'
		),
		array(
			'main_section' => 'Signup Form',
			'main_description' => '',
			'sub_elements' =>
				array(
					array(
						"label" => 'Enable Form',
						'description' => '',
						'name' => 'ocmx_signup_form',
						'default' => 'yes',
						'id' => 'ocmx_signup_form',
						'input_type' => 'select',
						'options' => array(
							'Yes' => 'yes',
							'No' => 'no'
						)
					),
					array(
						'label' => 'Form Image',
						'description' => 'Full URL to image for Signup Form',
						'name' => 'ocmx_signup_form_image',
						'default' => get_template_directory_uri() . '/images/new/signup_bg.jpg',
						'id' => 'upload_button_signup_form_image',
						'input_type' => 'file',
						'args' => array('width' => 930, 'height' => 239)
					),
					array(
						'label' => 'Form Title',
						'description' => '',
						'name' => 'ocmx_signup_form_title',
						'default' => 'Sign up to Receive',
						'id' => 'ocmx_signup_form_title',
						'input_type' => 'text'
					),
					array(
						'label' => 'Form Subtitle',
						'description' => '',
						'name' => 'ocmx_signup_form_subtitle',
						'default' => '20% OFF on Your order',
						'id' => 'ocmx_signup_form_subtitle',
						'input_type' => 'text'
					)
				)
		)
	);
	
	$theme_options["small_ad_options"] = array(
						array(
								"label" => "Number of Small Ads", 
								"description" => "When using the select box, you must click \"Save Changes\" before the blocks are added or removed.", 
								"name" => "ocmx_small_ads", 
								"id" =>  "ocmx_small_ads",
								"prefix" => "ocmx_small_ad",
								"default" => "0", 
								"input_type" => "select", 
								"options" => array("None" => "0", "1" => "1", "2" => "2", "3" => "3", "4" => "4", "5" => "5", "6" => "6", "7" => "7", "8" => "8", "9" => "9", "10" => "10"), 
								"args" => array("width" => 125, "height" => "125")
							)
					  );

	$theme_options["medium_ad_options"] = array( 
						array(
								"label" => "Number of Medium Ads", 
								"description" => "", 
								"name" => "ocmx_medium_ads", 
								"id" =>  "ocmx_medium_ads",
								"prefix" => "ocmx_medium_ad", 
								"default" => "0", 
								"input_type" => "select", 
								"options" => array("None" => "0", "1" => "1", "2" => "2", "3" => "3", "4" => "4", "5" => "5", "6" => "6", "7" => "7", "8" => "8", "9" => "9", "10" => "10"), 
								"args" => array("width" => 300, "height" => "250")
							)
						);
						
	$theme_options["home_page_options"] = array(
		array(
			  	"label" => "Home Page Layout",
				"description" => "Select whether you'd like to have a Blog home page with a sidebar, or use the business layout.",
				"name" => "ocmx_home_page_layout", "default" => "fullwidth",
				"id" => "ocmx_home_page_layout",
				"input_type" => "hidden",
				"default" => "fullwidth",
				"options" => 
					array(
							"blog" => array("label" => "Blog Layout", "description" => "Perfect if you just want your site to be a blog.", "image" => get_bloginfo('template_directory')."/ocmx/images/blog-layout.png"),
						  	"fullwidth" => array("label" => "Full Width Layout", "description" => "Display your photos in a full width grid on the homepage", "image" => get_bloginfo('template_directory')."/ocmx/images/fullwidth-layout.png"),
							"widget" => array("label" => "Widget Driven (Advanced)", "description" => "You determine how to layout your page via widgets.", "image" => get_bloginfo('template_directory')."/ocmx/images/widgetized-layout.png"),
						)
				)
	);
	
	$theme_options["layout_options"] = array(
		array(
				"label" => "Sidebar Layout",
				"description" => "Choose which side you would like your site sidebar to display on posts and pages. Alternatively hide it completely on all pages.",
				"name" => "ocmx_sidebar_layout", "default" => "sidebarright",
				"id" => "ocmx_sidebar_layout",
				"input_type" => "hidden",
				"default" => "sidebarright",
				"options" =>
					array(
							"sidebarright" => array("label" => "Sidebar Right", "description" => ""),
							"sidebarleft" => array("label" => "Sidebar Left", "description" => ""),
							"sidebarnone" => array("label" => "No Sidebar", "description" => "")
						)
				),
		array(
			"label" => "Shop Sidebar Layout",
			"description" => "Choose which side you would like your shop sidebar to display on posts and pages. Alternatively hide it completely on all shoppages.",
			"name" => "ocmx_shop_sidebar_layout", "default" => "shopsidebarright",
			"id" => "ocmx_shop_sidebar_layout",
			"input_type" => "hidden",
			"default" => "sidebarright",
			"options" =>
				array(
					"shopsidebarright" => array("label" => "Sidebar Right", "description" => ""),
					"shopsidebarleft" => array("label" => "Sidebar Left", "description" => ""),
					"shopsidebarnone" => array("label" => "No Sidebar", "description" => "")
				)
		)
	);
	
	
	
}
add_action("init", "ocmx_theme_options"); 
	
/***************************************************************************/
/* Setup Defaults for this theme for optiosn which aren't set in this page */
if(is_admin() && !get_option(isset($themeid)."-defaults")) :
	update_option("ocmx_general_font_style_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	update_option("ocmx_navigation_font_style_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	update_option("ocmx_sub_navigation_font_style_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	update_option("ocmx_post_font_titles_style_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	update_option("ocmx_post_font_meta_style_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	update_option("ocmx_post_font_copy_font_style_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	update_option("ocmx_widget_font_titles_font_style_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	update_option("ocmx_widget_footer_titles_font_size_default", "'proxima-nova', 'Proxima Nova', 'Helvetica Neue'");
	
	
	update_option("ocmx_general_font_color_default", "#333");
	update_option("ocmx_navigation_font_color_default", "#777");
	update_option("ocmx_sub_navigation_font_color_default", "#333");
	update_option("ocmx_post_titles_font_color_default", "#333");
	update_option("ocmx_post_meta_font_color_default", "#999");
	update_option("ocmx_post_copy_font_color_default", "#333");
	update_option("ocmx_widget_titles_font_color_default", "#999");
	update_option("ocmx_widget_footer_titles_font_color_default", "#999");
	
	update_option("ocmx_general_font_size_default", "17");
	update_option("ocmx_navigation_font_size_default", "12");
	update_option("ocmx_sub_navigation_font_size_default", "12");
	update_option("ocmx_post_titles_font_size_default", "10");
	update_option("ocmx_post_meta_font_size_default", "13");
	update_option("ocmx_post_copy_font_size_default", "17");
	update_option("ocmx_widget_titles_font_size_default", "15");
	update_option("ocmx_widget_footer_titles_font_size_default", "15");
	update_option($themeid."-defaults", 1);
endif;
update_option("allow_gallery_effect", "1");

add_action("switch_theme", "remove_ocmx_gallery_effects"); 
function remove_ocmx_gallery_effects(){delete_option("allow_gallery_effect");}; ?>