<?php //OCMX Custom logo and Favicon

function ocmx_logo_register($wp_customize){
    
    $wp_customize->add_section('ocmx_general', array(
        'title'    => __('General Theme Settings', 'ocmx'),
        'priority' => 30,
    ));
    
   //Custom Colors
	
	$wp_customize->add_setting('ocmx_ignore_colours', array(
        'default'        => 'no',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('header_color_scheme', array(
        'label'      => __('Use Theme Default Color Scheme', 'ocmx'),
        'section'    => 'ocmx_general',
        'settings'   => 'ocmx_ignore_colours',
        'type'       => 'radio',
        'priority' => 0,
        'choices'    => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
    ));
 
    $wp_customize->add_setting('ocmx_custom_logo', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'ocmx_custom_logo', array(
        'label'    => __('Custom Logo', 'ocmx'),
        'section'  => 'ocmx_general',
        'settings' => 'ocmx_custom_logo',
    )));
    
    $wp_customize->add_setting('ocmx_custom_favicon', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'ocmx_custom_favicon', array(
        'label'    => __('Custom Favicon', 'ocmx'),
        'section'  => 'ocmx_general',
        'settings' => 'ocmx_custom_favicon',
    )));
    
}

add_action('customize_register', 'ocmx_logo_register');

// OCMX Color Options 

function ocmx_customize_register($wp_customize) {


	// Header Color Scheme
	$wp_customize->add_section('header_color_scheme', array(
		'title' => __( 'Header Color Scheme', 'ocmx' ),
		'priority' => 35,
		)
	);
	
	$wp_customize->add_setting( 'ocmx_logo_font_color', array(
		'default' => '#000000',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_logo_font_color', array(
		'label' => __( 'Logo Color (text only)', 'ocmx' ),
		'section' => 'header_color_scheme',
		'settings' => 'ocmx_logo_font_color',
		'priority' => 1,
	)));
    
    $wp_customize->add_setting( 'ocmx_navigation_font_color', array(
		'default' => '#595959',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_navigation_font_color', array(
		'label' => __( 'Navigation Links', 'ocmx' ),
		'section' => 'header_color_scheme',
		'settings' => 'ocmx_navigation_font_color',
		'priority' => 1,
	)));
	
	$wp_customize->add_setting( 'ocmx_navigation_hover', array(
		'default' => '#000',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_navigation_hover', array(
		'label' => __( 'Navigation Hover', 'ocmx' ),
		'section' => 'header_color_scheme',
		'settings' => 'ocmx_navigation_hover',
		'priority' => 2,
	)));
	
	
	// Content Color Scheme
	$wp_customize->add_section('content_color_scheme', array(
		'title' => __( 'Content Color Scheme', 'ocmx' ),
		'priority' => 36
		)
	);
	
	$wp_customize->add_setting( 'ocmx_slider_container', array(
		'default' => '#fff',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_slider_container', array(
		'label' => __( 'Slider Container', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_slider_container',
		'priority' => 1,
	)));
	
	$wp_customize->add_setting( 'ocmx_slider_arrows', array(
		'default' => '#fff',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_slider_arrows', array(
		'label' => __( 'Slider Arrows', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_slider_arrows',
		'priority' => 2,
	)));
	
	$wp_customize->add_setting( 'ocmx_slider_arrows_hover', array(
		'default' => '#f93942',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_slider_arrows_hover', array(
		'label' => __( 'Slider Arrows Hover', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_slider_arrows_hover',
		'priority' => 3,
	)));
	
	$wp_customize->add_setting( 'ocmx_post_container', array(
		'default' => '#fff',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_post_container', array(
		'label' => __( 'Content Container', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_post_container',
		'priority' => 4,
	)));
		
	$wp_customize->add_setting( 'ocmx_post_titles_font_color', array(
		'default' => '#000',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_post_titles_font_color', array(
		'label' => __( 'Post Titles Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_post_titles_font_color',
		'priority' => 5,
	)));
	
	$wp_customize->add_setting( 'ocmx_post_titles_hover', array(
		'default' => '#595959',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_post_titles_hover', array(
		'label' => __( 'Post Titles Hover Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_post_titles_hover',
		'priority' => 6,
	)));
    
    $wp_customize->add_setting( 'ocmx_widget_titles_font_color', array(
		'default' => '#000',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_widget_titles_font_color', array(
		'label' => __( 'Section Titles', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_widget_titles_font_color',
		'priority' => 7,
	)));
	
	$wp_customize->add_setting( 'ocmx_date_meta', array(
		'default' => '#C8C8C8',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_date_meta', array(
		'label' => __( 'Date Text', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_date_meta',
		'priority' => 8,
	)));
	
	$wp_customize->add_setting( 'ocmx_price_meta', array(
		'default' => '#F93942',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_price_meta', array(
		'label' => __( 'Price Text', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_price_meta',
		'priority' => 9,
	)));
	
	$wp_customize->add_setting( 'ocmx_general_font_color', array(
		'default' => '#595959',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_general_font_color', array(
		'label' => __( 'General Body Text Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_general_font_color',
		'priority' => 10,
	)));
	
	$wp_customize->add_setting( 'ocmx_body_links', array(
		'default' => '#000',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_body_links', array(
		'label' => __( 'General Link Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_body_links',
		'priority' => 15,
	)));
	
	$wp_customize->add_setting( 'ocmx_body_links_hover', array(
		'default' => '#000',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_body_links_hover', array(
		'label' => __( 'General Link Color Hover', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_body_links_hover',
		'priority' => 20,
	)));
	
	$wp_customize->add_setting( 'ocmx_border_color', array(
		'default' => '#EEE',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_border_color', array(
		'label' => __( 'Border Colors', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_border_color',
		'priority' => 55,
	)));
	
	$wp_customize->add_setting( 'ocmx_buttons', array(
		'default' => '#F2F2F2',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_buttons', array(
		'label' => __( 'Button Background Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_buttons',
		'priority' => 65,
	)));
	
	$wp_customize->add_setting( 'ocmx_buttons_text', array(
		'default' => '#595959',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_buttons_text', array(
		'label' => __( 'Button Text Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_buttons_text',
		'priority' => 66,
	)));
	
	$wp_customize->add_setting( 'ocmx_buttons_borders', array(
		'default' => '#e1e1e1',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_buttons_borders', array(
		'label' => __( 'Button Border Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_buttons_borders',
		'priority' => 67,
	)));
	
	$wp_customize->add_setting( 'ocmx_buttons_hover', array(
		'default' => '#f93942',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_buttons_hover', array(
		'label' => __( 'Button Hover Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_buttons_hover',
		'priority' => 70,
	)));
	
	$wp_customize->add_setting( 'ocmx_buttons_text_hover', array(
		'default' => '#fff',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_buttons_text_hover', array(
		'label' => __( 'Button Text Hover Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_buttons_text_hover',
		'priority' => 71,
	)));
	
	$wp_customize->add_setting( 'ocmx_buttons_borders_hover', array(
		'default' => '#e7242d',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_buttons_borders_hover', array(
		'label' => __( 'Button Borders Hover Color', 'ocmx' ),
		'section' => 'content_color_scheme',
		'settings' => 'ocmx_buttons_borders_hover',
		'priority' => 72,
	)));
	
		
	// Footer Color Scheme
	$wp_customize->add_section('footer_color_scheme', array(
		'title' => __( 'Footer Color Scheme', 'ocmx' ),
		'priority' => 37,
		)
	);
	
	$wp_customize->add_setting( 'ocmx_footer_container', array(
		'default' => '#f1f1f1',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_footer_container', array(
		'label' => __( 'Footer Container', 'ocmx' ),
		'section' => 'footer_color_scheme',
		'settings' => 'ocmx_footer_container',
		'priority' => 30,
	)));
	
	$wp_customize->add_setting( 'ocmx_widget_footer_titles_font_color', array(
		'default' => '#000',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_widget_footer_titles_font_color', array(
		'label' => __( 'Footer Widget Titles', 'ocmx' ),
		'section' => 'footer_color_scheme',
		'settings' => 'ocmx_widget_footer_titles_font_color',
		'priority' => 36,
	)));
	
	$wp_customize->add_setting( 'ocmx_footer_text', array(
		'default' => '#878787',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_footer_text', array(
		'label' => __( 'Footer Text Color', 'ocmx' ),
		'section' => 'footer_color_scheme',
		'settings' => 'ocmx_footer_text',
		'priority' => 40,
	)));
	
	$wp_customize->add_setting( 'ocmx_footer_links', array(
		'default' => '#595959',
		'type' => 'option',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocmx_footer_links', array(
		'label' => __( 'Footer Link Color', 'ocmx' ),
		'section' => 'footer_color_scheme',
		'settings' => 'ocmx_footer_links',
		'priority' => 45,
	)));
	

	
	wp_reset_query();

//ADD JQUERY

if ( $wp_customize->is_preview() && ! is_admin() )
	add_action( 'wp_footer', 'ocmx_customize_preview', 21);
	
	function ocmx_customize_preview() {
	?>
	<script type="text/javascript">

	( function( $ ){
	
		wp.customize('ocmx_general_font_color',function( value ) {
			value.bind(function(to) {
				jQuery('body').css({'color': to});
			});
		});
	
		wp.customize('ocmx_logo_font_color',function( value ) {
			value.bind(function(to) {
				jQuery('.logo h1 a').css({'color': to});
			});
		});
		
		wp.customize('ocmx_navigation_font_color',function( value ) {
			value.bind(function(to) {
				jQuery('ul#nav li a').css({'color': to});
			});
		});

		wp.customize('ocmx_navigation_hover',function( value ) {
			value.bind(function(to) {
				jQuery('ul#nav li a:hover').css({'color': to});
			});
		});
	
		wp.customize('ocmx_slider_container',function( value ) {
			value.bind(function(to) {
				jQuery('.feature').css({'backgroundColor': to});
			});
		});
		
		wp.customize('ocmx_slider_arrows',function( value ) {
			value.bind(function(to) {
				jQuery('.next, .previous').css({'backgroundColor': to});
			});
		});

		wp.customize('ocmx_post_container',function( value ) {
			value.bind(function(to) {
				jQuery('ul#nav, ul#nav ul.sub-menu li, #content-container, .feature .overlay, .feature .left-column, .feature .right-column li').css({'backgroundColor': to});
			});
		});
		
		wp.customize('ocmx_price_meta',function( value ) {
			value.bind(function(to) {
				jQuery('.price, .amount').css({'color': to});
			});
		});
		
		wp.customize('ocmx_body_links',function( value ) {
			value.bind(function(to) {
				jQuery('.widgettitle a, .widgettitle span, .copy a, .widget .content a').css({'color': to});
			});
		});
		
		wp.customize('ocmx_body_links_hover',function( value ) {
			value.bind(function(to) {
				jQuery('.copy a:hover, .widget .content a:hover').css({'color': to});
			});
		});
		
		wp.customize('ocmx_widget_titles_font_color',function( value ) {
			value.bind(function(to) {
				jQuery('h3.widgettitle, h4.widgettitle, .widgettitle a, .section-title, .section-title a, .widgettitle a, .widgettitle span').css({'color': to});
			});
		});
		
		wp.customize('ocmx_post_titles_font_color',function( value ) {
			value.bind(function(to) {
				jQuery('.products h3, .post-title, .post-title a, .page-title, .four-column .post-title a, .three-column .post-title a').css({'color': to});
			});
		});
		
		wp.customize('ocmx_post_titles_hover',function( value ) {
			value.bind(function(to) {
				jQuery('.post-title:hover, .post-title a:hover, .page-title:hover').css({'color': to});
			});
		});
		
		wp.customize('ocmx_date_meta',function( value ) {
			value.bind(function(to) {
				jQuery('h5.date, .archives_list .date').css({'color': to});
			});
		});
		
		wp.customize('ocmx_buttons',function( value ) {
			value.bind(function(to) {
				jQuery('.ui-slider-handle, .more-info, .copy .button, textarea, input, button, .button, #submit, .submit_button, .submitbutton, .gform_footer input.button, .product-content button, .quantity .plus, .quantity .minus, div.product .woocommerce_tabs ul.tabs li').css({'backgroundColor': to});
			});
		});
		
		wp.customize('ocmx_buttons_text',function( value ) {
			value.bind(function(to) {
				jQuery('.more-info, .copy .button, textarea, input, button, .button, #submit, .submit_button, .submitbutton, .gform_footer input.button, .product-content button, .quantity .plus, .quantity .minus, div.product .woocommerce_tabs ul.tabs li').css({'color': to});
			});
		});
		
		wp.customize('ocmx_buttons_borders',function( value ) {
			value.bind(function(to) {
				jQuery(' .feature .previous, .feature .next, .widget_product_search #searchform input#s, textarea, input, button, .more-info, .copy .button, .button, #submit, .submit_button, .submitbutton, .gform_footer input.button, .product-content button, .quantity .plus, .quantity .minus, div.product .woocommerce_tabs ul.tabs li, div.product .woocommerce_tabs .panel').css({'borderColor': to});
			});
		});
		
		wp.customize('ocmx_footer_container',function( value ) {
			value.bind(function(to) {
				jQuery('#footer-container').css({'backgroundColor': to});
			});
		});
		
		wp.customize('ocmx_widget_footer_titles_font_color',function( value ) {
			value.bind(function(to) {
				jQuery('#footer h4, #footer h4 a, #footer .widgettitle').css({'color': to});
			});
		});
		
		wp.customize('ocmx_footer_text',function( value ) {
			value.bind(function(to) {
				jQuery('.footer-text p, #footer').css({'color': to});
			});
		});
		
		wp.customize('ocmx_footer_links',function( value ) {
			value.bind(function(to) {
				jQuery('.footer-text a, #footer a').css({'color': to});
			});
		});
		
		wp.customize('ocmx_footer_links_hover',function( value ) {
			value.bind(function(to) {
				jQuery('#footer a:hover, .obox-credit a:hover').css({'color': to});
			});
		});
		
		wp.customize('ocmx_border_color',function( value ) {
			value.bind(function(to) {
				jQuery('ul#nav, ul#nav li a, .feature, .blog-main-post-container li, ul.widget-list li.widget, ul.widget-list li.widget li, .footer-text').css({'borderColor': to});
			});
		});
	
	} )( jQuery );
	</script>
<?php } 

//ADD POST MESSAGE

$wp_customize->get_setting('ocmx_header_background')->transport='postMessage';
$wp_customize->get_setting('ocmx_logo_text')->transport='postMessage';
$wp_customize->get_setting('ocmx_header_text')->transport='postMessage';
$wp_customize->get_setting('ocmx_navigation_background')->transport='postMessage';
$wp_customize->get_setting('ocmx_logo_font_color')->transport='postMessage';
$wp_customize->get_setting('ocmx_navigation_font_color')->transport='postMessage';
$wp_customize->get_setting('ocmx_navigation_hover')->transport='postMessage';
$wp_customize->get_setting('ocmx_general_font_color')->transport='postMessage';
$wp_customize->get_setting('ocmx_content_links')->transport='postMessage';
$wp_customize->get_setting('ocmx_content_links_hover')->transport='postMessage';
$wp_customize->get_setting('ocmx_navigation_submenu')->transport='postMessage';
$wp_customize->get_setting('ocmx_navigation_submenu_border')->transport='postMessage';
$wp_customize->get_setting('ocmx_navigation_submenu_links')->transport='postMessage';
$wp_customize->get_setting('ocmx_buttons_dates')->transport='postMessage';
$wp_customize->get_setting('ocmx_buttons_hover')->transport='postMessage';
$wp_customize->get_setting('ocmx_accent_color')->transport='postMessage';
$wp_customize->get_setting('ocmx_buttons_dates_text')->transport='postMessage';
$wp_customize->get_setting('ocmx_widget_titles_font_color')->transport='postMessage';
$wp_customize->get_setting('ocmx_post_titles_font_color')->transport='postMessage';
$wp_customize->get_setting('ocmx_page_title')->transport='postMessage';
$wp_customize->get_setting('ocmx_content_background')->transport='postMessage';
$wp_customize->get_setting('ocmx_tabs_nav')->transport='postMessage';
$wp_customize->get_setting('ocmx_testimonial_background')->transport='postMessage';
$wp_customize->get_setting('ocmx_testimonial_text')->transport='postMessage';
$wp_customize->get_setting('ocmx_post_title_background')->transport='postMessage';
$wp_customize->get_setting('ocmx_footer_background')->transport='postMessage';
$wp_customize->get_setting('ocmx_footer_links')->transport='postMessage';
$wp_customize->get_setting('ocmx_footer_links_hover')->transport='postMessage';
$wp_customize->get_setting('ocmx_footer_text')->transport='postMessage';
$wp_customize->get_setting('ocmx_copyright_text')->transport='postMessage';
$wp_customize->get_setting('ocmx_copyright_background')->transport='postMessage';
}
add_action( 'customize_register', 'ocmx_customize_register' );

function ocmx_add_query_vars($query_vars) {
	$query_vars[] = 'stylesheet';
	return $query_vars;
}
add_filter( 'query_vars', 'ocmx_add_query_vars' );
function ocmx_takeover_css() {
	    $style = get_query_var('stylesheet');
	    if($style == "custom") {
		    include_once(TEMPLATEPATH . '/style.php');
	        exit;
	    }
	}
add_action( 'template_redirect', 'ocmx_takeover_css');